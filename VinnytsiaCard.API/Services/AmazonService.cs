﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Transfer;
using System.IO;
using Amazon;
using Amazon.Runtime;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Services
{
    public class AmazonService
    {
        private const string USER_IMAGE_FOLDER_NAME = "users_images";
         private const string USER_RESIZED_IMAGE_FOLDER_NAME = "resized_users_images";
        private const string _bucketName = "vinnytsiacard";
        private string _filePath = "*** provide the full path name of the file to upload ***";
        private readonly RegionEndpoint bucketRegion = RegionEndpoint.EUNorth1;
        private IAmazonS3 s3Client;
        private IConfiguration _config;
        private static Random random = new Random();

        public enum ImagesTypes
        {
            UserMainImage,
            IdeaImage,
            PetitionImage,
            Image
        }


        public AmazonService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<Image> UploadUserImageFileAsync(string key, Stream stream, string bucketName = _bucketName)
        {
            string keyName = USER_IMAGE_FOLDER_NAME + "/" + key;

            try
            {
                using (var client = new AmazonS3Client(_config.GetSection("AWS:Access_Key").Value, _config.GetSection("AWS:Secret_Key").Value, RegionEndpoint.EUNorth1))
                {
                    var fileTransferUtility =
                        new TransferUtility(client);

                    await fileTransferUtility.UploadAsync(stream, bucketName, keyName);
                }

                keyName = USER_RESIZED_IMAGE_FOLDER_NAME + "/" + key;

                Image image = new Image()
                {
                    ImageTypeId = 0,
                    ImageKey = keyName,
                    ImageSrc = (_config.GetSection("AWS:URL").Value + "/" + keyName).ToLower()
                };

                return image;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't upload image to Amazon S3: {ex.Message}");
            }
        }

        public static void UploadPartProgressEventCallback(object sender, StreamTransferProgressArgs e)
        {
            // Process event. 
            Console.WriteLine("{0}/{1}", e.TransferredBytes, e.TotalBytes);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
