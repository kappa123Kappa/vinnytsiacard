using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Services
{
	/*
		Service for managing Telegram Api.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Service for managing Telegram Api.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class TelegramService
    {
        private const string TELEGRAM_REQUEST_URI = "https://api.telegram.org/bot";
        private const string SEND_MESSAGE_REQUEST_NAME = "sendMessage";
        private readonly IConfiguration _config;
        private readonly IHttpClientFactory _httpClientFactory;

        public TelegramService(IConfiguration config, IHttpClientFactory httpClientFactory)
        {
            _config = config;
            _httpClientFactory = httpClientFactory;
        }

		// <summary>
		/// Retrieves statistic about salaries by year.
		/// </summary>
		/// <returns>
		/// List of SalaryDto
		/// </returns>
		/// <param name="year">number of year</param>
		public async Task<SendBotMessageResponse> PostMessageBotAsync(string chatId, string text)
        {
            var client = _httpClientFactory.CreateClient();

            if (string.IsNullOrEmpty(chatId) || string.IsNullOrEmpty(text))
            {
                throw new ArgumentNullException($"Invalid data");
            }

            string requestUri = TELEGRAM_REQUEST_URI + _config.GetSection("telegramBotToken").Value + "/"
                + SEND_MESSAGE_REQUEST_NAME + "?chat_id=" + chatId + "&text=" + text;

			HttpResponseMessage moviesResponse;

			try
			{
				moviesResponse = await client.PostAsync(requestUri, null);
			}
			catch (HttpRequestException ex)
			{
				throw new HttpRequestException($"Can't post messege using Telegram Api: {ex.Message}");
			}
			catch (Exception ex)
			{
				throw new Exception($"Something went wrong: {ex.Message}");
			}

			return new SendBotMessageResponse();
        }
    }
}