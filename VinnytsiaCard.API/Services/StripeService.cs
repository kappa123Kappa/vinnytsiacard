using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Stripe;

namespace VinnytsiaCard.API.Services
{
	/*
		Service for managing Stripe Api.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Service for managing Stripe Api.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class StripeService
    {
        private readonly IConfiguration _config;

        public StripeService(IConfiguration config)
        {
            _config = config;
        }

		/// <summary>
		/// Creates card.
		/// </summary>
		/// <returns>
		/// Task Card
		/// </returns>
		/// <param name="cardToken">Token of card</param>
		/// <param name="custometId">Id of customer</param>
		public async Task<Card> CreateCardAsync(string cardToken, string custometId)
        {
            try
            {
                StripeConfiguration.ApiKey = _config.GetSection("StripeApiKey").Value;

                var options = new CardCreateOptions
                {
                    Source = cardToken,
                };
                var service = new CardService();
                
                return await service.CreateAsync(custometId, options);
            }
            catch (StripeException e)
            {
                switch (e.StripeError.ErrorType)
                {
                    case "card_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid card: {e.StripeError.Message}");
                    case "api_connection_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid connection: {e.StripeError.Message}");
                    case "api_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Problem with Api request: {e.StripeError.Message}");
                    case "authentication_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid authentication: {e.StripeError.Message}");
                    case "invalid_request_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid request: {e.StripeError.Message}");
                    case "rate_limit_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid rate: {e.StripeError.Message}");
                    case "validation_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid data: {e.StripeError.Message}");
                    default:
                        return null;
                }
            }
        }

		/// <summary>
		/// Creates charge.
		/// </summary>
		/// <returns>
		/// Task Charge
		/// </returns>
		/// <param name="chargeOptions">Object with options of charge</param>
		public async Task<Charge> CreateChargeAsync(ChargeCreateOptions chargeOptions)
        {
            try
            {
                StripeConfiguration.ApiKey = _config.GetSection("StripeApiKey").Value; ;
                var service = new ChargeService();
                return await service.CreateAsync(chargeOptions);
            }
            catch (StripeException e)
            {
                switch (e.StripeError.ErrorType)
                {
                    case "card_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid card: {e.StripeError.Message}");
                    case "api_connection_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid connection: {e.StripeError.Message}");
                    case "api_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Problem with Api request: {e.StripeError.Message}");
                    case "authentication_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid authentication: {e.StripeError.Message}");
                    case "invalid_request_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid request: {e.StripeError.Message}");
                    case "rate_limit_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid rate: {e.StripeError.Message}");
                    case "validation_error":
                        throw new StripeException($"Error code: {e.StripeError.Code}. Invalid data: {e.StripeError.Message}");
                    default:
                        return null;
                }
            }
        }
    }
}