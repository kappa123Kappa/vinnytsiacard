namespace VinnytsiaCard.API.Models
{
    public class ActivityType
    {
        public int Id { get; set; }
        public string ActivityTypeName { get; set; }
    }
}