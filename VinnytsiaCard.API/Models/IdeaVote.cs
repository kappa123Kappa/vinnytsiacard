using System;

namespace VinnytsiaCard.API.Models
{
    public class IdeaVote
    {
        public int Id { get; set; } = 0;
        public int IdeaId { get; set; } = 0;
        public int AuthorId { get; set; } = 0;
        public bool VoteStatus { get; set; } = false;
        public DateTime Date { get; set; } 
    }
}