namespace VinnytsiaCard.API.Models
{
    public class GetSearchAllAcabs
    {
        public string Text { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}