namespace VinnytsiaCard.API.Models
{
    public class Pagination
    {
        public int PageSize { get; set; } = 0;
        public int PageNumber { get; set; } = 0;
        public int ItemsCount { get; set; } = 0;
    }
}