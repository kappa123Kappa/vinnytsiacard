namespace VinnytsiaCard.API.Models
{
    public class FoodStatistic
    {
        public int Id { get; set; }
        public int FoodTypeId { get; set; }
        public float FoodValue { get; set; }
        public int Year { get; set; }
    }
}