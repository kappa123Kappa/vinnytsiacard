namespace VinnytsiaCard.API.Models
{
    public class Street
    {
        public int Id {get; set;}
        public string StreetName {get; set;}
    }
}