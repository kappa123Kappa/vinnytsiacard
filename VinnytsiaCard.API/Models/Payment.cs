using System;

namespace VinnytsiaCard.API.Models
{
    public class Payment
    {
        public int Id { get; set; } = 0;
        public int UserId { get; set; } = 0;
        public int RecipientId { get; set; } = 0;
        public string PaymentId { get; set; } = "";
        public double Amount { get; set; } = 0;
        public string Type { get; set; } = "";
        public string PayType { get; set; } = "";
        public string Description { get; set; } = "";
        public string SenderCardMask { get; set; } = "";
        public string SenderCardBank { get; set; } = "";
        public string SenderCardType { get; set; } = "";
        public string Currency { get; set; } = "";
        public DateTime CreateDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}