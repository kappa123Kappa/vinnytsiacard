namespace VinnytsiaCard.API.Models
{
    public class UserImage
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ImageId { get; set; }
    }
}