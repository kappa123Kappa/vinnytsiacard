namespace VinnytsiaCard.API.Models
{
    public class UserAdditionalBill
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int AdditionaBillId { get; set; }
    }
}