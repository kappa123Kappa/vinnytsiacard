namespace VinnytsiaCard.API.Models
{
    public class PaymentRecipient
    {
        public int Id { get; set; }
        public string RecipientName { get; set; }
        public string RecipientType { get; set; }
    }
}