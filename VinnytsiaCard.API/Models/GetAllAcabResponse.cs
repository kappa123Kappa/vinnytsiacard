using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace VinnytsiaCard.API.Models
{
    public class GetAllAcabResponse
    {
        [JsonProperty("help")]
        public string Help { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("result")]
        public Result Result { get; set; }
    }

    public class Result
    {
        [JsonProperty("include_total")]
        public bool IncludeTotal { get; set; }

        [JsonProperty("resource_id")]
        public string ResourceId { get; set; }

        [JsonProperty("fields")]
        public List<Field> Fields { get; set; }

        [JsonProperty("records_format")]
        public string RecordsFormat { get; set; }

        [JsonProperty("records")]
        public List<Record> Records { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }
    }

    public class Links
    {
        [JsonProperty("start")]
        public string Start { get; set; }

        [JsonProperty("next")]
        public string Next { get; set; }
    }

     public class Record
    {

        [JsonProperty("_id")]
        public int _Id { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("nameACMB")]
        public string NameACMB { get; set; }

        [JsonProperty("legalAddress")]
        public string LegalAddress { get; set; }

        [JsonProperty("Addresses")]
        public string Addresses { get; set; }

        [JsonProperty("RegistrationDate")]
        public DateTime RegistrationDate { get; set; }

        [JsonProperty("Re-RegistrationDate")]
        public object ReRegistrationDate { get; set; }
    }

    public class Field
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
       
}