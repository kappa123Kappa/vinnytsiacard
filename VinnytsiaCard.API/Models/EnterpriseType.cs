namespace VinnytsiaCard.API.Models
{
    public class EnterpriseType
    {
        public int Id { get; set; }
        public string EnterpriseTypeName { get; set; }
    }
}