using System;
using VinnytsiaCard.API.DTO;

namespace VinnytsiaCard.API.Models
{
    public class Forum
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int MainMessageId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int AuthorId { get; set; }

        public ForumDto FromForumToForumDto(Forum forum)
        {
            return new ForumDto() 
            {
                Id = forum.Id,
                Title = forum.Title,
                AuthorImageSrc = "",
                MainMessageId = forum.MainMessageId,
                CreatedAt = forum.CreatedAt,
                AuthorId = forum.AuthorId
            };
        }
    }
}