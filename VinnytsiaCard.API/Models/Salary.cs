namespace VinnytsiaCard.API.Models
{
    public class Salary
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public int SalaryValue { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}