using System;

namespace VinnytsiaCard.Models
{
    public class PetitionVote
    {
        public int Id { get; set; } = 0;
        public int PetitionId { get; set; } = 0;
        public int AuthorId { get; set; } = 0;
        public bool VoteStatus { get; set; } = false;
        public DateTime Date { get; set; } 
    }
}