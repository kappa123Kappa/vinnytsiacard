namespace VinnytsiaCard.API.Models
{
    public class MessageMedia
    {
        public int Id { get; set; }
        public int MessageId { get; set; } = 0;
        public string MediaLink { get; set; } = "";
        public int MediaType { get; set; } = 0;
    }
}