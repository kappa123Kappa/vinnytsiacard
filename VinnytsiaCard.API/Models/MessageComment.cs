namespace VinnytsiaCard.API.Models
{
    public class MessageComment
    {
        public int Id { get; set; }
        public int ForumId { get; set; } = 0;
        public int MessageId { get; set; }
        public int CommentId { get; set; }

    }
}