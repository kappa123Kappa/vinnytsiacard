namespace VinnytsiaCard.API.Models
{
    public class UserBill
    {
        public int Id { get; set; } = 0;
        public int EnterpriseId { get; set; } = 0;   
        public int BillTypeId { get; set; } = 0;
        public int UserId { get; set; } = 0;
        public string BillName { get; set; } = "";
        public long BillNumber { get; set; } = 0;
    }
}