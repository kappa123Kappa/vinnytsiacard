using System;

namespace VinnytsiaCard.API.Models
{
    public class Idea
    {
        public int Id { get; set; } = 0;
        public int AcabId { get; set; } = 0;
        public int AuthorId { get; set; } = 0;
        public string IdeaName { get; set; } = "";
        public string Description { get; set; } = "";
        public string ImageLink { get; set; } = "";
        public DateTime Date { get; set; }
    }
}