namespace VinnytsiaCard.API.Models
{
    public class FoodType
    {
        public int Id { get; set; }
        public string FoodTypeName { get; set; }
    }
}