using Newtonsoft.Json;

namespace VinnytsiaCard.API.Models
{
    public class SmartCinemaGetMoviesRequestPayload
    {
        [JsonProperty("cityId")]
        public int CityId { get; set; } = 1;
    }
}