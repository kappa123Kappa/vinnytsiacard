using System;

namespace VinnytsiaCard.API.Models
{
    public class ForumMessage
    {
        public int Id { get; set; }
        public int ForumId { get; set; } = 0;
        public int AuthorId { get; set; } = 0;
        public string Message { get; set; } = "";
        public DateTime Date { get; set; }
    }
}