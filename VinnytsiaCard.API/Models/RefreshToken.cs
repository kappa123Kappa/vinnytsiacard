﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VinnytsiaCard.Models
{
	public class RefreshToken
	{
		public int Id { get; set; }
		public string Login { get; set; }
		public string RefreshTokenString { get; set; }
		public string WhiteIP { get; set; }
		public string Browser { get; set; }
		public DateTime Date { get; set;} 
	}
}
