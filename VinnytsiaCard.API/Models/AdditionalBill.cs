namespace VinnytsiaCard.API.Models
{
    public class AdditionalBill
    {
        public int Id { get; set; }
        public int EnterpriseServiceId { get; set; }
        public string CustomerBillId { get; set; }
    }
}