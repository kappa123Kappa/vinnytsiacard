using System;

namespace VinnytsiaCard.Models
{
    public class Petition
    {
        public int Id { get; set; } = 0;
        public int AuthorId { get; set; } = 0;
        public string PetitionName { get; set; } = "";
        public string Description { get; set; } = "";
        public string ImageLink { get; set; } = "";
        public DateTime Date { get; set; }
    }
}