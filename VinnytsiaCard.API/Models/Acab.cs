using System;

namespace VinnytsiaCard.API.Models
{
    public class Acab
    {
        public int Id { get; set; }
        public int AcabId { get; set; }
        public int BuildingId { get; set; }
        public string AcabName { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}