namespace VinnytsiaCard.API.Models
{
    public class EnterpriseService
    {
        public int Id { get; set; }
        public int EnterpriseId { get; set; }
        public int ServiceId { get; set; }
    }
}