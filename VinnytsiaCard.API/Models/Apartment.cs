namespace VinnytsiaCard.API.Models
{
    public class Apartment
    {
        public int Id { get; set; } = 0;
        public int AcabId { get; set; } = 0;
        public int ApartmentNumber { get; set; } = 0;
        public int OwnerId { get; set; } = 0;
        public int ResidentsCount { get; set; } = 0;
        public int RoomsCount { get; set; } = 0;
    }
}