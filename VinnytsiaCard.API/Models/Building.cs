namespace VinnytsiaCard.API.Models
{
    public class Building
    {
        public int Id {get; set;}
        public int StreetId {get; set;}
        public string BuildingNumber {get; set;}
        public int CountOfApartments { get; set; }
    }
}