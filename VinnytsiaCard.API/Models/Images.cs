namespace VinnytsiaCard.API.Models
{
    public class Image
    {
        public int Id { get; set; } = 0;
        public int ImageTypeId { get; set; } = 0;
        public string ImageKey { get; set; } = "";
        public string ImageSrc { get; set; } = "";
    }
}