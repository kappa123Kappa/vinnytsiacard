using System;
using System.Collections.Generic;

namespace VinnytsiaCard.API.Models
{
    public class PaymentDto
    {
        public List<SimplePayment> PaymentsList { get; set; } = new List<SimplePayment>();
        public int PaymentsCount { get; set; } = 0;
    }

    public class SimplePayment
    {
        public int Id { get; set; } = 0;
        public int UserId { get; set; } = 0;
        public int RecipientId { get; set; } = 0;
        public string RecipientName { get; set; } = "";
        public string PaymentId { get; set; } = "";
        public double Amount { get; set; } = 0;
        public string Type { get; set; } = "";
        public string PayType { get; set; } = "";
        public string Description { get; set; } = "";
        public string SenderCardMask { get; set; } = "";
        public string Currency { get; set; } = "";
        public DateTime EndDate { get; set; }
    }
}