namespace VinnytsiaCard.API.Models
{
    public class MainBill
    {
        public int Id { get; set; }
        public int BillTypeId { get; set; }
        public string CustomerBillId { get; set; }
        public int EnterpriseId { get; set; }
        public string BillName { get; set; }
    }
}