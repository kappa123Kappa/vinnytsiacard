using System;

namespace VinnytsiaCard.API.Models
{
    public class Population
    {
        public int Id { get; set; }
        public int CityId { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
    }
}