using System.Collections.Generic;
using Newtonsoft.Json;

namespace VinnytsiaCard.API.Models
{
    public class SmartCinemaGetMoviesResponse
    {
        [JsonProperty("soon")]
        public List<Soon> Soon { get; set; }

        [JsonProperty("rent")]
        public List<Rent> Rent { get; set; }

        [JsonProperty("branch")]
        public List<Branch> Branch { get; set; }

        [JsonProperty("shares")]
        public List<Share> Shares { get; set; }
    }

    public class SeoInline
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class Soon
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("age")]
        public int Age { get; set; }

        [JsonProperty("poster")]
        public string Poster { get; set; }

        [JsonProperty("have_showtime")]
        public bool HaveShowtime { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("seo_inline")]
        public SeoInline SeoInline { get; set; }

        [JsonProperty("film_release_date")]
        public string FilmReleaseDate { get; set; }

        [JsonProperty("pictures")]
        public string Pictures { get; set; }

        [JsonProperty("trailer")]
        public string Trailer { get; set; }
    }

    public class Rent
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("age")]
        public int Age { get; set; }

        [JsonProperty("poster")]
        public string Poster { get; set; }

        [JsonProperty("have_showtime")]
        public bool HaveShowtime { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("seo_inline")]
        public SeoInline SeoInline { get; set; }

        [JsonProperty("film_release_date")]
        public string FilmReleaseDate { get; set; }

        [JsonProperty("pictures")]
        public string Pictures { get; set; }

        [JsonProperty("trailer")]
        public string Trailer { get; set; }
    }

    public class Branch
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("lon")]
        public string Lon { get; set; }

        [JsonProperty("lat")]
        public string Lat { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }
    }

    public class Share
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("short_description")]
        public string ShortDescription { get; set; }

        [JsonProperty("seo_inline")]
        public SeoInline SeoInline { get; set; }

        [JsonProperty("type_of_post")]
        public string TypeOfPost { get; set; }
    }
}