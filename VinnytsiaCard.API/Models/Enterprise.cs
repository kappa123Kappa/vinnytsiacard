namespace VinnytsiaCard.API.Models
{
    public class Enterprise
    {
        public int Id { get; set; }
        public int EnterpriseTypeId { get; set; }
        public string EnterpriseName { get; set; }
    }
}