namespace VinnytsiaCard.API.Models
{
    public class ImageType
    {
        public int Id { get; set; }
        public string ImageTypeName { get; set; }
    }
}