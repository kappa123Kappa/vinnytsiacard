﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace VinnytsiaCard.Options
{
    public class AuthOptions
    {
        public const string ISSUER = "MyAuthServer"; // видає токен
        public const string AUDIENCE = "http://localhost:5000/"; // споживач токена
        const string JWT_KEY = "Vinistsia Is The Capital Of Great Ukraine! 1 2 3";   // ключ для шифрування jwt токена
		const string JWT_REFRESH_KEY = "Vinistsia Is The Capital Of Great Ukraine! Said Batman";   // ключ для шифрування рефреш токена
		public const int LIFETIME = 2; // життя токена в днях

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT_KEY));
        }

		public static SymmetricSecurityKey GetSymmetricSecurityRefreshKey()
		{
			return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT_REFRESH_KEY));
		}

		public static bool ValidateToken(string authToken)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var validationParameters = GetValidationParameters();

			SecurityToken validatedToken;
			IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
			return true;
		}

		public static bool ValidateRefreshToken(string authToken)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var validationParameters = GetValidationRefreshParameters();

			SecurityToken validatedToken;
			IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
			return true;
		}

		public static TokenValidationParameters GetValidationParameters()
		{
			return new TokenValidationParameters()
			{
				ValidateLifetime = true,
				ValidateAudience = false,
				ValidateIssuer = false,
				ValidIssuer = ISSUER,
				ValidAudience = AUDIENCE,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT_KEY))
			};
		}

		public static TokenValidationParameters GetValidationRefreshParameters()
		{
			return new TokenValidationParameters()
			{
				ValidateLifetime = true,
				ValidateAudience = false,
				ValidateIssuer = false,
				ValidIssuer = ISSUER,
				ValidAudience = AUDIENCE,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT_REFRESH_KEY))
			};
		}
	}
}
