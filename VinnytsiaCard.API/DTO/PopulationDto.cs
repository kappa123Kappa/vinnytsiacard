using System;

namespace VinnytsiaCard.API.DTO
{
    public class PopulationDto
    {
        public int Id { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
    }
}