namespace VinnytsiaCard.API.DTO
{
    public class PayDto
    {
        public int Id { get; set; }
        public int EnterpriseId { get; set; }
        public string CustomerId { get; set; }
        public string RecipientId { get; set; }
        public string CardId { get; set; }
        // public string CardNumber { get; set; }
        // public int ExpYear { get; set; }
        // public int ExpMonth { get; set; }
        // public string Cvc { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
    }
}