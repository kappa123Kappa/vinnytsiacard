namespace VinnytsiaCard.API.DTO
{
    public class AddAdditionalBillDto
    {
        public int EnterpriseId { get; set; } = 0;
        public int ServiceId { get; set; } = 0;
        public int UserId { get; set; } = 0;
    }
}