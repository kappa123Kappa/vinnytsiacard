using System;

namespace VinnytsiaCard.API.DTO
{
    public class CreateForumDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
        public int AuthorId { get; set; }
    }
}