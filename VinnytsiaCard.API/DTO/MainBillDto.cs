namespace VinnytsiaCard.API.DTO
{
    public class MainBillDto
    {
        public int Id { get; set; }
        public string BillType { get; set; }
        public string CustomerBillId { get; set; }
        public int EnterpriseId { get; set; }
        public string Enterprise { get; set; }
        public string BillName { get; set; }
    }
}