using System.ComponentModel.DataAnnotations;

namespace VinnytsiaCard.API.DTO
{
    public class UserLoginDto
    {
        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify user name from 4 to 24")]
        public string Username { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify password from 4 to 8")]
        public string Password { get; set; }      
        public bool IsRemembered { get; set; }
    }
}