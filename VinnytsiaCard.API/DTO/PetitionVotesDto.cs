using System.Collections.Generic;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.DTO
{
    public class PetitionVotesDto
    {
        public List<PetitionVote> VotesList { get; set; } = new List<PetitionVote>();
        public bool IsUserVoted { get; set; } = false;
    }
}