namespace VinnytsiaCard.API.DTO
{
    public class SalaryDto
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int SalaryValue { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}