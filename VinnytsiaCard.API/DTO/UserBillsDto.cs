namespace VinnytsiaCard.API.DTO
{
    public class UserBillsDto
    {
        public int Id { get; set; }
        public int BillTypeId { get; set; }
        public string BillTypeName { get; set; }
        public int UserId { get; set; }
        public long BillNumber { get; set; }
    }
}