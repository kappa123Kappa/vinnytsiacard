using System.Collections.Generic;

namespace VinnytsiaCard.API.DTO
{
    public class AdditionalBillDto
    {
        public List<AdditionalBillItem> AdditionalBillList { get; set; } = new List<AdditionalBillItem>();
        public int AdditionalBillsCount { get; set; } = 0;
    }

    public class AdditionalBillItem
    {
        public int Id { get; set; }
        public string CustomerBillId { get; set; }
        public int EnterpriseId { get; set; }
        public string Enterprise { get; set; }
        public string ServiceName { get; set; }
    }
}