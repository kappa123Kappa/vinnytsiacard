using System;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.DTO
{
    public class ForumDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string AuthorImageSrc { get; set; }
        public int MainMessageId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int AuthorId { get; set; }

        public Forum FromForumDtoToForum(ForumDto forum)
        {
            return new Forum()
            {
                Id = forum.Id,
                Title = forum.Title,
                MainMessageId = forum.MainMessageId,
                CreatedAt = forum.CreatedAt,
                AuthorId = forum.AuthorId
            };
        }
    }
}