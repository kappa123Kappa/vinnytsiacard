using System.ComponentModel.DataAnnotations;

namespace VinnytsiaCard.API.DTO
{
    public class UserRegisterDto
    {
        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify login from 4 to 24")]
        public string Login { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify password from 4 to 24")]
        public string Password { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify password from 4 to 24")]
        public string Password2 { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify first name from 4 to 24")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify last name from 4 to 24")]
        public string LastName { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "You must specify middle name from 4 to 24")]
        public string MiddleName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Phone]
        public string Phone { get; set; }
    }
}