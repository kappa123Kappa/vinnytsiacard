using System.Collections.Generic;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.DTO
{
    public class StatisticPaymentDto
    {
        public int RecipientId { get; set; } = 0;
        public decimal Amount { get; set; } = 0;
        public int MonthNumber { get; set; } = 0;
    }
}