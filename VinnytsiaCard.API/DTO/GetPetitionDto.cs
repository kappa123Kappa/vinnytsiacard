
using System;
using System.Collections.Generic;

namespace VinnytsiaCard.API.DTO
{
    public class GetPetitionDto
    {
        public List<PetitionDto> petitionDtoList { get; set; } = new List<PetitionDto>();
        public int PetitionsCount { get; set; } = 0;
    }

    public class PetitionDto
    {
        public int Id { get; set; } = 0;
        public int AuthorId { get; set; } = 0;
        public string AuthorFirstName { get; set; } = "";
        public string AuthorLastName { get; set; } = "";
        public string PetitionName { get; set; } = "";
        public string Description { get; set; } = "";
        public string ImageLink { get; set; } = "";
        public DateTime Date { get; set; }
    }
}