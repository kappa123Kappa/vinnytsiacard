namespace VinnytsiaCard.API.DTO
{
    public class ApartmentDto
    {
        public int Id { get; set; }
        public int AcabId { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string AcabName { get; set; }
        public int UserApartmentNumber { get; set; }    
    }
}