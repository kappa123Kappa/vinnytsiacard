namespace VinnytsiaCard.API.DTO
{
    public class FoodStatisticDto
    {
        public int Id { get; set; }
        public int FoodTypeId { get; set; }
        public string FoodTypeName { get; set; }
        public float FoodValue { get; set; }
        public int Year { get; set; }
    }
}