using System;
using System.Collections.Generic;

namespace VinnytsiaCard.API.DTO
{
    public class GetIdeaDto
    {
        public List<IdeaDto> IdeaDtoList { get; set; } = new List<IdeaDto>();
        public int IdeasCount { get; set; } = 0;
    }

    public class IdeaDto
    {
        public int Id { get; set; } = 0;
        public int AcabId { get; set; } = 0;
        public string AcabName { get; set; } = "";
        public int AuthorId { get; set; } = 0;
        public string AuthorFirstName { get; set; } = "";
        public string AuthorLastName { get; set; } = "";
        public string IdeaName { get; set; } = "";
        public string Description { get; set; } = "";
        public string ImageLink { get; set; } = "";
        public DateTime Date { get; set; }
    }
}