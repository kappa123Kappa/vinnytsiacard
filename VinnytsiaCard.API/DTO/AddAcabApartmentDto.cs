using System.Collections.Generic;

namespace VinnytsiaCard.API.DTO
{
    public class AddNewAcabApartmentDto
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string AcabName { get; set; }
        public string RegistrationDate { get; set; }
        public int CountOfApartments { get; set; }
        public int UserApartmentNumber { get; set; }
        public List<int> AvaliableApartments { get; set; }      
    }
}