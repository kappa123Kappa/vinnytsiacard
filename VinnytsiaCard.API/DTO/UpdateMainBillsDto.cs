using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.DTO
{
    public class UpdateMainBillsDto
    {
        public UserBill GasBill { get; set; }
        public UserBill WaterBill { get; set; }
        public UserBill ElectricityBill { get; set; }
    }
}