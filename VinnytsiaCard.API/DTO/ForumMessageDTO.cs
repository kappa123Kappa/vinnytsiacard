using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.DTO
{
    public class ForumMessageDto
    {
       public List<ForumMessageWithLoginsAndMediaLinks> ForumMessages { get; set; } = new List<ForumMessageWithLoginsAndMediaLinks>();
       public Pagination Pagination { get; set; } = new Pagination();
    }

    public class ForumMessageWithLoginsAndMediaLinks
    {
        public int Id { get; set; }
        public int ForumId { get; set; } = 0;
        public int AuthorMessageId { get; set; } = 0;
        public string AuthorMessageLogin { get; set; } = "";
        public string AuthorImageSrc { get; set; } = ""; 
        public string Message { get; set; } = "";
        public int AuthorQuoteId { get; set; } = 0;
        public string AuthorQuoteLogin { get; set; } = "";
        public int QuoteId { get; set; } = 0;
        public string Quote { get; set; } = "";
        public string MediaLink { get; set; } = "";
        public DateTime Date { get; set; }
    } 
}
