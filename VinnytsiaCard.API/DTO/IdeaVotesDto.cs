using System.Collections.Generic;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.DTO
{
    public class IdeaVotesDto
    {
        public List<IdeaVote> VotesList { get; set; } = new List<IdeaVote>();
        public bool IsUserVoted { get; set; } = false;
    }
}