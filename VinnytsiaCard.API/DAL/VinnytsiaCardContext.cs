using Microsoft.EntityFrameworkCore;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.DAL
{
    public class VinnytsiaCardContext : DbContext
    {
        public VinnytsiaCardContext(DbContextOptions<VinnytsiaCardContext> options) : base(options)
        {
           
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentRecipient> PaymentRecipients { get; set; }
        public DbSet<Forum> Forums { get; set; }
        public DbSet<ForumMessage> ForumMessages { get; set; }
        public DbSet<MessageComment> MessagesComments { get; set; }
        public DbSet<MessageMedia> MessagesMedia { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Acab> Acabs { get; set; }
        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<EnterpriseType> EnterprisesTypes { get; set; }
        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<EnterpriseService> EnterprisesServices { get; set; }
        public DbSet<BillType> BillTypes { get; set; }
        public DbSet<UserBill> UsersBills { get; set; }
        public DbSet<MainBill> MainBills { get; set; }
        public DbSet<AdditionalBill> AdditionalBill { get; set; }
        public DbSet<UserAdditionalBill> UserAdditionalBills { get; set; }
        public DbSet<Idea> Ideas { get; set; }
        public DbSet<IdeaVote> IdeasVotes { get; set; }
        public DbSet<Petition> Petitions { get; set; }
        public DbSet<PetitionVote> PetitionsVotes { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Population> Populations { get; set; }
        public DbSet<PopulationStable> PopulationsStable { get; set; }
        public DbSet<ActivityType> ActivitiesTypes { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<FoodType> FoodTypes { get; set; }
        public DbSet<FoodStatistic> FoodStatistics { get; set; }
        public DbSet<ImageType> ImagesTypes { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<UserImage> UsersImages { get; set; }
    }
}