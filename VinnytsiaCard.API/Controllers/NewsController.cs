using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NewsAPI.Constants;
using NewsAPI.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private IConfiguration _config; 
        private INewsRepository _repository;

        public NewsController(IConfiguration config, INewsRepository repository)
        {
            _config = config;
            _repository = repository;
        }

        [HttpGet("getnews/{key}/{page}/{pageSize}")]
        public async Task<IActionResult> GetNewsAsync(string key, string page, string pageSize)
        {           
            int requestPage = 0;
            int requestPageSize = 0;

            try
            {
                int.TryParse(page,out requestPage);
                int.TryParse(pageSize,out requestPageSize);
            }
            catch (Exception ex)
            {
                BadRequest($"invalid passed page '{page}' and pageSize '{pageSize}' values: {ex.Message}");
            }

            EverythingRequest request = new EverythingRequest()
            {
                Q = key,
                SortBy = SortBys.PublishedAt,
                Language = Languages.UK,
                From = DateTime.Today.AddMonths(-1),
                To = DateTime.Now,
                Page = requestPage,
                PageSize = requestPageSize
            };

            ArticlesResult result = await _repository.GetNewsAsync(request, _config);

            if (result.Error != null)
            {
                 return BadRequest($"Can't get news: {result.Error.Message}");
            }
            return Ok(result);
           
        }
    }
}