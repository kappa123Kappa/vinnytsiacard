using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stripe;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        IAuthRepository _authRepository;
        IPaymentRepository _paymentRepository;
        IProfileRepository _profileRepository;
        public PaymentController(IAuthRepository authRepository, IProfileRepository profileRepository, IPaymentRepository paymentRepository)
        {
            _authRepository = authRepository;
            _profileRepository = profileRepository;
            _paymentRepository = paymentRepository;
        }

        [HttpGet("GetUserPayments/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetUserPaymentsAsync(int pageSize, int pageNumber)
        {
            int id = 0;
            string userName = User.Identity.Name;

            if (!string.IsNullOrEmpty(userName))
            {
                User userData = await _profileRepository.GetUserDataAsync(userName);
                id = userData.Id;

                PaymentDto userPayments = _paymentRepository.GetUserPaymentsById(id, pageSize, pageNumber);

                return Ok(userPayments);
            }

            return BadRequest();
        }

        [HttpPost("AddPayments")]
        public async Task<IActionResult> AddPaymentAsync(Payment payment)
        {
            int id = 0;
            string userName = User.Identity.Name;

            if (!string.IsNullOrEmpty(userName))
            {
                User userData = await _profileRepository.GetUserDataAsync(userName);
                payment.UserId = userData.Id;

                _paymentRepository.AddNewPayment(payment);

                return Ok($"Payment for a user '{ id }' was added successfully");
            }

            return BadRequest("The payment was not added.");
        }

        [HttpPost("DeleteUserAdditionalPaymentById/{paymentId}")]
        public async Task<IActionResult> DeleteUserPaymentByIdAsync(int paymentId)
        {
            string userName = User.Identity.Name;

            if (!string.IsNullOrEmpty(userName))
            {
                if(await _paymentRepository.DeleteUserAdditionalPaymentAsync(paymentId, userName))
                {
                    return Ok(true);
                }
            }

            return BadRequest("The payment was not deleted.");
        }

        [HttpGet("GetUserMainBills")]
        public async Task<IActionResult> GetUserPaymentsAsync()
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<MainBillDto> userBills = _paymentRepository.GetUserMainBills();

                if (userBills == null)
                {
                    return BadRequest($"Can't receive users main bills.");
                }

                return Ok(userBills);
            }

            return BadRequest($"Invalid user");
        }

        [HttpGet("GetBillsTypes")]
        public async Task<IActionResult> GetBillsTypesAsync()
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<BillType> billsTypesList = await _paymentRepository.GetAllBillsTypesAsync();

                if (billsTypesList == null)
                {
                    return BadRequest($"Can't receive bills types.");
                }

                return Ok(billsTypesList);
            }

            return BadRequest($"Invalid user");
        }

        [HttpGet("GetEnterprisesTypes")]
        public async Task<IActionResult> GetEnterprisesTypesAsync()
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<EnterpriseType> billsTypesList = _paymentRepository.GetAllEnterprisesTypes();

                if (billsTypesList == null)
                {
                    return BadRequest($"Can't receive enterprises types.");
                }

                return Ok(billsTypesList);
            }

            return BadRequest($"Invalid user");
        }

        [HttpGet("GetEnterprisesNames/{enterpriseTypeId}")]
        public async Task<IActionResult> GetEnterprisesNames(int enterpriseTypeId)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<Enterprise> billsTypesList = _paymentRepository.GetEnterprisesById(enterpriseTypeId);

                if (billsTypesList == null)
                {
                    return BadRequest($"Can't receive enterprises name.");
                }

                return Ok(billsTypesList);
            }

            return BadRequest($"Invalid user");
        }

        [HttpGet("GetEnterpriseServicesNames/{enterpriseId}")]
        public async Task<IActionResult> GetEnterpriseServiceNames(int enterpriseId)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<Service> billsTypesList = await _paymentRepository.GetEnterpriseServiceByIdAsync(enterpriseId, userName);

                if (billsTypesList == null)
                {
                    return BadRequest($"Can't receive enterprises services.");
                }

                return Ok(billsTypesList);
            }

            return BadRequest($"Invalid user");
        }

        [HttpPost("UpdatMainBills")]
        public async Task<IActionResult> UpdateMainBillsAsync(UpdateMainBillsDto bills)
        {
            string userName = User.Identity.Name;

            if (!string.IsNullOrEmpty(userName))
            {
                User userData = await _profileRepository.GetUserDataAsync(userName);

                bills = await _paymentRepository.UpdateMainBillsAsync(bills, userName);
                if (bills != null)
                {
                    return Ok(bills);
                }
            }

            return BadRequest("The bills were not updated.");
        }

        [HttpGet("GetUserOtherBills")]
        public async Task<IActionResult> GetUserOtherPaymentsAsync()
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<UserBillsDto> userBills = await _paymentRepository.GetUserOtherBillsAsync(userName);

                if (userBills == null)
                {
                    return BadRequest($"Can't receive users bills.");
                }

                return Ok(userBills);
            }

            return BadRequest($"Invalid user");
        }

        
        [HttpPost("AddUserAdditionalBill")]
        public async Task<IActionResult> AddUserAdditionalBill(AddAdditionalBillDto addAdditionalBill)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                UserAdditionalBill userAdditionalBill = await _paymentRepository.AddUserAdditionalBillAsync(addAdditionalBill, userName);

                if (userAdditionalBill != null)
                {
                    return Ok(true);
                }
            }

            return BadRequest("Invalid transaction");
        }

        [HttpGet("GetUserAdditionalBills")]
        public async Task<IActionResult> GetUserAdditionalBills()
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                AdditionalBillDto additionalBillDto = await _paymentRepository.GetUserAdditionalBillAsync(userName);

                if (additionalBillDto != null)
                {
                    return Ok(additionalBillDto);
                }
            }

            return BadRequest("Invalid transaction");
        }

        [HttpPost("Pay")]
        public async Task<IActionResult> PayAsync(PayDto pay)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                Payment payment = await _paymentRepository.PayAsync(pay, userName);

                if (payment != null)
                {
                    return Ok(true);
                }
            }

            return BadRequest("Invalid transaction");
        }
    }
}