using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Repositories;
using VinnytsiaCard.API.Services;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PetitionController : ControllerBase
    {
        IConfiguration _config;
        IAuthRepository _authRepository;
        IPetitionRepository _petitionRepository;
        ITelegramRepository _telegramRepository;

        public PetitionController(IConfiguration config, IAuthRepository authRepository, IPetitionRepository petitionRepository,
            ITelegramRepository telegramRepository)
        {
            _config = config;   
            _authRepository = authRepository;
            _petitionRepository = petitionRepository;
            _telegramRepository = telegramRepository;                  
        }

        [HttpGet("GetPetitions/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetPetitionsAsync(int pageSize, int pageNumber)
        {
            string userLogin = User.Identity.Name;
            GetPetitionDto petitions = new GetPetitionDto();

            if(await _authRepository.UserExistAsync(userLogin))
            {
                petitions = _petitionRepository.GetPetitions(pageSize, pageNumber);

                if(petitions == null)
                {
                    return BadRequest("Cant receive petitions");
                }

                return Ok(petitions);
            }

            return BadRequest("Invalid user");
        }

        [HttpGet("GetPetition/{petitionId}")]
        public async Task<IActionResult> GetPetitionsByIdAsync(int petitionId)
        {
            string userLogin = User.Identity.Name;
            PetitionDto petitions = new PetitionDto();

            if(await _authRepository.UserExistAsync(userLogin))
            {
                petitions = await _petitionRepository.GetPetitionByIdAsync(petitionId);

                if(petitions == null)
                {
                    return BadRequest($"Cant receive petition #{petitionId}");
                }

                return Ok(petitions);
            }

            return BadRequest("Invalid user");
        }

        [HttpPost("AddPetition")]
        public async Task<IActionResult> AddPetitionAsync(Petition petition)
        {
            string userLogin = User.Identity.Name;

            if(await _authRepository.UserExistAsync(userLogin))
            {
                petition = await _petitionRepository.AddPetitionAsync(petition, userLogin);

                if(petition == null)
                {
                    return BadRequest("Petition was not added.");
                }

                string chatId = _config.GetSection("telegramChatId").Value;
                string text = $"Створено нову петицію на тему '{petition.PetitionName}'. Більш детальніше, за посиланням: " + 
                    $"{_config.GetSection("SiteUrl").Value}home/voting/petition/{petition.Id}";
                await _telegramRepository.PostMessageBotAsync(chatId, text);

                return Ok(petition);
            }

            return BadRequest("Invalid user.");
        }

        [HttpGet("GetPetitionVotes/{petitionId}")]
        public async Task<IActionResult> GetPetitionsVotes(int petitionId)
        {
            string userLogin = User.Identity.Name;
            PetitionVotesDto petitionVotes = new PetitionVotesDto();

            if(await _authRepository.UserExistAsync(userLogin))
            {
                petitionVotes = await _petitionRepository.GetPetitionVotes(petitionId, userLogin);

                if(petitionVotes == null)
                {
                    return BadRequest($"Cant receive votes of petition #{petitionId}");
                }

                return Ok(petitionVotes);
            }

            return BadRequest("Invalid user");
        }

        [HttpPost("Vote")]
        public async Task<IActionResult> VoteAsync(PetitionVote petitionVote)
        {
            string userLogin = User.Identity.Name;

            if(await _authRepository.UserExistAsync(userLogin))
            {
                petitionVote = await _petitionRepository.PostPetitionVotesAsync(petitionVote, userLogin);

                if(petitionVote == null)
                {
                    return BadRequest("Petition vote was not added.");
                }

                return Ok(petitionVote);
            }

            return BadRequest("Invalid user.");
        }
    }
}
