using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ForumController : ControllerBase
    {
        IForumRepository _repository;
        IAuthRepository _authRepository;

        public ForumController(IForumRepository repository, IAuthRepository authRepository)
        {
            _repository = repository;
            _authRepository = authRepository;
        }


        [HttpPost("CreateNewTopic")]
        public async Task<IActionResult> CreateNewTopic(CreateForumDto forumDto)
        {
            string userName = User.Identity.Name;

            if (!await _authRepository.UserExistAsync(userName))
            {
                return BadRequest("Invalid username");
            }

            Forum newForumTopic = await _repository.CreateNewTopicAsync(forumDto, userName);

            if (newForumTopic == null)
            {
                return BadRequest($"Can't add new forum Topic '{forumDto.Title}'");
            }
            
            ForumMessageWithLoginsAndMediaLinks forumMessage = new ForumMessageWithLoginsAndMediaLinks()
            {
                ForumId = newForumTopic.Id,
                AuthorMessageId = newForumTopic.AuthorId,
                Message = forumDto.Message,
                Date = DateTime.Now
            };

            ForumMessage message = await _repository.SendMessageAsync(forumMessage, userName);

            if(message == null)
            {
                return BadRequest($"Can't add message to Topic '{forumDto.Title}'");
            }

            if(!await _repository.MarkMessageAsMainAsync(message))
            {
                return BadRequest($"Can't mark message as maint for Topic '{forumDto.Title}'");
            }

            return Ok(newForumTopic);
        }

        [HttpGet("GetTopicData/{id}")]
        public async Task<IActionResult> GetTopicData(int id)
        {
            string userName = User.Identity.Name;

            if (!await _authRepository.UserExistAsync(userName))
            {
                return BadRequest("Invalid username");
            }

            ForumDto forumTopic = await _repository.GetTopicDataAsync(id);

            if (forumTopic == null)
            {
                return BadRequest($"Can't get forum data by Id '{id}'");
            }

            return Ok(forumTopic);
        }

        [HttpGet("GetHotForumTopics/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetHotForumTopics(int pageSize, int pageNumber)
        {
            List<Forum> forumTopics = await _repository.GetForumTopicsAsync(pageSize, pageNumber);

            if (forumTopics == null)
            {
                BadRequest("Can't add new forum topic");
            }

            return Ok(forumTopics);
        }

        [HttpGet("GetForumTopics/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetForumTopics(int pageSize, int pageNumber)
        {
            List<Forum> forumTopics = await _repository.GetForumTopicsAsync(pageSize, pageNumber);

            if (forumTopics == null)
            {
                BadRequest("Can't add new forum topic");
            }

            return Ok(forumTopics);
        }

        [HttpGet("GetForumMessages/{forumID}/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetForumMessages(int forumId, int pageSize, int pageNumber)
        {
            ForumDto forum = await _repository.GetTopicDataAsync(forumId);

            if (forum == null)
            {
                BadRequest($"Topic #{forumId} doesn't exist");
            }

            ForumMessageDto forumMessages = _repository.GetForumMessages(forum.FromForumDtoToForum(forum), pageSize, pageNumber);

            if (forumMessages == null)
            {
                BadRequest($"Topic #{forumId} doesn't contain any message/comment");
            }

            return Ok(forumMessages);
        }
        
        [HttpPost("SendMessage")]
        public async Task<IActionResult> SendMessage(ForumMessageWithLoginsAndMediaLinks message)
        {
            message.Date = DateTime.Now;
            string userName = User.Identity.Name;

            if (!await _authRepository.UserExistAsync(userName))
            {
                return BadRequest("Invalid username");
            }

            ForumMessage returnedMessage = await _repository.SendMessageAsync(message, userName);

            if (returnedMessage == null)
            {
                return BadRequest($"Can't send new message Topic #'{message.ForumId}'");
            }
        
            return Ok(returnedMessage);
        }
    }
}