using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;
using VinnytsiaCard.API.Services;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IConfiguration _config;
        private IProfileRepository _repository;

        public ProfileController(IConfiguration config, IHttpContextAccessor accessor, IProfileRepository repository)
        {
            _config = config;
            _repository = repository;
        }

        [HttpGet("UserData")]
        public IActionResult GetUserData()
        {
            string userName = User.Identity.Name;

            if (userName != null)
            {
                return Ok(_repository.GetUserDataAsync(userName).Result);
            }

            return BadRequest();
        }

        [HttpPost("UpdateUser")]
        public IActionResult UpdateUserData(UserUpdateDto userUpdateDTO)
        {
            string userName = User.Identity.Name;

            if (userName != null)
            {
                return Ok(_repository.UpdateUserDataAsync(userName, userUpdateDTO).Result);
            }

            return BadRequest();
        }

        [HttpGet("GetUserMainImage")]
        public async Task<IActionResult> GetUserMainImage()
        {
            string userName = User.Identity.Name;

            if (userName != null)
            {
                Image image = await _repository.GetUserMainImageAsync(userName);
                return Ok(image);
            }

            return BadRequest();
        }

        [HttpPost("UpdateUserImage")]
        public async Task<IActionResult> UpdateUserImage(IFormFile file)
        {
            if (file != null)
            {
                string userName = User.Identity.Name;
                Image image;

                if (userName != null)
                {
                    image = await _repository.UpdateUserMainImageAsync(userName, file);
                    return Ok(image);
                }
            }

            return BadRequest();
        }
    }
}