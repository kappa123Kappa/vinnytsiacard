using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CinemaController : ControllerBase
    {
        private IHttpClientFactory _clientFactory;
        private ICinemaRepository _cinemaRepository;
        public CinemaController(IHttpClientFactory clientFactory, ICinemaRepository cinemaRepository)
        {
            _clientFactory = clientFactory;
            _cinemaRepository = cinemaRepository;
        }

        [HttpPost("getMovies")]
        public async Task<IActionResult> GetMovies(SmartCinemaGetMoviesRequestPayload requestPayload)
        {
            SmartCinemaGetMoviesResponse response = await _cinemaRepository.GetMoviesAsync(requestPayload);

            if(response == null)
            {
                return BadRequest("Can't get movies");
            }

            return Ok(response);
        }

    }
}