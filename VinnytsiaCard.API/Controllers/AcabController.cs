using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AcabController : ControllerBase
    {
        IAcabRepository _acabRepository;
        IAuthRepository _authRepository;

        public AcabController(IAcabRepository acabRepository, IAuthRepository authRepository)
        {
            this._acabRepository = acabRepository;
            this._authRepository = authRepository;
        }

        [HttpGet("getAllAcabs/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetAllAcabs(int pageSize, int pageNumber)
        {
            GetAllAcabResponse allAcabResponse = await _acabRepository.GetAllAcabsAsync(pageSize, pageNumber);

            if (allAcabResponse == null)
            {
                return BadRequest("Can't retrieve all ACABs");
            }

            return Ok(allAcabResponse);
        }

        [HttpPost("getSearchAllAcabs")]
        public async Task<IActionResult> GetSearchAllAcabs(GetSearchAllAcabs searchAllAcabs)
        {
            GetAllAcabResponse allAcabResponse;

            if(!string.IsNullOrEmpty(searchAllAcabs.Text))
            {
                allAcabResponse = await _acabRepository.GetAllAcabsAsync(searchAllAcabs);
            }
            else
            {
                allAcabResponse = await _acabRepository.GetAllAcabsAsync(searchAllAcabs.PageSize, searchAllAcabs.PageNumber);
            }
            

            if (allAcabResponse == null)
            {
                return BadRequest("Can't retrieve all ACABs");
            }

            return Ok(allAcabResponse);
        }
        

        [HttpPost("addAcab")]
        public async Task<IActionResult> AddAcab(Acab newAcab)
        {
            Acab savedAcab = await _acabRepository.AddAcabAsync(newAcab);

            if (savedAcab == null)
            {
                return BadRequest($"Can't save new ACAB '{savedAcab.AcabName}'");
            }

            return Ok(savedAcab);
        }

        [HttpPost("addAcabAndWithApartment")]
        public async Task<IActionResult> AddAcabAndWithApartment(AddNewAcabApartmentDto newAcabApartment)
        {
            // Check user
            string userLogin = User.Identity.Name;
            if (!await _authRepository.UserExistAsync(userLogin))
            {
                return BadRequest($"Invalid user");
            }

            // Add street
            Street newStreet = new Street();
            newStreet.StreetName = newAcabApartment.Street;
            newStreet = await _acabRepository.AddStreetAsync(newStreet);
            if (newStreet == null)
            {
                return BadRequest($"Can't save new street '{newStreet.StreetName}'");
            }

            // Add building
            Building newBuilding = new Building()
            {
                StreetId = newStreet.Id,
                BuildingNumber = newAcabApartment.Building,
                CountOfApartments = newAcabApartment.CountOfApartments
            };
            newBuilding = await _acabRepository.AddBuildingAsync(newBuilding);
            if (newBuilding == null)
            {
                return BadRequest($"Can't save new building '{newBuilding.BuildingNumber}'");
            }

            // Add ACAB
            newAcabApartment.RegistrationDate = newAcabApartment.RegistrationDate.Replace("T", " ");
            Acab newAcab = new Acab()
            {
                AcabId = newAcabApartment.Id,
                BuildingId = newBuilding.Id,
                AcabName = newAcabApartment.AcabName,
                RegistrationDate = DateTime.ParseExact(newAcabApartment.RegistrationDate, "yyyy-MM-dd HH:mm:ss", null)
            };
            
            newAcab = await _acabRepository.AddAcabAsync(newAcab);

            if (newAcab == null)
            {
                return BadRequest($"Can't save new ACAB");
            }

            Apartment newApartment = new Apartment()
            {
                AcabId = newAcab.AcabId,
                ApartmentNumber = newAcabApartment.UserApartmentNumber,
            };

            newApartment = await _acabRepository.AddApartmentAsync(newApartment, userLogin);

            if (newApartment == null)
            {
                return BadRequest($"Can't save new apartment #'{newApartment.ApartmentNumber}'");
            }
            newAcabApartment.Id = newApartment.AcabId;

            return Ok(newAcabApartment);
        }


        [HttpGet("getAcabById/{id}")]
        public async Task<IActionResult> GetAcabById(int id)
        {
            Acab acab = await _acabRepository.GetAcabByIdAsync(id);
            AddNewAcabApartmentDto acabApartmentDto = new AddNewAcabApartmentDto();

            if (acab != null)
            {
                Building building = await _acabRepository.GetBuildingByIdAsync(acab.BuildingId);

                if (building != null)
                {
                    Street street = await _acabRepository.GetStreetByIdAsync(building.StreetId);

                    if (street != null)
                    {
                        List<int> allApartmentsNumbersList = Enumerable.Range(1, building.CountOfApartments).ToList();
                        List<int> unavailableApartmentsNumbers = _acabRepository.GetUnavailableApartmentsNumbers(acab.AcabId);
                        var t = allApartmentsNumbersList.Except(unavailableApartmentsNumbers).ToList();
                        acabApartmentDto = new AddNewAcabApartmentDto()
                        {
                            Id = acab.AcabId,
                            Street = street.StreetName,
                            Building = building.BuildingNumber,
                            AcabName = acab.AcabName,
                            RegistrationDate = acab.RegistrationDate.ToString("yyyy-MM-dd HH:mm:ss"),
                            CountOfApartments = building.CountOfApartments,
                            AvaliableApartments = t
                        };

                        return Ok(acabApartmentDto);
                    }
                }
            }
            
            return Ok(null);
        }

        [HttpGet("getUserApartments")]
        public async Task<IActionResult> GetUserApartments()
        {
            string userLogin = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userLogin))
            {
                List<ApartmentDto> apartments = await _acabRepository.GetUserApartmentsAsync(userLogin);

                return Ok(apartments);
            }

            return BadRequest($"User 'userLogin' does not exis");
        }

        [HttpGet("getApartment/{acabId}")]
        public IActionResult GetApartment(int acabId)
        {
            List<Apartment> apartments = _acabRepository.GetApartmentsByAcabId(acabId);
            if (apartments == null)
            {
                return BadRequest($"Apartments for ACAB #{acabId} do not exist.");
            }

            return Ok(apartments);
        }

        [HttpPost("addApartment")]
        public async Task<IActionResult> AddApartment(Apartment newApartment)
        {
            string userLogin = User.Identity.Name;
            Apartment apartments = new Apartment();

            if (await _authRepository.UserExistAsync(userLogin))
            {
                apartments = await _acabRepository.AddApartmentAsync(newApartment, userLogin);
            }

            if (apartments == null)
            {
                return BadRequest($"Can't save new Apartment for ACAB #{newApartment.AcabId}");
            }

            return Ok(apartments);
        }

    }
}