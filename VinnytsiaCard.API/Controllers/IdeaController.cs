using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class IdeaController : ControllerBase
    {
        IAuthRepository _authRepository;
        IIdeaRepository _ideaRepository;

        public IdeaController(IAuthRepository authRepository, IIdeaRepository ideaRepository)
        {
            _authRepository = authRepository;
            _ideaRepository = ideaRepository;
        }

        [HttpGet("GetIdeas/{pageSize}/{pageNumber}")]
        public async Task<IActionResult> GetIdeasAsync(int pageSize, int pageNumber)
        {
            string userLogin = User.Identity.Name;
            GetIdeaDto ideas = new GetIdeaDto();

            if(await _authRepository.UserExistAsync(userLogin))
            {
                ideas = await _ideaRepository.GetAcabsIdeasAsync(pageSize, pageNumber, userLogin);

                if(ideas == null)
                {
                    return BadRequest("Cant receive ideas");
                }

                return Ok(ideas);
            }

            return BadRequest("Invalid user");
        }

        [HttpGet("GetIdea/{ideaId}")]
        public async Task<IActionResult> GetIdeasByIdAsync(int ideaId)
        {
            string userLogin = User.Identity.Name;
            IdeaDto ideas = new IdeaDto();

            if(await _authRepository.UserExistAsync(userLogin))
            {
                ideas = await _ideaRepository.GetIdeaByIdAsync(ideaId);

                if(ideas == null)
                {
                    return BadRequest($"Cant receive idea #{ideaId}");
                }

                return Ok(ideas);
            }

            return BadRequest("Invalid user");
        }

        [HttpPost("AddIdea")]
        public async Task<IActionResult> AddIdeaAsync(Idea idea)
        {
            string userLogin = User.Identity.Name;

            if(await _authRepository.UserExistAsync(userLogin))
            {
                idea = await _ideaRepository.AddIdeaAsync(idea, userLogin);

                if(idea == null)
                {
                    return BadRequest("Idea was not added.");
                }

                return Ok(idea);
            }

            return BadRequest("Invalid user.");
        }

        [HttpGet("GetIdeaVotes/{ideaId}")]
        public async Task<IActionResult> GetIdeasVotes(int ideaId)
        {
            string userLogin = User.Identity.Name;
            IdeaVotesDto ideaVotes = new IdeaVotesDto();

            if(await _authRepository.UserExistAsync(userLogin))
            {
                ideaVotes = await _ideaRepository.GetIdeaVotesAsync(ideaId, userLogin);

                if(ideaVotes == null)
                {
                    return BadRequest($"Cant receive votes of idea #{ideaId}");
                }

                return Ok(ideaVotes);
            }

            return BadRequest("Invalid user");
        }

        [HttpPost("Vote")]
        public async Task<IActionResult> VoteAsync(IdeaVote ideaVote)
        {
            string userLogin = User.Identity.Name;

            if(await _authRepository.UserExistAsync(userLogin))
            {
                ideaVote = await _ideaRepository.PostIdeaVotesAsync(ideaVote, userLogin);

                if(ideaVote == null)
                {
                    return BadRequest("Idea vote was not added.");
                }

                return Ok(ideaVote);
            }

            return BadRequest("Invalid user.");
        }
    }
}