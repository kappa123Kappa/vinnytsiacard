using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;
using VinnytsiaCard.Models;
using VinnytsiaCard.Options;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private IAuthRepository _repository;

        public AuthController(IConfiguration config, IHttpContextAccessor accessor, IAuthRepository repository)
        {
            _config = config;
            _repository = repository;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegisterDto userRegisterDto)
        {
            // validate request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userRegisterDto.Password != userRegisterDto.Password2)
            {
                return BadRequest(ModelState);
            }

            userRegisterDto.Login = userRegisterDto.Login.ToLower();

            if (await _repository.UserExistAsync(userRegisterDto.Login))
            {
                return BadRequest("User already exists");
            }

            var createdUser = await _repository.RegisterAsync(userRegisterDto);
            UserLoginDto userLoginDto = new UserLoginDto
            {
                Username = userRegisterDto.Login,
                Password = userRegisterDto.Password,
                IsRemembered = true
            };

            return await Login(userLoginDto);
        }


        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login(UserLoginDto userLoginDto)
        {
            var userFromRepository = await _repository.LoginAsync(userLoginDto.Username, userLoginDto.Password);

            if (userFromRepository == null)
            {
                return Unauthorized();
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userFromRepository.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepository.Login)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            SecurityTokenDescriptor tokenDescriptor;

            string userAgent = "undefined";

            if (Request.Headers.ContainsKey("User-Agent"))
            {
                userAgent = Request.Headers["User-Agent"];
            }

            if (userLoginDto.IsRemembered)
            {
                var refreshKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:RefreshToken").Value));
                tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddHours(AuthOptions.LIFETIME),
                    SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                };

                var refreshTokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddMonths(AuthOptions.LIFETIME),
                    SigningCredentials = new SigningCredentials(refreshKey, SecurityAlgorithms.HmacSha256Signature)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var refreshToken = tokenHandler.CreateToken(refreshTokenDescriptor);

                RefreshToken newRefreshToken = new RefreshToken
                {
                    Login = userLoginDto.Username,
                    RefreshTokenString = tokenHandler.WriteToken(refreshToken),
                    Browser = userAgent,
                    Date = DateTime.Now
                };

                await _repository.AddRefreshTokenAsync(newRefreshToken);

                return Ok(new
                {
                    username = userLoginDto.Username,
                    token = tokenHandler.WriteToken(token),
                    refreshToken = tokenHandler.WriteToken(refreshToken),
                });
            }
            else
            {
                tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddMinutes(60),
                    SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return Ok(new
                {
                    username = userLoginDto.Username,
                    token = tokenHandler.WriteToken(token)
                });
            }
        }

        [AllowAnonymous]
        [HttpPost("refreshTokens")]
        public async Task<IActionResult> RefreshTokens()
        {
            var refreshToken = Request.Headers.FirstOrDefault(x => x.Key == "refreshToken").Value.FirstOrDefault();
            var username = Request.Headers.FirstOrDefault(x => x.Key == "username").Value.FirstOrDefault();

            if (await _repository.RefreshTokenExistAsync(username, refreshToken))
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
                var refreshKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:RefreshToken").Value));
                var userAgent = "undefined";
                RefreshToken newRefreshToken = new RefreshToken();

                if (Request.Headers.ContainsKey("User-Agent"))
                {
                    userAgent = Request.Headers["User-Agent"];
                }

                var newRefreshTokens = await _repository.RefreshTokensAsync(username, key, refreshKey, newRefreshToken);
                var tokenHandler = new JwtSecurityTokenHandler();

                return Ok(new
                {
                    username = username,
                    token = tokenHandler.WriteToken(newRefreshTokens.Item1),
                    refreshToken = tokenHandler.WriteToken(newRefreshTokens.Item2),
                });
            }

            return StatusCode(201);
        }

        [AllowAnonymous]
        [HttpPost("updateUserPassword")]
        public async Task<IActionResult> updateUserPassword(UpdatePasswordDto updatePassword)
        {
            string userName = User.Identity.Name;

            if (await _repository.UserExistAsync(userName))
            {
                if (updatePassword != null)
                {
                    updatePassword = await _repository.UpdateUserPasswordAsync(updatePassword, userName);

                    if (updatePassword != null)
                    {
                        return Ok(updatePassword);
                    }
                }
            }

            return BadRequest("Something went wrong");
        }

        [AllowAnonymous]
        [HttpPost("sendMessageForUpdatingPassword")]

        public async Task<IActionResult> sendMessageForUpdatingPassword(UpdatePassword updatePasssword)
        {

            if (updatePasssword != null)
            {
                if (await _repository.EmailExistAsync(updatePasssword.Email))
                {
                    if (await _repository.CreateLinkForUpdatingPasswordAsync(updatePasssword.Email))
                    {
                        return Ok(true);
                    }
                }
            }


            return BadRequest("Invalid e-mail");
        }

        [HttpGet("getUsersLogins")]
        public async Task<IActionResult> GetUsersLogins()
        {
            string userName = User.Identity.Name;
            List<LoginsIntoSystem> usersLogins = await _repository.GetUsersLoginsAsync(userName);

            if (usersLogins != null)
            {
                return Ok(usersLogins);
            }

            return BadRequest("Can't select users logins");
        }
    }
}