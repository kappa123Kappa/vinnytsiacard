using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using VinnytsiaCard.API.Repositories;
using VinnytsiaCard.Models;
using Microsoft.Extensions.Configuration;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IConfiguration _config;

        public HomeController(IConfiguration config, IHttpContextAccessor accessor)
        {
            _config = config;
        }

        
		[HttpGet]
		[HttpPost("ping")]
		public bool Ping(){
			return User.Identity.Name != null;
		}
    }
}