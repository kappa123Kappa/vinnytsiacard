using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticController : ControllerBase
    {
        IAuthRepository _authRepository;
        IStatisticRepository _statisticRepository;


        public StatisticController(IAuthRepository authRepository, IStatisticRepository statisticRepository)
        {
            _authRepository = authRepository;
            _statisticRepository = statisticRepository;
        }

        [HttpGet("GetPaymentStatistic/{recipientId}/{year}")]
        public async Task<IActionResult> GetPaymentStatistic(int recipientId, int year)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<StatisticPaymentDto> statisticPaymentDtoList = _statisticRepository.GetStatisticPayment(recipientId, year);

                if (statisticPaymentDtoList != null)
                {
                    return Ok(statisticPaymentDtoList);
                }
            }

            return BadRequest("Can't find data for statistic");
        }

        [HttpGet("GetPopulationStatistic/{year}")]
        public async Task<IActionResult> GetPopulationStatistic(int year)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<PopulationDto> statisticPaymentDtoList = await _statisticRepository.GetStatisticPopulationAsync(year);

                if (statisticPaymentDtoList != null)
                {
                    return Ok(statisticPaymentDtoList);
                }
            }

            return BadRequest("Can't find data for statistic");
        }

        [HttpGet("GetPopulationStableStatistic/{year}")]
        public async Task<IActionResult> GetPopulationStableStatistic(int year)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<PopulationStable> statisticPaymentDtoList = await _statisticRepository.GetStatisticPopulationStableAsync(year);

                if (statisticPaymentDtoList != null)
                {
                    return Ok(statisticPaymentDtoList);
                }
            }

            return BadRequest("Can't find data for statistic");
        }

        [HttpGet("GetSalariesStatistic/{year}")]
        public async Task<IActionResult> GetSalariesStatistic(int year)
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<SalaryDto> statisticSalariesDtoList = await _statisticRepository.GetSalariesStatisticAsync(year);

                if (statisticSalariesDtoList != null)
                {
                    return Ok(statisticSalariesDtoList);
                }
            }

            return BadRequest("Can't find data for statistic");
        }

        [HttpGet("GetFoodStatistic")]
        public async Task<IActionResult> GetFoodStatistic()
        {
            string userName = User.Identity.Name;

            if (await _authRepository.UserExistAsync(userName))
            {
                List<FoodStatisticDto> statisticfoodStatisticDtoList = await _statisticRepository.GetFoodStatisticAsync();

                if (statisticfoodStatisticDtoList != null)
                {
                    return Ok(statisticfoodStatisticDtoList);
                }
            }

            return BadRequest("Can't find data for statistic");
        }

        
    }
}