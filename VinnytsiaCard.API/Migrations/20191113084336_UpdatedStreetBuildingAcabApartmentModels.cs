﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinnytsiaCard.API.Migrations
{
    public partial class UpdatedStreetBuildingAcabApartmentModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountOfApartments",
                table: "Acabs");

            migrationBuilder.DropColumn(
                name: "StreetId",
                table: "Acabs");

            migrationBuilder.AddColumn<int>(
                name: "CountOfApartments",
                table: "Buildings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StreetId",
                table: "Buildings",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountOfApartments",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "StreetId",
                table: "Buildings");

            migrationBuilder.AddColumn<int>(
                name: "CountOfApartments",
                table: "Acabs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StreetId",
                table: "Acabs",
                nullable: false,
                defaultValue: 0);
        }
    }
}
