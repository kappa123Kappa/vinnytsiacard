﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinnytsiaCard.API.Migrations
{
    public partial class UpdatedAdditionalBillModel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EnterpriseId",
                table: "AdditionalBill",
                newName: "EnterpriseServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EnterpriseServiceId",
                table: "AdditionalBill",
                newName: "EnterpriseId");
        }
    }
}
