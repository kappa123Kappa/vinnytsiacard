﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinnytsiaCard.API.Migrations
{
    public partial class UpdatedUserBillModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "BillNumber",
                table: "UsersBills",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "BillName",
                table: "UsersBills",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "BillNumber",
                table: "UsersBills",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "BillName",
                table: "UsersBills",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
