﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinnytsiaCard.API.Migrations
{
    public partial class UpdatedEnterpriseModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "EnterpriseTypeId",
                table: "Enterprises",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "EnterpriseTypeId",
                table: "Enterprises",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
