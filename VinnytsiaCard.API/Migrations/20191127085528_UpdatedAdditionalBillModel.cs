﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinnytsiaCard.API.Migrations
{
    public partial class UpdatedAdditionalBillModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BillName",
                table: "AdditionalBill");

            migrationBuilder.DropColumn(
                name: "BillType",
                table: "AdditionalBill");

            migrationBuilder.DropColumn(
                name: "Enterprise",
                table: "AdditionalBill");

            migrationBuilder.DropColumn(
                name: "Service",
                table: "AdditionalBill");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BillName",
                table: "AdditionalBill",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BillType",
                table: "AdditionalBill",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Enterprise",
                table: "AdditionalBill",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Service",
                table: "AdditionalBill",
                nullable: true);
        }
    }
}
