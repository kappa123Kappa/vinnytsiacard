using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NewsAPI.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface INewsRepository
    {
        Task<ArticlesResult> GetNewsAsync(EverythingRequest request, IConfiguration config);
    }
}