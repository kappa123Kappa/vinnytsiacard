using System.Collections.Generic;
using System.Threading.Tasks;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IAcabRepository
    {
        Task<GetAllAcabResponse> GetAllAcabsAsync(int pageSize, int pageNumber);
        Task<GetAllAcabResponse> GetAllAcabsAsync(GetSearchAllAcabs searchAllAcabs);
        Task<Street> AddStreetAsync(Street newStreet);
        Task<Building> AddBuildingAsync(Building newBuilding);
        Task<Acab> AddAcabAsync(Acab newAcab);
        Task<Street> GetStreetByIdAsync(int id);
        Task<Building> GetBuildingByIdAsync(int id);
        Task<Acab> GetAcabByIdAsync(int id);
        Task<bool> IsAcabExistAsync(int acabId);
        Task<List<ApartmentDto>> GetUserApartmentsAsync(string login);
        List<Apartment> GetApartmentsByAcabId(int acabId);
        Task<List<Apartment>> GetUserApartmentsByAcabIdAsync(int acabId, string login);
        List<int> GetUnavailableApartmentsNumbers(int acabId);
        Task<Apartment> AddApartmentAsync(Apartment newApartment, string login);
    }
}