using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Services;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
    /*
		Repository for managing users profiles.
		Contains all methods for performing basic functionality.
	*/
    /// <summary>
    /// Repository for managing users profiles.
    /// Contains all methods for performing basic functionality.
    /// </summary>
    public class ProfileRepository : IProfileRepository
    {
        private VinnytsiaCardContext _vinnytsiaCardContext;
        private IConfiguration _config;

        public ProfileRepository(VinnytsiaCardContext vinnytsiaCardContext, IHttpContextAccessor accessor, IConfiguration config)
        {
            _vinnytsiaCardContext = vinnytsiaCardContext;
            _config = config;
        }

        /// <summary>
        /// Retrieves users main image by users login.
        /// </summary>
        /// <returns>
        /// Task User
        /// </returns>
        /// <param name="userName">Login of user</param>
        public async Task<Image> GetUserMainImageAsync(string userName)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == userName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't fined user: {ex.Message}");
            }

            if (user != null)
            {
                try
                {
                    return (from usersImages in _vinnytsiaCardContext.UsersImages
                            from users in _vinnytsiaCardContext.Users
                            from images in _vinnytsiaCardContext.Images
                            from imagesTypes in _vinnytsiaCardContext.ImagesTypes
                            where users.Id == user.Id && usersImages.UserId == users.Id
                                  && usersImages.ImageId == images.Id && images.ImageTypeId == imagesTypes.Id
                                  && imagesTypes.ImageTypeName == AmazonService.ImagesTypes.UserMainImage.ToString()
                            select images).FirstOrDefault() ?? new Image();
                }
                catch (Exception ex) 
                {
                    throw new Exception($"Can't find users main image: {ex.Message}");
                }
            }

            return new Image();
        }

        /// <summary>
        /// Retrieves users data by users login.
        /// </summary>
        /// <returns>
        /// Task User
        /// </returns>
        /// <param name="userName">Login of user</param>
        public async Task<User> GetUserDataAsync(string userName)
        {
            try
            {
                return await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == userName);
            }
            catch (ArgumentNullException exception)
            {
                throw new ArgumentNullException($"userName value({ userName }) is invalid: { exception.Message }");
            }
            catch (Exception exception)
            {
                throw new Exception($"Can't get users data using userName value({ userName }): { exception.Message }");
            }
        }

        /// <summary>
        /// Updates users data by users login.
        /// </summary>
        /// <returns>
        /// Task User
        /// </returns>
        /// <param name="userName">Login of user</param>
        public async Task<User> UpdateUserDataAsync(string userName, UserUpdateDto userUpdateDTO)
        {
            try
            {
                User userToUpdate = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == userName);
                userToUpdate.FirstName = userUpdateDTO.FirstName;
                userToUpdate.LastName = userUpdateDTO.LastName;
                userToUpdate.MiddleName = userUpdateDTO.MiddleName;
                userToUpdate.Email = userUpdateDTO.Email;
                userToUpdate.Phone = userUpdateDTO.Phone;

                await _vinnytsiaCardContext.SaveChangesAsync();

                return userToUpdate;
            }
            catch (ArgumentNullException exception)
            {
                throw new ArgumentNullException($"Invalid user name({ userName }): { exception.Message }");
            }
            catch (Exception exception)
            {
                throw new Exception($"Can't update user's data: { exception.Message }");
            }
        }

        /// <summary>
		/// Updates users image.
		/// </summary>
		/// <returns>
		/// Task Image
		/// </returns>
		/// <param name="userName">Login of user</param>
        /// <param name="file">Image file</param>
        public async Task<Image> UpdateUserMainImageAsync(string userName, IFormFile file)
        {
            User user;

            AmazonService amazonService = new AmazonService(_config);

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == userName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't fined user: {ex.Message}");
            }

            if (user != null)
            {
                // find existing user image
                UserImage oldUserImage = (from usersImages in _vinnytsiaCardContext.UsersImages
                                          from users in _vinnytsiaCardContext.Users
                                          from images in _vinnytsiaCardContext.Images
                                          from imagesTypes in _vinnytsiaCardContext.ImagesTypes
                                          where users.Id == user.Id && usersImages.UserId == users.Id
                                                && usersImages.ImageId == images.Id && images.ImageTypeId == imagesTypes.Id
                                                && imagesTypes.ImageTypeName == AmazonService.ImagesTypes.UserMainImage.ToString()
                                          select usersImages).FirstOrDefault();


                // Upload news image
                Image newImage = await amazonService
                    .UploadUserImageFileAsync(file.FileName, file.OpenReadStream());
                int addedImagesCount = 0;

                // if new image was uploaded, add image type and save image data in 'Images' table
                if (newImage != null)
                {
                    newImage.ImageTypeId = _vinnytsiaCardContext.ImagesTypes
                        .FirstOrDefault(x => x.ImageTypeName == AmazonService.ImagesTypes.UserMainImage.ToString()).Id;

                    try
                    {
                        await _vinnytsiaCardContext.Images.AddAsync(newImage);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Can't add async new image for user {userName}: {ex.Message}");
                    }

                    try
                    {
                        addedImagesCount = await _vinnytsiaCardContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Image was not saved for user {userName}: {ex.Message}");
                    }
                }

                if (addedImagesCount > 0)
                {
                    // find uploaded image data from 'Images' table
                    Image uploadedImage = (from images in _vinnytsiaCardContext.Images
                                           where images.ImageKey == newImage.ImageKey && images.ImageSrc == newImage.ImageSrc
                                           && images.ImageTypeId == newImage.ImageTypeId
                                           select images).FirstOrDefault();

                    // if UserImage entity with users main image exists, update image id in UserImage entity. Else - add new UserImage entity 
                    if (oldUserImage != null)
                    {
                        oldUserImage.ImageId = uploadedImage.Id;
                        int updatedImagesCount = 0;

                        try
                        {
                            updatedImagesCount = await _vinnytsiaCardContext.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"User main image was not updated: {ex.Message}");
                        }

                         return uploadedImage;
                    }
                    else
                    {
                        UserImage userImage = new UserImage()
                        {
                            UserId = user.Id,
                            ImageId = uploadedImage.Id
                        };

                        try
                        {
                            await _vinnytsiaCardContext.UsersImages.AddAsync(userImage);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Can't add async new image for user {userName}: {ex.Message}");
                        }

                        addedImagesCount = 0;

                        try
                        {
                            addedImagesCount = await _vinnytsiaCardContext.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Image was not saved for user {userName}: {ex.Message}");
                        }

                        return uploadedImage;
                    }
                }
            }

            return new Image();
        }
    }
}