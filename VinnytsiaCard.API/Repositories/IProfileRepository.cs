using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IProfileRepository
    {
        Task<User> GetUserDataAsync(string userName);
        Task<User> UpdateUserDataAsync(string userName, UserUpdateDto userUpdateDTO);
        Task<Image> GetUserMainImageAsync(string userName);
        Task<Image> UpdateUserMainImageAsync(string userName, IFormFile file);
	}
}