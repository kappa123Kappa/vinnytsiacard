using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing cinema.
		Contains all methods for performing basic functionality
	*/
	/// <summary>
	/// Repository for managing cinema.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class CinemaRepository : ICinemaRepository
    {
		private const string SMART_CINEMA_REQUEST_URI = "https://smartcinema.ua/api/movies/";

		private IHttpClientFactory _httpClientFactory;	

		public CinemaRepository(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

		// Retrieve movies of SmartCinema
		/// <summary>
		///  Retrieve movies of SmartCinema
		/// </summary>
		/// <returns>
		/// Task SmartCinemaGetMoviesResponse
		/// </returns>
		/// <param name="requestPayload">Request payload for retrieving movies of SmartCinema</param>
		public async Task<SmartCinemaGetMoviesResponse> GetMoviesAsync(SmartCinemaGetMoviesRequestPayload requestPayload)
        {
            string json = "";

            json = JsonConvert.SerializeObject(requestPayload);
            StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                        
            var client = _httpClientFactory.CreateClient();
			HttpResponseMessage moviesResponse;

			try
			{
				moviesResponse = await client.PostAsync(SMART_CINEMA_REQUEST_URI, content);
			}
			catch (Exception ex)
			{
				throw new Exception($"Post request for getting movies failed: { ex.Message }");
			}

			SmartCinemaGetMoviesResponse response = new SmartCinemaGetMoviesResponse();

            try
            {
                response = JObject.Parse(await moviesResponse.Content.ReadAsStringAsync()).ToObject<SmartCinemaGetMoviesResponse>();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't parse response.Content to SmartCinemaGetMoviesResponse object: { ex.Message }");
            }

            return response;
        }
    }
}