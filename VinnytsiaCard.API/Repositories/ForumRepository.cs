using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
    /*
		Repository for managing forums
		Contains all methods for performing basic functionality
	*/
    /// <summary>
    /// Repository for managing forums.
    /// Contains all methods for performing basic functionality.
    /// </summary>
    public class ForumRepository : IForumRepository
    {
        IConfiguration _config;
        private VinnytsiaCardContext _vinnytsiaCardContext;

        public ForumRepository(IConfiguration config, VinnytsiaCardContext vinnytsiaCardContext)
        {
            _vinnytsiaCardContext = vinnytsiaCardContext;
            _config = config;
        }

        // Create new forum topic 
        /// <summary>
        ///  Create new forum topic.
        /// </summary>
        /// <returns>
        /// Returns created forum object
        /// </returns>
        /// <param name="forumDto">object of new forum</param>
        /// <param name="login">Login of user</param>
        public async Task<Forum> CreateNewTopicAsync(CreateForumDto forumDto, string login)
        {
            Forum forum = new Forum()
            {
                Title = forumDto.Title,
                CreatedAt = forumDto.CreatedAt,
                AuthorId = forumDto.AuthorId,
            };

            try
            {
                forum.AuthorId = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login).Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find user with login { login }: { ex.Message }");
            }

            forum.CreatedAt = DateTime.Now;

            try
            {
                _vinnytsiaCardContext.Forums.Add(forum);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't add topic with { forum.Title }: { ex.Message }");
            }

            int savedItemsCount = 0;

            try
            {
                savedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't save topic with { forum.Title }: { ex.Message }");
            }

            if (savedItemsCount == 0)
            {
                return null;
            }

            return forum;
        }

        // Retrieve forum topic data by id
        /// <summary>
        ///  Retrieve forum topic data by id.
        /// </summary>
        /// <returns>
        /// Returns forum data by id
        /// </returns>
        /// <param name="topicId">Id of forum topic</param>
        public async Task<ForumDto> GetTopicDataAsync(int topicId)
        {
            Forum forum = await _vinnytsiaCardContext.Forums.FirstOrDefaultAsync(x => x.Id == topicId);
            ForumDto forumDto = forum.FromForumToForumDto(forum);
            forumDto.AuthorImageSrc = (from userImages in _vinnytsiaCardContext.UsersImages
                                       from images in _vinnytsiaCardContext.Images
                                       where userImages.UserId == forum.AuthorId && images.Id == userImages.ImageId
                                       select images).FirstOrDefault()?.ImageSrc ?? "";
            return forumDto;
        }

        // Send message
        /// <summary>
        ///  Retrieve forum topic data by id.
        /// </summary>
        /// <returns>
        /// Returns forum data by id
        /// </returns>
        /// <param name="forumMessage">Id of forum topic</param>
        /// <param name="login">Login of user</param>
        public async Task<ForumMessage> SendMessageAsync(ForumMessageWithLoginsAndMediaLinks forumMessage, string login)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find user '{ login }': { ex.Message }");
            }

            if (user != null)
            {
                forumMessage.AuthorMessageId = user.Id;
            }

            ForumMessage newMessage = new ForumMessage()
            {
                ForumId = forumMessage.ForumId,
                AuthorId = forumMessage.AuthorMessageId,
                Message = forumMessage.Message,
                Date = forumMessage.Date
            };

            try
            {
                _vinnytsiaCardContext.ForumMessages.Add(newMessage);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't add message to topic #{ forumMessage.ForumId }: { ex.Message }");
            }

            int savedItemsCount = 0;

            try
            {
                savedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't save message: { ex.Message }");
            }

            if (savedItemsCount == 0)
            {
                return null;
            }

            await SaveMessageCommetAsync(forumMessage);

            return newMessage;
        }

        // Send comment message
        /// <summary>
        ///  Send comment message.
        /// </summary>
        /// <returns>
        /// Returns ForumMessage object of sent message
        /// </returns>
        /// <param name="message">Comment message</param>
        private async Task<ForumMessage> SaveMessageCommetAsync(ForumMessageWithLoginsAndMediaLinks message)
        {
            ForumMessage savedMessage = await _vinnytsiaCardContext.ForumMessages.FirstOrDefaultAsync(x => x.AuthorId == message.AuthorMessageId
                && x.ForumId == message.ForumId && x.Date == message.Date);

            MessageComment messageComment = new MessageComment()
            {
                ForumId = message.ForumId,
                MessageId = savedMessage.Id,
                CommentId = message.QuoteId
            };

            await _vinnytsiaCardContext.MessagesComments.AddAsync(messageComment);
            int savedItemsCount = 0;

            try
            {
                savedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't save MessagesComments data: { ex.Message }");
            }

            if (savedItemsCount == 0)
            {
                return null;
            }

            return savedMessage;
        }

        // Marks message as main
        /// <summary>
        ///  Marks message as main.
        /// </summary>
        /// <returns>
        /// Returns true if message was marked as main
        /// </returns>
        /// <param name="message">Message for marking as main</param>
        public async Task<bool> MarkMessageAsMainAsync(ForumMessage message)
        {
            Forum savedForum = await _vinnytsiaCardContext.Forums.LastOrDefaultAsync(x => x.AuthorId == message.AuthorId);
            savedForum.MainMessageId = message.Id;

            if (await _vinnytsiaCardContext.SaveChangesAsync() > 0)
            {
                return true;
            }

            return false;
        }

        // Retrieve paginated messages
        /// <summary>
        ///  Retrieve paginated messages.
        /// </summary>
        /// <returns>
        /// Returns ForumMessageDto object
        /// </returns>
        /// <param name="forum">Forum data object</param>
        /// <param name="pageSize">Page size value</param>
        /// <param name="pageNumber">Page number value</param>
        public ForumMessageDto GetForumMessages(Forum forum, int pageSize, int pageNumber)
        {
            List<ForumMessageWithLoginsAndMediaLinks> tempForumMessages;
            ForumMessageDto forumMessageDto = new ForumMessageDto();

            try
            {
                using (IDbConnection db = new SqlConnection(_config.GetSection("ConnectionStrings:DefaultConnection").Value))
                {
                    tempForumMessages = db.Query<ForumMessageWithLoginsAndMediaLinks>(
                    $"GetForumMessages @ForumId = {forum.Id}, @PageSize = {pageSize}, @PageNumber = {pageNumber}")
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select messages for topic #{forum.Id}: {ex.Message}");
            }

            forumMessageDto.Pagination = new Pagination()
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                ItemsCount = tempForumMessages.Count
            };

            tempForumMessages.OrderBy(x => x.Date);
            var mainMassage = tempForumMessages.FirstOrDefault(x => x.Id == forum.MainMessageId);
            tempForumMessages.Remove(mainMassage);
            tempForumMessages.Insert(0, mainMassage);
            forumMessageDto.ForumMessages.AddRange(tempForumMessages);

            return forumMessageDto;
        }

        // Retrieve list of forums
        /// <summary>
        ///  Retrieve list of forums.
        /// </summary>
        /// <returns>
        /// Returns list of Forums objects
        /// </returns>
        /// <param name="pageSize">Page size value</param>
        /// <param name="pageNumber">Page number value</param>
        public async Task<List<Forum>> GetForumTopicsAsync(int pageSize, int pageNumber)
        {
            List<Forum> forumTopicsList = new List<Forum>();

            try
            {
                forumTopicsList = await _vinnytsiaCardContext.Forums.Skip(pageSize * pageNumber).Take(pageSize).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select topics from { pageSize * pageNumber } to " +
                        $"{ (pageSize * pageNumber) + pageSize }: { ex.Message }");
            }

            return forumTopicsList;
        }
    }
}