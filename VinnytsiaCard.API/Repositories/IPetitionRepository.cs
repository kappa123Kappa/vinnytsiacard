using System.Collections.Generic;
using System.Threading.Tasks;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IPetitionRepository
    {
        GetPetitionDto GetPetitions(int pageSize, int pageNumber);
        Task<PetitionDto> GetPetitionByIdAsync(int petitionId);
        Task<Petition> AddPetitionAsync(Petition petition, string userName);
        Task<PetitionVotesDto> GetPetitionVotes(int petitionId, string login);
        Task<PetitionVote> PostPetitionVotesAsync(PetitionVote petitionVote, string login);
        Task<bool> IsVoteExistAsync(PetitionVote petitionVote);
    }
}