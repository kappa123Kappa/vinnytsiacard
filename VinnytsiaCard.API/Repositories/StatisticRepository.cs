using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing statistic.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing statistic.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class StatisticRepository : IStatisticRepository
    {
        IConfiguration _config;
        private VinnytsiaCardContext _vinnytsiaCardContext;

        public StatisticRepository(VinnytsiaCardContext vinnytsiaCardContext, IConfiguration config)
        {
            _vinnytsiaCardContext = vinnytsiaCardContext;
            _config = config;
        }

		/// <summary>
		/// Retrieves statistic about salaries by year.
		/// </summary>
		/// <returns>
		/// List of SalaryDto
		/// </returns>
		/// <param name="year">number of year</param>
		public async Task<List<SalaryDto>> GetSalariesStatisticAsync(int year)
        {
            try
            {
                return await (from salaries in _vinnytsiaCardContext.Salaries
                        from activitiesTypes in _vinnytsiaCardContext.ActivitiesTypes
                        where salaries.Year == year && activitiesTypes.Id == salaries.ActivityId
                        select new SalaryDto()
                        {
                            Id = salaries.Id,
                            ActivityId = salaries.ActivityId,
                            ActivityName = activitiesTypes.ActivityTypeName,
                            SalaryValue = salaries.SalaryValue,
                            Month = salaries.Month,
                            Year = salaries.Year
                        }).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select population statistic for {year} year: {ex.Message}");
            }
        }

		/// <summary>
		/// Retrieves statistic about payments by year and id of recipient.
		/// </summary>
		/// <returns>
		/// List of StatisticPaymentDto
		/// </returns>
		/// <param name="recipientId">Id of recipient</param>
		/// <param name="year">number of year</param>
		public List<StatisticPaymentDto> GetStatisticPayment(int recipientId, int year)
        {
            List<StatisticPaymentDto> statisticPaymentDto;

            try
            {
                using (IDbConnection db = new SqlConnection(_config.GetSection("ConnectionStrings:DefaultConnection").Value))
                {
                    statisticPaymentDto = db.Query<StatisticPaymentDto>(
                    $"dbo.GetPaymentsSumByRecipientIdYear @RecipientId  = {recipientId}, @Year = {year}")
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select payments for recipient #{recipientId}: {ex.Message}");
            }

            return statisticPaymentDto;
        }

		/// <summary>
		/// Retrieves statistic about population by year.
		/// </summary>
		/// <returns>
		/// List of PopulationDto
		/// </returns>
		/// <param name="year">number of year</param>
		public async Task<List<PopulationDto>> GetStatisticPopulationAsync(int year)
        {
            try
            {
                return await (from populations in _vinnytsiaCardContext.Populations
                        from cities in _vinnytsiaCardContext.Cities
                        where populations.Date.Year == year && cities.Id == populations.CityId
                        select new PopulationDto()
                        {
                            Id = populations.Id,
                            CityId = populations.CityId,
                            CityName = cities.CityName,
                            Count = populations.Count,
                            Date = populations.Date
                        }).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select population statistic for {year} year: {ex.Message}");
            }
        }

		/// <summary>
		/// Retrieves statistic about stable population by year.
		/// </summary>
		/// <returns>
		/// List of PopulationStable
		/// </returns>
		/// <param name="year">number of year</param>
		public async Task<List<PopulationStable>> GetStatisticPopulationStableAsync(int year)
        {
            try
            {
                return await _vinnytsiaCardContext.PopulationsStable.Where(x => x.Date.Year == year).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select population statistic for {year} year: {ex.Message}");
            }
        }

		/// <summary>
		/// Retrieves statistic about food consumption.
		/// </summary>
		/// <returns>
		/// List of FoodStatisticDto
		/// </returns>
		public async Task<List<FoodStatisticDto>> GetFoodStatisticAsync()
        {
            try
            {
                return await (from foodStatistics in _vinnytsiaCardContext.FoodStatistics
                        from foodTypes in _vinnytsiaCardContext.FoodTypes
                        where foodStatistics.FoodTypeId == foodTypes.Id
                        select new FoodStatisticDto()
                        {
                            Id = foodStatistics.Id,
                            FoodTypeId = foodStatistics.FoodTypeId,
                            FoodTypeName = foodTypes.FoodTypeName,
                            FoodValue = foodStatistics.FoodValue,
                            Year = foodStatistics.Year
                        }).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select food statistic: {ex.Message}");
            }
        }
    }
}