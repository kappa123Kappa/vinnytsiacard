using System.Collections.Generic;
using System.Threading.Tasks;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IForumRepository
    {
        Task<Forum> CreateNewTopicAsync(CreateForumDto forumDto, string userName);
        Task<ForumDto> GetTopicDataAsync(int topicId);
        Task<List<Forum>> GetForumTopicsAsync(int pageSize, int pageNumber);
        Task<ForumMessage> SendMessageAsync(ForumMessageWithLoginsAndMediaLinks forumMessage, string login);
        Task<bool> MarkMessageAsMainAsync(ForumMessage message);
        ForumMessageDto GetForumMessages(Forum forum, int pageSize, int pageNumber);
    }
}