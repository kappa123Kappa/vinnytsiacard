using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MimeKit;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.Models;
using VinnytsiaCard.Options;

namespace VinnytsiaCard.API.Repositories
{
    /*
		Repository for managing authorization
		Contains all methods for performing basic functionality
	*/
    /// <summary>
    /// Repository for managing authorization.
    /// Contains all methods for performing basic functionality.
    /// </summary>
    public class AuthRepository : IAuthRepository
    {
        private VinnytsiaCardContext _vinnytsiaCardContext;
        private IHttpContextAccessor _accessor;
        IConfiguration _config;

        public AuthRepository(VinnytsiaCardContext vinnytsiaCardContext, IHttpContextAccessor accessor, IConfiguration config)
        {
            _vinnytsiaCardContext = vinnytsiaCardContext;
            _accessor = accessor;
            _config = config;
        }

        // Registration of new user
        /// <summary>
        /// Registration of new user.
        /// </summary>
        /// <returns>
        /// Task User
        /// </returns>
        /// <param name="userRegisterDto">Object of new User</param>
        public async Task<User> RegisterAsync(UserRegisterDto userRegisterDto)
        {
            byte[] passwordHash, passwordSalt;

            User user = new User
            {
                Login = userRegisterDto.Login.ToLower(),
                Password = userRegisterDto.Password,
                FirstName = userRegisterDto.FirstName,
                LastName = userRegisterDto.LastName,
                MiddleName = userRegisterDto.MiddleName,
                Email = userRegisterDto.Email,
                Phone = userRegisterDto.Phone
            };

            CreatePasswordHash(user.Password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            await _vinnytsiaCardContext.Users.AddAsync(user);
            await _vinnytsiaCardContext.SaveChangesAsync();

            return user;
        }

        // Check if login and password is correct for existing user
        /// <summary>
        /// Check if login and password is correct for existing user.
        /// </summary>
        /// <returns>
        /// Task User 
        /// </returns>
        /// <param name="login">Login of user</param>
        /// <param name="password">Password of user</param>
        public async Task<User> LoginAsync(string login, string password)
        {
            var user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            return user;
        }

        // Update users password by users login
        /// <summary>
        /// Update users password by users login.
        /// </summary>
        /// <returns>
        /// Task UpdatePasswordDto
        /// </returns>
        /// <param name="updatePassword">UpdatePasswordDto object with users data for updating password</param>
        /// <param name="login">Login of user</param>
        public async Task<UpdatePasswordDto> UpdateUserPasswordAsync(UpdatePasswordDto updatePassword, string login)
        {
            User user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            if (VerifyPasswordHash(updatePassword.OldPassword, user.PasswordHash, user.PasswordSalt))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(updatePassword.NewPassword, out passwordHash, out passwordSalt);

                user.Password = updatePassword.NewPassword;
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                await _vinnytsiaCardContext.SaveChangesAsync();

                return updatePassword;
            }

            return null;
        }

        // Verify password hash
        /// <summary>
        ///  Verify password hash.
        /// </summary>
        /// <returns>
        /// Return true if password hash is correct
        /// </returns>
        /// <param name="password">Users password</param>
        /// <param name="passwordHash">byte array of password hash</param>
        /// <param name="passwordSalt">byte array of password salt</param>
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var compudedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                return compudedHash.SequenceEqual(passwordHash);
            }
        }

        // Create password hash
        /// <summary>
        /// Create password hash.
        /// </summary>
        /// <param name="password">Users password</param>
        /// <param name="passwordHash">byte array of password hash</param>
        /// <param name="passwordSalt">byte array of password salt</param>
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        // Check if user exists
        /// <summary>
        ///  Check if user exists.
        /// </summary>
        /// <returns>
        /// Return true if user exist
        /// </returns>
        /// <param name="username">Login of user</param>
        public async Task<bool> UserExistAsync(string username)
        {
            if (await _vinnytsiaCardContext.Users.AnyAsync(x => x.Login == username))
            {
                return true;
            }

            return false;
        }

        // Check if users email exists
        /// <summary>
        ///  Check if users email exists.
        /// </summary>
        /// <returns>
        /// Return true if users email exist
        /// </returns>
        /// <param name="email">Email of user</param>
        public async Task<bool> EmailExistAsync(string email)
        {

            if (await _vinnytsiaCardContext.Users.AnyAsync(x => x.Email == email))
            {
                return true;
            }

            return false;
        }

        // Logout from users account
        /// <summary>
        ///  Logout from users account.
        /// </summary>
        /// <param name="login">Login of user</param>
        public void Logout(string login)
        {
            throw new System.NotImplementedException();
        }

        // Add refresh token
        /// <summary>
        ///  Add refresh token.
        /// </summary>
        /// <returns>
        /// Task RefreshToken
        /// </returns>
        /// <param name="RefreshToken">RefreshToken object for addding refresh token</param>
        public async Task<RefreshToken> AddRefreshTokenAsync(RefreshToken newRefreshToken)
        {
            if (await RefreshTokenExistAsync(newRefreshToken.Login, newRefreshToken.RefreshTokenString))
            {
                throw new Exception("Refresh token already exist for this user and ip address");
            }
            else
            {
                var userIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                newRefreshToken.WhiteIP = userIP;

                await _vinnytsiaCardContext.RefreshTokens.AddAsync(newRefreshToken);
                await _vinnytsiaCardContext.SaveChangesAsync();
            }

            return newRefreshToken;
        }

        // Update refresh token
        /// <summary>
        ///  Update refresh token.
        /// </summary>
        /// <returns>
        /// Task RefreshToken
        /// </returns>
        /// <param name="RefreshToken">RefreshToken object for updating refresh token</param>
        public async Task<RefreshToken> UpdateRefreshTokenAsync(RefreshToken newRefreshToken)
        {
            var userIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var refreshTokenFromContext = await _vinnytsiaCardContext.RefreshTokens.LastOrDefaultAsync(
                    x => x.Login == newRefreshToken.Login && x.WhiteIP == userIP);
            refreshTokenFromContext.RefreshTokenString = newRefreshToken.RefreshTokenString;

            await _vinnytsiaCardContext.SaveChangesAsync();

            return newRefreshToken;
        }

        // Refreshing access token and refresh token
        /// <summary>
        ///  Refreshing access token and refresh token.
        /// </summary>
        /// <returns>
        /// Task(SecurityToken, SecurityToken)
        /// </returns>
        /// <param name="username">Login of user</param>
        /// <param name="key">SecurityKey of access key</param>
        /// <param name="refreshKey">SecurityKey of refresh key</param>
        /// <param name="newRefreshToken">SecurityKey of new refresh token</param>
        public async Task<(SecurityToken, SecurityToken)> RefreshTokensAsync(string username, SecurityKey key, SecurityKey refreshKey, RefreshToken newRefreshToken)
        {
            if (!await UserExistAsync(username))
            {
                return (null, null);
            }

            var maxUsersAuthorizationsCount = int.Parse(_config.GetSection("MaxUsersAuthorizationsCount").Value);
            var tokenHandler = new JwtSecurityTokenHandler();
            var userIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == username);
            var refreshTokenFromContext = await _vinnytsiaCardContext.RefreshTokens.FirstOrDefaultAsync(
                x => x.Login == username && x.WhiteIP == userIP
            );

            if (_vinnytsiaCardContext.RefreshTokens.Where(x => x.Login == newRefreshToken.Login).Count() > maxUsersAuthorizationsCount)
            {
                if (ValidateToken(refreshTokenFromContext.RefreshTokenString, true))
                {
                    var securityToken = GenerateToken(user, key, false);
                    var refreshSecurityToken = GenerateToken(user, key, true);
                    newRefreshToken.Login = username;
                    newRefreshToken.RefreshTokenString = tokenHandler.WriteToken(refreshSecurityToken);
                    newRefreshToken.WhiteIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    newRefreshToken.Date = DateTime.Now;
                    await UpdateRefreshTokenAsync(newRefreshToken);

                    return (securityToken, refreshSecurityToken);
                }
                else
                {
                    return (null, null);
                }
            }
            else
            {

                var securityToken = GenerateToken(user, key, false);
                var refreshSecurityToken = GenerateToken(user, key, true);
                newRefreshToken.Login = username;
                newRefreshToken.RefreshTokenString = tokenHandler.WriteToken(refreshSecurityToken);
                newRefreshToken.WhiteIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                newRefreshToken.Date = DateTime.Now;
                await AddRefreshTokenAsync(newRefreshToken);

                return (securityToken, refreshSecurityToken);
            }
        }

        // Check if refresh token exist
        /// <summary>
        ///  Check if refresh token exist.
        /// </summary>
        /// <returns>
        /// Return true if refresh token exist
        /// </returns>
        /// <param name="username">Login of user</param>
        /// <param name="refreshToken">Refresh token</param>
        public async Task<bool> RefreshTokenExistAsync(string username, string refreshToken)
        {
            var userIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var refreshTokenEntity = await _vinnytsiaCardContext.RefreshTokens.FirstOrDefaultAsync(
                x => x.Login == username && x.WhiteIP == userIP && x.RefreshTokenString == refreshToken
            );

            if (refreshTokenEntity != null)
            {
                return true;
            }

            return false;
        }

        // Generate tokens (access or refresh) for user
        /// <summary>
        ///  Generate tokens (access or refresh) for user
        /// </summary>
        /// <returns>
        /// Return SecurityToken object
        /// </returns>
        /// <param name="user">User object</param>
        /// <param name="key">SecurityKey of access token</param>
        /// <param name="isRefreshToken">Indicates if this is an refresh token</param>
        private SecurityToken GenerateToken(User user, SecurityKey key, bool isRefreshToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Login)
            };

            if (isRefreshToken)
            {
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddMonths(2),
                    SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                };
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                return securityToken;
            }
            else
            {
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddDays(2),
                    SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                };
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                return securityToken;
            }

        }

        // Generate tokens (access or refresh) for user
        /// <summary>
        ///  Generate tokens (access or refresh) for user
        /// </summary>
        /// <returns>
        /// Return SecurityToken object
        /// </returns>
        /// <param name="user">User object</param>
        /// <param name="key">SecurityKey of access token</param>
        /// <param name="isRefreshToken">Indicates if this is an refresh token</param>
        private static bool ValidateToken(string authToken, bool isResfresh)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            TokenValidationParameters validationParameters;

            if (isResfresh)
            {
                validationParameters = AuthOptions.GetValidationRefreshParameters();
            }
            else
            {
                validationParameters = AuthOptions.GetValidationParameters();
            }

            SecurityToken validatedToken;
            IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);

            return true;
        }

        // Retrieve list of logins
        /// <summary>
        ///  Retrieve list of logins
        /// </summary>
        /// <returns>
        /// Return list of logins
        /// </returns>
        /// <param name="userName">Login of user</param>
        public async Task<List<LoginsIntoSystem>> GetUsersLoginsAsync(string userName)
        {
            List<LoginsIntoSystem> usersLogins = new List<LoginsIntoSystem>();

            try
            {
                usersLogins = await _vinnytsiaCardContext.RefreshTokens.Where(x => x.Login == userName)
                        .Select(x => new LoginsIntoSystem()
                        {
                            WhiteIP = x.WhiteIP,
                            Browser = x.Browser,
                            Date = x.Date
                        }).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select logins data for user '{userName}': { ex.Message }");
            }

            try
            {
                for (int i = 0; i < usersLogins.Count(); i++)
                {
                    if (usersLogins[i]?.Browser?.Contains("Chrome") ?? false)
                    {
                        usersLogins[i].Browser = "Chrome";
                    }

                    else if (usersLogins[i]?.Browser?.Contains("Safari") ?? false)
                    {
                        usersLogins[i].Browser = "Safari";
                    }

                    if (usersLogins[i].Browser == null)
                    {
                        usersLogins[i].Browser = "-";
                    }
                }

                return usersLogins;
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new IndexOutOfRangeException($"Invalid index for usersLogins: { ex.Message }");
            }
        }

        // Generate link for updating password 
        /// <summary>
        ///  Generate link for updating password
        /// </summary>
        /// <returns>
        /// Returns true if link for updating password was created
        /// </returns>
        /// <param name="email">Email of user</param>
        public async Task<bool> CreateLinkForUpdatingPasswordAsync(string email)
        {
            MimeMessage emailMessage = new MimeMessage();
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string token = "";

            try
            {
                token = new string(Enumerable.Repeat(chars, 100)
                      .Select(s => s[random.Next(s.Length)]).ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't generate value for updating password: {ex.Message}");
            }

            if (!string.IsNullOrEmpty(token))
            {
                User user;

                try
                {
                    user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Email == email);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't find user: {ex.Message}");
                }

                emailMessage.From.Add(new MailboxAddress("Администрация сайта", "insanefury31@gmail.com"));
                emailMessage.To.Add(new MailboxAddress("", email));
                emailMessage.Subject = "Оновлення паролю для інформаціної системи 'Картка Вінничанина'";
                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = "Для оновлення паролю, перейдіть за посиланням - " + _config.GetSection("SiteUrl").Value + "/updatePassword/" + token
                };

                string mail = _config.GetSection("mail").Value;
                string password = _config.GetSection("mailPassword").Value;

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("smtp.gmail.com", 587, false);
                    await client.AuthenticateAsync(mail, password);
                    await client.SendAsync(emailMessage);

                    await client.DisconnectAsync(true);
                }

            }

            return false;
        }
    }
}