using System.Collections.Generic;
using System.Threading.Tasks;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IStatisticRepository
    {
        List<StatisticPaymentDto> GetStatisticPayment(int recipientId, int year);
        Task<List<PopulationDto>> GetStatisticPopulationAsync(int year);
        Task<List<PopulationStable>> GetStatisticPopulationStableAsync(int year);
        Task<List<SalaryDto>> GetSalariesStatisticAsync(int year);
        Task<List<FoodStatisticDto>> GetFoodStatisticAsync();
    }
}