using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Services;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing telegram bot.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing telegram bot.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class TelegramRepository : ITelegramRepository
    {
        private readonly IConfiguration _config;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly TelegramService _telegramService;

        public TelegramRepository(IConfiguration config, IHttpClientFactory httpClientFactory)
        {
            _config = config;
            _httpClientFactory = httpClientFactory;
            _telegramService = new TelegramService(_config, _httpClientFactory);
        }

		// Post message using telegram bot
		/// <summary>
		/// Post message using telegram bot.
		/// </summary>
		/// <returns>
		/// Task SendBotMessageResponse
		/// </returns>
		/// <param name="chatId">Id of chat</param>
		/// <param name="text">text of message</param>
		public async Task<SendBotMessageResponse> PostMessageBotAsync(string chatId, string text)
        {
            return await _telegramService.PostMessageBotAsync(chatId, text);
        }
    }
}