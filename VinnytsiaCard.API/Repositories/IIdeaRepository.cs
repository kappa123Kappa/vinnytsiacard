using System.Threading.Tasks;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IIdeaRepository
    {
        Task<GetIdeaDto> GetAcabsIdeasAsync(int pageSize, int pageNumber, string login);
        Task<IdeaDto> GetIdeaByIdAsync(int ideaId);
        Task<Idea> AddIdeaAsync(Idea Idea, string userName);
        Task<IdeaVotesDto> GetIdeaVotesAsync(int ideaId, string login);
        Task<IdeaVote> PostIdeaVotesAsync(IdeaVote ideaVote, string login);
        Task<bool> IsIdeaVoteExistAsync(int acabId, IdeaVote ideaVote);
    }
}