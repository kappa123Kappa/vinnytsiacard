using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Stripe;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Services;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing payments.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing payments.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class PaymentRepository : IPaymentRepository
    {
        private const string OTHER_BILL_TYPE_NAME = "Other";
        private VinnytsiaCardContext _vinnytsiaCardContext;

        private readonly IConfiguration _config;

        public PaymentRepository(IConfiguration config, VinnytsiaCardContext vinnytsiaCardContext)
        {
            _config = config;
            _vinnytsiaCardContext = vinnytsiaCardContext;
        }

		/// <summary>
		/// Retrieves list of main bills.
		/// </summary>
		/// <returns>
		/// Returns List MainBillDto
		/// </returns>
		public List<MainBillDto> GetUserMainBills()
        {
            List<MainBill> mainBillsList = _vinnytsiaCardContext.MainBills.ToList();

            List<MainBillDto> userBillsList;

            try
            {
                userBillsList = (from mainBills in mainBillsList
                                 from billsTypes in _vinnytsiaCardContext.BillTypes
                                 from enterprises in _vinnytsiaCardContext.Enterprises
                                 where billsTypes.Id == mainBills.BillTypeId && enterprises.Id == mainBills.EnterpriseId

                                 select new MainBillDto
                                 {
                                     Id = mainBills.Id,
                                     BillType = billsTypes.BillTypeName,
                                     CustomerBillId = mainBills.CustomerBillId,
                                     EnterpriseId = mainBills.EnterpriseId,
                                     Enterprise = enterprises.EnterpriseName,
                                     BillName = mainBills.BillName
                                 }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select users main bills: {ex.Message}");
            }

            return userBillsList;
        }

		/// <summary>
		/// Retrieves users list of additionals bills.
		/// </summary>
		/// <returns>
		/// Returns List of UserBillsDto
		/// </returns>
		/// <param name="login">Login of user</param>
		public async Task<List<UserBillsDto>> GetUserOtherBillsAsync(string login)
        {
            User user = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            List<BillType> billTypesList = await GetAllBillsTypesAsync();

            List<UserBillsDto> userBillsList;

            try
            {
                userBillsList = await (from usersBills in _vinnytsiaCardContext.UsersBills
                                 from billsTypes in _vinnytsiaCardContext.BillTypes
                                 where usersBills.UserId == user.Id && usersBills.BillTypeId == billsTypes.Id
                                       && billsTypes.BillTypeName == OTHER_BILL_TYPE_NAME
                                 select new UserBillsDto
                                 {
                                     Id = usersBills.Id,
                                     BillTypeId = usersBills.Id,
                                     BillTypeName = billsTypes.BillTypeName,
                                     UserId = usersBills.UserId,
                                     BillNumber = usersBills.BillNumber
                                 }).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select users other bills: {ex.Message}");
            }

            return userBillsList;
        }

		/// <summary>
		/// Retrieves users list of bills.
		/// </summary>
		/// <returns>
		/// Returns List of UserBillsDto
		/// </returns>
		/// <param name="login">Login of user</param>
		public async Task<List<UserBillsDto>> GetUserAllBills(string login)
        {
            User user = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            List<BillType> billTypesList = await GetAllBillsTypesAsync();

            List<UserBillsDto> userBillsList;

            try
            {
                userBillsList = (from usersBills in _vinnytsiaCardContext.UsersBills
                                 from billsTypes in _vinnytsiaCardContext.BillTypes
                                 where usersBills.UserId == user.Id && usersBills.BillTypeId == billsTypes.Id
                                 select new UserBillsDto
                                 {
                                     Id = usersBills.Id,
                                     BillTypeId = usersBills.Id,
                                     BillTypeName = billsTypes.BillTypeName,
                                     UserId = usersBills.UserId,
                                     BillNumber = usersBills.BillNumber
                                 }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select users all bills: {ex.Message}");
            }

            return userBillsList;
        }

		/// <summary>
		/// Retrieves list of types of bills.
		/// </summary>
		/// <returns>
		/// Returns List of BillType
		/// </returns>
		public async Task<List<BillType>> GetAllBillsTypesAsync()
        {
            try
            {
                return await _vinnytsiaCardContext.BillTypes.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't get all types of biils: {ex.Message}");
            }
        }

		/// <summary>
		/// Retrieves list of types of enterprises.
		/// </summary>
		/// <returns>
		/// Returns List of EnterpriseType
		/// </returns>
		public List<EnterpriseType> GetAllEnterprisesTypes()
        {
            try
            {
                return _vinnytsiaCardContext.EnterprisesTypes.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't get all types of enterprises: {ex.Message}");
            }
        }

		/// <summary>
		/// Retrieves enterprise by type id.
		/// </summary>
		/// <returns>
		/// Returns List of Enterprise
		/// </returns>
		/// <param name="enterpriseTypeId">Id of type of enterprise</param>
		public List<Enterprise> GetEnterprisesById(int enterpriseTypeId)
        {
            try
            {
                return _vinnytsiaCardContext.Enterprises.Where(x => x.EnterpriseTypeId == enterpriseTypeId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't get enterprises by id({enterpriseTypeId}): {ex.Message}");
            }
        }

		/// <summary>
		/// Retrieves services by enterprise id and users login.
		/// </summary>
		/// <returns>
		/// Returns List of Service
		/// </returns>
		/// <param name="enterpriseId">Id of enterprise</param>
		/// <param name="login">Login of user</param>
		public async Task<List<Service>> GetEnterpriseServiceByIdAsync(int enterpriseId, string login)
        {
            AdditionalBillDto additionalBillDto = await GetUserAdditionalBillAsync(login);

            try
            {
                if(additionalBillDto.AdditionalBillList.FirstOrDefault(x => x.EnterpriseId == enterpriseId) != null)
                {
                    return (from additionalBill in additionalBillDto.AdditionalBillList
                        from services in _vinnytsiaCardContext.Services
                        from enterprisesServices in _vinnytsiaCardContext.EnterprisesServices
                        where enterprisesServices.EnterpriseId == enterpriseId
                        && services.Id == enterprisesServices.ServiceId
                        && enterprisesServices.EnterpriseId == additionalBill.EnterpriseId
                        && services.ServiceName != additionalBill.ServiceName
                        select services).ToList();
                }
                else
                {
                    return (from services in _vinnytsiaCardContext.Services
                        from enterprisesServices in _vinnytsiaCardContext.EnterprisesServices
                        where enterprisesServices.EnterpriseId == enterpriseId
                        && services.Id == enterprisesServices.ServiceId
                        select services).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't get services by enterpriseId({enterpriseId}): {ex.Message}");
            }
        }

		/// <summary>
		/// Updates users main bill.
		/// </summary>
		/// <returns>
		/// Returns new UpdateMainBillsDto object
		/// </returns>
		/// <param name="bills">Object of new main bills</param>
		/// <param name="login">Login of user</param>
		public async Task<UpdateMainBillsDto> UpdateMainBillsAsync(UpdateMainBillsDto bills, string login)
        {
            int userId;

            try
            {
                userId = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login).Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find  user '{login}': {ex.Message}");
            }

            bills.GasBill.UserId = userId;
            bills.WaterBill.UserId = userId;
            bills.ElectricityBill.UserId = userId;

            if (await UpdateBillAsync(bills.GasBill, userId) == null || await UpdateBillAsync(bills.WaterBill, userId) == null
                || await UpdateBillAsync(bills.ElectricityBill, userId) == null)
            {
                return null;
            }

            return bills;
        }

		/// <summary>
		/// Adds new bill for user.
		/// </summary>
		/// <returns>
		/// Returns added UserBill object
		/// </returns>
		/// <param name="userBill">Object of new main bill</param>
		/// <param name="userId">Id of user</param>
		public async Task<UserBill> AddBill(UserBill userBill, int userId)
        {
            if (IsValidBill(userBill, userId))
            {
                UserBill billDb;

                try
                {
                    billDb = await _vinnytsiaCardContext.UsersBills.FirstOrDefaultAsync(x => x.Id == userBill.Id && x.BillTypeId == userBill.BillTypeId);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't find User's Gas bill: {ex.Message}");
                }

                int addedItemsCount = 0;

                if (billDb == null)
                {
                    try
                    {
                        await _vinnytsiaCardContext.UsersBills.AddAsync(userBill);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Can't add new bill for user #{userId}: {ex.Message}");
                    }

                    try
                    {
                        addedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Can't save new bill for user #{userId}: {ex.Message}");
                    }

                    if (addedItemsCount > 0)
                    {
                        return userBill;
                    }
                }
            }

            return null;
        }

		/// <summary>
		/// Adds new additional bill for user.
		/// </summary>
		/// <returns>
		/// Returns added UserAdditionalBill object
		/// </returns>
		/// <param name="addAdditionalBill">Object of new additional bill</param>
		/// <param name="login">Login of user</param>
		public async Task<UserAdditionalBill> AddUserAdditionalBillAsync(AddAdditionalBillDto addAdditionalBill, string login)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find user '{login}': {ex.Message}");
            }

            if (user != null)
            {
                EnterpriseService enterpriseService;

                try
                {
                    enterpriseService = await _vinnytsiaCardContext.EnterprisesServices.FirstOrDefaultAsync(
                    x => x.EnterpriseId == addAdditionalBill.EnterpriseId && x.ServiceId == addAdditionalBill.ServiceId);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't find service #{addAdditionalBill.ServiceId} for enterprise #{addAdditionalBill.EnterpriseId}: "
                        + $"{ex.Message}");
                }

                if (enterpriseService != null)
                {
                    AdditionalBill additional;
                    try
                    {
                        additional = await _vinnytsiaCardContext.AdditionalBill.FirstOrDefaultAsync(
                            x => x.EnterpriseServiceId == enterpriseService.Id);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Can't find additional bill: {ex.Message}");
                    }

                    if (additional != null)
                    {
                        UserAdditionalBill newUserAdditionalBill = new UserAdditionalBill()
                        {
                            UserId = user.Id,
                            AdditionaBillId = additional.Id
                        };

                        int addedItemsCount = 0;

                        try
                        {
                            await _vinnytsiaCardContext.UserAdditionalBills.AddAsync(newUserAdditionalBill);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Can't add new additional bill for user #{user.Id}: {ex.Message}");
                        }

                        try
                        {
                            addedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Can't save added additional bill for user #{user.Id}: {ex.Message}");
                        }

                        if (addedItemsCount > 0)
                        {
                            return newUserAdditionalBill;
                        }
                    }
                }
            }

            return null;
        }

		/// <summary>
		/// Retrieves additional bill.
		/// </summary>
		/// <returns>
		/// Task AdditionalBillDto
		/// </returns>
		/// <param name="login">Login of user</param>
		public async Task<AdditionalBillDto> GetUserAdditionalBillAsync(string login)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find user '{login}': {ex.Message}");
            }

            if (user != null)
            {
                AdditionalBillDto additionalBillDto = new AdditionalBillDto();

                try
                {
                    additionalBillDto.AdditionalBillsCount = _vinnytsiaCardContext.UserAdditionalBills.Where(x => x.UserId == user.Id).Count();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't count users additional bills: {ex.Message}");
                }

                try
                {
                    additionalBillDto.AdditionalBillList = (from userAdditionalBills in _vinnytsiaCardContext.UserAdditionalBills
                                                            from additionalBills in _vinnytsiaCardContext.AdditionalBill
                                                            from enterprisesServices in _vinnytsiaCardContext.EnterprisesServices
                                                            from enterprises in _vinnytsiaCardContext.Enterprises
                                                            from services in _vinnytsiaCardContext.Services
                                                            where userAdditionalBills.UserId == user.Id
                                                                && additionalBills.Id == userAdditionalBills.AdditionaBillId
                                                                && enterprisesServices.Id == additionalBills.EnterpriseServiceId
                                                                && enterprises.Id == enterprisesServices.EnterpriseId
                                                                && services.Id == enterprisesServices.ServiceId
                                                            select new AdditionalBillItem()
                                                            {
                                                                Id = userAdditionalBills.Id,                                                           
                                                                CustomerBillId = additionalBills.CustomerBillId,
                                                                EnterpriseId = enterprises.Id,
                                                                Enterprise = enterprises.EnterpriseName,
                                                                ServiceName = services.ServiceName
                                                            }).ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't select users additional bills: {ex.Message}");
                }

                return additionalBillDto;
            }

            return null;
        }

		/// <summary>
		/// Updates users bill.
		/// </summary>
		/// <returns>
		/// Task UserBill
		/// </returns>
		/// <param name="userBill">Bill with updated properties</param>
		/// <param name="userId">Id of user</param>
		public async Task<UserBill> UpdateBillAsync(UserBill userBill, int userId)
        {
            if (IsValidBill(userBill, userId))
            {
                UserBill dbBill;

                try
                {
                    dbBill = await _vinnytsiaCardContext.UsersBills.FirstOrDefaultAsync(x => x.Id == userBill.Id && x.BillTypeId == userBill.BillTypeId);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't find User's Gas bill: {ex.Message}");
                }

                if (dbBill == null)
                {
                    return await AddBill(userBill, userId);
                }
                else
                {
                    //if bills nibers are not the same, update bill number
                    if (dbBill.BillNumber != userBill.BillNumber)
                    {
                        dbBill.BillNumber = userBill.BillNumber;

                        int updatedItemsCount = 0;

                        try
                        {
                            updatedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Can't update bill for user #{userId}: {ex.Message}");
                        }

                        if (updatedItemsCount > 0)
                        {
                            return userBill;
                        }
                    }

                    return userBill;
                }
            }

            return null;
        }

		/// <summary>
		/// Checks if bill is valid.
		/// </summary>
		/// <returns>
		/// Returns true if a bill is valid 
		/// </returns>
		/// <param name="bill">Bill that will be checked</param>
		/// <param name="userId">Id of user</param>
		public bool IsValidBill(UserBill bill, int userId)
        {
            return bill.UserId == userId;
        }

		/// <summary>
		/// Adds new payment.
		/// </summary>
		/// <param name="userPayment">Object of new payment</param>
		public void AddNewPayment(Payment userPayment)
        {
            if (userPayment == null)
            {
                throw new ArgumentNullException("Payment userPayment is null");
            }

            try
            {
                _vinnytsiaCardContext.Payments.Add(userPayment);
                _vinnytsiaCardContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't update payment: { ex.Message }");
            }
        }

		/// <summary>
		/// Deletes additional payment.
		/// </summary>
		/// <returns>
		/// Returns true if a payment was successfuyly deleted.
		/// </returns>
		/// <param name="paymentId">Id of payment</param>
		/// <param name="login">Login of user</param>
		public async Task<bool> DeleteUserAdditionalPaymentAsync(int paymentId, string login)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find user '{login}': {ex.Message}");
            }

            if (user != null)
            {
                if (paymentId < 1)
                {
                    throw new ArgumentNullException("Payment userPayment is null");
                }

                UserAdditionalBill payment;

                try
                {
                    payment = await _vinnytsiaCardContext.UserAdditionalBills.FirstOrDefaultAsync(x => x.Id == paymentId && x.UserId == user.Id);

                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't find payment: { ex.Message }");
                }

                if (payment != null)
                {
                    int deletedItemsCount = 0;

                    try
                    {
                        _vinnytsiaCardContext.UserAdditionalBills.Remove(payment);

                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Can't delete payment: { ex.Message }");
                    }

                    try
                    {
                        deletedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Can't save deletion of additional bill for user #{user.Id}: {ex.Message}");
                    }

                    if (deletedItemsCount > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

		/// <summary>
		/// Retrieves payment by id.
		/// </summary>
		/// <returns>
		/// Returns true if a payment was successfuyly deleted.
		/// </returns>
		/// <param name="userId">Id of user</param>
		/// <param name="pageSize">Page size value</param>
		/// <param name="pageNumber">Page number value</param>
		public PaymentDto GetUserPaymentsById(int userId, int pageSize, int pageNumber)
        {
            if (userId <= 0)
            {
                throw new ArgumentNullException($"User's id is not valid.");
            }

            PaymentDto usersPaymentsList = new PaymentDto();
            usersPaymentsList.PaymentsCount = _vinnytsiaCardContext.Payments.Where(x => x.UserId == userId).Count();

            try
            {
                usersPaymentsList.PaymentsList.AddRange((from payments in _vinnytsiaCardContext.Payments
                                                         from enterprises in _vinnytsiaCardContext.Enterprises
                                                         where payments.UserId == userId && enterprises.Id == payments.RecipientId
                                                         orderby payments.EndDate descending
                                                         select new SimplePayment()
                                                         {
                                                             Id = payments.Id,
                                                             UserId = payments.UserId,
                                                             RecipientId = payments.RecipientId,
                                                             RecipientName = enterprises.EnterpriseName,
                                                             PaymentId = payments.PaymentId,
                                                             Amount = payments.Amount / 100,
                                                             Type = payments.Type,
                                                             PayType = payments.PayType,
                                                             Description = payments.Description,
                                                             SenderCardMask = payments.SenderCardMask,
                                                             Currency = payments.Currency,
                                                             EndDate = payments.EndDate,
                                                         }).Skip(pageSize * pageNumber).Take(pageSize).ToList());
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select payments: { ex.Message }");
            }

            return usersPaymentsList;
        }

		/// <summary>
		/// Performs a payment.
		/// </summary>
		/// <returns>
		/// Task Payment.
		/// </returns>
		/// <param name="pay">Data of payment</param>
		/// <param name="login">Login of user</param>
		public async Task<Payment> PayAsync(PayDto pay, string login)
        {
            User user = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            Payment payment = new Payment();
            payment.CreateDate = DateTime.Now;

            StripeService service = new StripeService(_config);
            // CreditCardOptions cardOptions = new CreditCardOptions()
            // {
            //     Number = pay.CardNumber,
            //     ExpYear = pay.ExpYear,
            //     ExpMonth = pay.ExpMonth,
            //     Cvc = pay.Cvc
            // };

            Card card = await service.CreateCardAsync(pay.CardId, pay.CustomerId);

            if (!string.IsNullOrEmpty(pay.CardId))
            {
                ChargeCreateOptions chargeOptions = new ChargeCreateOptions
                {
                    Customer = pay.CustomerId,
                    Amount = long.Parse(pay.Amount, CultureInfo.InvariantCulture),
                    Currency = "uah",
                    Source = card.Id,
                    Description = pay.Description,
                };

                Charge charge = await service.CreateChargeAsync(chargeOptions);

                if (charge != null)
                {
                    payment.UserId = user.Id;
                    payment.RecipientId = pay.EnterpriseId;
                    payment.PaymentId = charge.Id;
                    payment.Amount = charge.Amount;
                    payment.Type = "buy";
                    payment.PayType = charge.PaymentMethodDetails.Type;
                    payment.Description = charge.Description;
                    payment.SenderCardMask = charge.PaymentMethodDetails.Card.Last4;
                    payment.SenderCardBank = "";
                    payment.SenderCardType = charge.PaymentMethodDetails.Card.Brand;
                    payment.Currency = charge.Currency;
                    payment.EndDate = DateTime.Now;

                    int addedItemsCount = 0;

                    if (payment != null)
                    {
                        try
                        {
                            await _vinnytsiaCardContext.Payments.AddAsync(payment);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Can't add new payment for user #{user.Id}: {ex.Message}");
                        }

                        try
                        {
                            addedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Can't save new payment for user #{user.Id}: {ex.Message}");
                        }

                        if (addedItemsCount > 0)
                        {
                            return payment;
                        }
                    }
                }

            }

            return null;
        }
    }
}