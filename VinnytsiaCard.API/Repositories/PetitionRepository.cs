using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing petitions.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing petitions.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class PetitionRepository : IPetitionRepository
    {
        private VinnytsiaCardContext _vinnytsiaCardContext;

        public PetitionRepository(VinnytsiaCardContext vinnytsiaCardContext)
        {
            _vinnytsiaCardContext = vinnytsiaCardContext;
        }

		/// <summary>
		/// Adds new petition.
		/// </summary>
		/// <returns>
		/// Task Petition
		/// </returns>
		/// <param name="petition">Object of new petitions</param>
		/// <param name="userName">Login of user</param>
		public async Task<Petition> AddPetitionAsync(Petition petition, string userName)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == userName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Invalid user name {userName}: {ex.Message}");
            }

            if (user == null)
            {
                return null;
            }

            petition.AuthorId = user.Id;
            petition.Date = DateTime.Now;

            try
            {
                await _vinnytsiaCardContext.Petitions.AddAsync(petition);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't addAsync new petition '{petition.PetitionName}': {ex.Message}");
            }

            int addedItemsCount = 0;

            try
            {
                addedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't addAsync new petition '{petition.PetitionName}': {ex.Message}");
            }

            if (addedItemsCount == 0)
            {
                return null;
            }

            return petition;
        }

		/// <summary>
		/// Retreives petitions.
		/// </summary>
		/// <returns>
		/// Returns GetPetitionDto
		/// </returns>
		/// <param name="pageSize">Page size value</param>
		/// <param name="pageNumber">Page number value</param>
		public GetPetitionDto GetPetitions(int pageSize, int pageNumber)
        {
            GetPetitionDto petitionsList = new GetPetitionDto();
            petitionsList.PetitionsCount = _vinnytsiaCardContext.Petitions.Count();

            try
            {
                petitionsList.petitionDtoList = (from petitions in _vinnytsiaCardContext.Petitions
                                                 from users in _vinnytsiaCardContext.Users
                                                 where petitions.AuthorId == users.Id
                                                 orderby petitions.Date

                                                 select new PetitionDto()
                                                 {
                                                     Id = petitions.Id,
                                                     AuthorId = petitions.AuthorId,
                                                     AuthorFirstName = users.FirstName,
                                                     AuthorLastName = users.LastName,
                                                     PetitionName = petitions.PetitionName,
                                                     Description = petitions.Description,
                                                     ImageLink = petitions.ImageLink,
                                                     Date = petitions.Date
                                                 }).Skip(pageSize * pageNumber).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select petitions: {ex.Message}");
            }

            return petitionsList;
        }

		/// <summary>
		/// Retreives petition by id.
		/// </summary>
		/// <returns>
		/// Task PetitionDto
		/// </returns>
		/// <param name="petitionId">Id of petition</param>
		public async Task<PetitionDto> GetPetitionByIdAsync(int petitionId)
        {
            Petition petition;

            try
            {
                petition = await _vinnytsiaCardContext.Petitions.FirstOrDefaultAsync(x => x.Id == petitionId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find petition #{petitionId}: {ex.Message}");
            }

            if (petition == null)
            {
                return null;
            }

            User author;

            try
            {
                author = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Id == petition.AuthorId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find petition #{petitionId}: {ex.Message}");
            }

            PetitionDto petitionDto = new PetitionDto()
            {
                Id = petition.Id,
                AuthorId = petition.Id,
                AuthorFirstName = author.FirstName,
                AuthorLastName = author.LastName,
                PetitionName = petition.PetitionName,
                Description = petition.Description,
                ImageLink = petition.ImageLink,
                Date = petition.Date
            };

            return petitionDto;
        }

		/// <summary>
		/// Retreives votes of petition by id of petition.
		/// </summary>
		/// <returns>
		/// Task PetitionVotesDto
		/// </returns>
		/// <param name="petitionId">Id of petition</param>
		/// <param name="login">Login of user</param>
		public async Task<PetitionVotesDto> GetPetitionVotes(int petitionId, string login)
        {
            int userId;

            try
            {
                userId = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login).Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find petition #{petitionId}: {ex.Message}");
            }

            Petition petition;

            try
            {
                petition = await _vinnytsiaCardContext.Petitions.FirstOrDefaultAsync(x => x.Id == petitionId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find petition #{petitionId}: {ex.Message}");
            }

            if (petition == null)
            {
                return null;
            }

            PetitionVotesDto votes = new PetitionVotesDto();

            try
            {
                votes.VotesList = _vinnytsiaCardContext.PetitionsVotes.Where(x => x.PetitionId == petitionId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find votes of petition #{petitionId}: {ex.Message}");
            }

            if (votes.VotesList?.FirstOrDefault(x => x.AuthorId == userId) != null)
            {
                votes.IsUserVoted = true;
            }

            return votes;
        }

		/// <summary>
		/// Performs vote for petition.
		/// </summary>
		/// <returns>
		/// Task PetitionVote
		/// </returns>
		/// <param name="petitionVote">Object of vote</param>
		/// <param name="login">Login of user</param>
		public async Task<PetitionVote> PostPetitionVotesAsync(PetitionVote petitionVote, string login)
        {
            int userId = 0;

            try
            {
                userId = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login).Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Invalid user: {ex.Message}");
            }

            petitionVote.AuthorId = userId;
            petitionVote.Date = DateTime.Now;

            if (userId != 0 && !await IsVoteExistAsync(petitionVote))
            {
                try
                {
                    await _vinnytsiaCardContext.PetitionsVotes.AddAsync(petitionVote);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't addAsync PetitionsVotes: {ex.Message}");
                }

                int addedPetitions = 0;

                try
                {
                    addedPetitions = await _vinnytsiaCardContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't save PetitionsVote: {ex.Message}");
                }

                if (addedPetitions == 0)
                {
                    return null;
                }

                return petitionVote;
            }

            return null;
        }

		/// <summary>
		/// Checks if vote is exists.
		/// </summary>
		/// <returns>
		/// Returns true if vote is exists
		/// </returns>
		/// <param name="petitionVote">Object of vote</param>
		public async Task<bool> IsVoteExistAsync(PetitionVote petitionVote)
        {
            try
            {
                return await _vinnytsiaCardContext.PetitionsVotes.FirstOrDefaultAsync(x => x.AuthorId == petitionVote.AuthorId
                    && x.PetitionId == petitionVote.PetitionId) != null;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find vote: {ex.Message}");
            }
        }
    }
}