using System.Collections.Generic;
using System.Threading.Tasks;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IPaymentRepository
    {
        List<MainBillDto> GetUserMainBills();
        Task<List<UserBillsDto>> GetUserOtherBillsAsync(string login);
        Task<List<UserBillsDto>> GetUserAllBills(string login);
        Task<List<BillType>> GetAllBillsTypesAsync();
        List<EnterpriseType> GetAllEnterprisesTypes();
        List<Enterprise> GetEnterprisesById(int enterpriseTypeId);
        Task<List<Service>> GetEnterpriseServiceByIdAsync(int enterpriseId, string login);
        PaymentDto GetUserPaymentsById(int id, int pageSize, int pageNumber);
        Task<UpdateMainBillsDto> UpdateMainBillsAsync(UpdateMainBillsDto bills, string login);
        Task<UserAdditionalBill> AddUserAdditionalBillAsync(AddAdditionalBillDto addAdditionalBill, string login);
        Task<AdditionalBillDto> GetUserAdditionalBillAsync(string login);
        Task<UserBill> UpdateBillAsync(UserBill userBill, int userId);
        void AddNewPayment(Payment userPayment);
        Task<bool> DeleteUserAdditionalPaymentAsync(int paymentId, string login);
        Task<Payment> PayAsync(PayDto pay, string login);
    }
}