using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing users association of co-owners of apartment building.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing users association of co-owners of apartment building.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class AcabRepository : IAcabRepository
	{
		private IHttpClientFactory _httpClientFactory;
		private VinnytsiaCardContext _vinnytsiaCardContext;

		public AcabRepository(VinnytsiaCardContext vinnytsiaCardContext, IHttpClientFactory httpClientFactory)
		{
			_httpClientFactory = httpClientFactory;
			_vinnytsiaCardContext = vinnytsiaCardContext;
		}


		// Retrieves paginated acabs data
		/// <summary>
		/// Retrieves paginated acabs data.
		/// </summary>
		/// <returns>
		/// Task GetAllAcabResponse
		/// </returns>
		/// <param name="pageSize">A page size value </param>
		/// <param name="pageNumber">A page number value.</param>
		public async Task<GetAllAcabResponse> GetAllAcabsAsync(int pageSize, int pageNumber)
		{
			string json = "";

			StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
			string requestUri = "https://opendata.gov.ua/api/3/action/datastore_search?resource_id=90fc7b56-a639-4527-b947-6d42fbdee9e7&limit=10000";

			var client = _httpClientFactory.CreateClient();

			var moviesResponse = await client.GetAsync(requestUri);
			GetAllAcabResponse response;

			try
			{
				response = JObject.Parse(await moviesResponse.Content.ReadAsStringAsync()).ToObject<GetAllAcabResponse>();
			}
			catch (JsonReaderException ex)
			{
				throw new JsonReaderException($"JsonReader can't read json: { ex.Message }");
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't parse response.Content to GetAllAcabResponse object: { ex.Message }");
			}

			try
			{
				response.Result.Records = response.Result.Records.Skip(pageSize * pageNumber).Take(pageSize).ToList();
			}
			catch (ArgumentNullException ex)
			{
				throw new ArgumentNullException($"Invalid pageSize and pageNumber values: { ex.Message }");
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't pagianate GetAllAcabResponse response.Result.Records (Page size: {pageSize}; page number {pageNumber}): { ex.Message }");
			}

			return response;
		}

		// Retrieves searched acabs data
		/// <summary>
		/// Retrieves searched acabs data.
		/// </summary>
		/// <returns>
		/// Task GetAllAcabResponse
		/// </returns>
		/// <param name="searchAllAcabs">A GetSearchAllAcabs object</param>
		public async Task<GetAllAcabResponse> GetAllAcabsAsync(GetSearchAllAcabs searchAllAcabs)
		{
			string json = "";

			StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
			string requestUri = "https://opendata.gov.ua/api/3/action/datastore_search?resource_id=90fc7b56-a639-4527-b947-6d42fbdee9e7&limit=10000";

			var client = _httpClientFactory.CreateClient();

			var moviesResponse = await client.GetAsync(requestUri);
			GetAllAcabResponse response;

			try
			{
				response = JObject.Parse(await moviesResponse.Content.ReadAsStringAsync()).ToObject<GetAllAcabResponse>();
			}
			catch (JsonReaderException ex)
			{
				throw new JsonReaderException($"JsonReader can't read json: { ex.Message }");
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't parse response.Content to GetAllAcabResponse object: { ex.Message }");
			}

			try
			{
				response.Result.Records = response.Result.Records.Where(x => x.NameACMB.Replace("\"", string.Empty).ToLower().Contains(searchAllAcabs.Text.ToLower()))
				.Skip(searchAllAcabs.PageSize * searchAllAcabs.PageNumber).Take(searchAllAcabs.PageSize).ToList();
			}
			catch (ArgumentNullException ex)
			{
				throw new ArgumentNullException($"Invalid GetSearchAllAcabs searchAllAcabs value: { ex.Message }");
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't pagianate GetAllAcabResponse response.Result.Records (Page size: {searchAllAcabs.PageSize};" +
					$"page number {searchAllAcabs.PageNumber}): { ex.Message }");
			}

			return response;
		}

		// Retrieves street by id
		/// <summary>
		/// Retrieves street by id.
		/// </summary>
		/// <returns>
		/// Task Street
		/// </returns>
		/// <param name="id">A id of street</param>
		public async Task<Street> GetStreetByIdAsync(int id)
		{
			try
			{
				return await _vinnytsiaCardContext.Streets.FirstOrDefaultAsync(x => x.Id == id);
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't find Street by Id #{id}: {ex.Message}");
			}
		}

		// Retrieves building by id
		/// <summary>
		/// Retrieves building by id.
		/// </summary>
		/// <returns>
		/// Task Building
		/// </returns>
		/// <param name="id">A id of building</param>
		public async Task<Building> GetBuildingByIdAsync(int id)
		{
			try
			{
				return await _vinnytsiaCardContext.Buildings.FirstOrDefaultAsync(x => x.Id == id);
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't find ACAB #{id}: {ex.Message}");
			}
		}

		// Retrieves ACAB by id
		/// <summary>
		/// Retrieves ACAB by id.
		/// </summary>
		/// <returns>
		/// Task Acab
		/// </returns>
		/// <param name="id">A id of ACAB</param>
		public async Task<Acab> GetAcabByIdAsync(int id)
		{
			try
			{
				return await _vinnytsiaCardContext.Acabs.FirstOrDefaultAsync(x => x.AcabId == id);
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't find ACAB #{id}: {ex.Message}");
			}
		}

		// Adds new street and Retrieves added street
		/// <summary>
		/// Adds new stree.
		/// </summary>
		/// <returns>
		/// Task Street
		/// </returns>
		/// <param name="newStreet">A object of Street</param>
		public async Task<Street> AddStreetAsync(Street newStreet)
		{
			Street sameStreet;

			try
			{
				sameStreet = await _vinnytsiaCardContext.Streets.FirstOrDefaultAsync(x => x.StreetName.ToLower() == newStreet.StreetName.ToLower());
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't add find street by street name '{newStreet.StreetName}': {ex.Message}'");
			}

			if (sameStreet == null)
			{
				try
				{
					await _vinnytsiaCardContext.Streets.AddAsync(newStreet);
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't addAsync new street '{newStreet.StreetName}': {ex.Message}'");
				}

				try
				{
					await _vinnytsiaCardContext.SaveChangesAsync();
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't save changes: {ex.Message}'");
				}

				return await _vinnytsiaCardContext.Streets.FirstOrDefaultAsync(x => x.StreetName.ToLower() == newStreet.StreetName.ToLower());
			}
			else
			{
				return sameStreet;
			}
		}

		// Adds new build and Retrieves added build
		/// <summary>
		/// Adds new build.
		/// </summary>
		/// <returns>
		/// Task Building
		/// </returns>
		/// <param name="newBuilding">A object of Building</param>
		public async Task<Building> AddBuildingAsync(Building newBuilding)
		{
			Building sameBuilding;

			try
			{
				sameBuilding = await _vinnytsiaCardContext.Buildings.FirstOrDefaultAsync(x => x.StreetId == newBuilding.StreetId
					&& x.BuildingNumber.ToLower() == newBuilding.BuildingNumber.ToLower() && x.CountOfApartments == newBuilding.CountOfApartments);
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't add find building by streetId '{newBuilding.StreetId}' #{newBuilding.BuildingNumber}: {ex.Message}'");
			}

			if (sameBuilding == null)
			{
				try
				{
					await _vinnytsiaCardContext.Buildings.AddAsync(newBuilding);
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't addAsync new building '{newBuilding.StreetId}' #{newBuilding.BuildingNumber}: {ex.Message}'");
				}

				try
				{
					await _vinnytsiaCardContext.SaveChangesAsync();
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't save changes: {ex.Message}'");
				}

				return await _vinnytsiaCardContext.Buildings.FirstOrDefaultAsync(x => x.StreetId == newBuilding.StreetId
					&& x.BuildingNumber.ToLower() == newBuilding.BuildingNumber.ToLower() && x.CountOfApartments == newBuilding.CountOfApartments);
			}
			else
			{
				return sameBuilding;
			}
		}

		// Adds new ACAB and Retrieves added acab
		/// <summary>
		/// Adds new ACAB.
		/// </summary>
		/// <returns>
		/// Task Acab
		/// </returns>
		/// <param name="newAcab">A object of Acab</param>
		public async Task<Acab> AddAcabAsync(Acab newAcab)
		{
			Acab sameAcab = await _vinnytsiaCardContext.Acabs.FirstOrDefaultAsync(x => x.AcabId == newAcab.AcabId
				&& x.AcabName == newAcab.AcabName
				&& x.BuildingId == newAcab.BuildingId);

			if (sameAcab == null)
			{
				try
				{
					await _vinnytsiaCardContext.Acabs.AddAsync(newAcab);
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't addAsync new ACAB '{newAcab.AcabName}': {ex.Message}");
				}

				int savedItemsCount = 0;

				try
				{
					savedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't save new ACAB: {ex.Message}");
				}

				if (savedItemsCount == 0)
				{
					return null;
				}

				return sameAcab;
			}

			return sameAcab;
		}

		// Check if ACAB exist
		/// <summary>
		/// Check if ACAB exists.
		/// </summary>
		/// <returns>
		/// Returns true if ACAB exixts
		/// </returns>
		/// <param name="acabId">A id of Acab</param>
		public async Task<bool> IsAcabExistAsync(int acabId)
		{
			if (await _vinnytsiaCardContext.Acabs.FirstOrDefaultAsync(x => x.Id == acabId) != null)
			{
				return true;
			}

			return false;
		}

		// Retrieves users apartments
		/// <summary>
		/// Retrieves users apartments.
		/// </summary>
		/// <returns>
		/// Returns List of ApartmentDto
		/// </returns>
		/// <param name="login">Login of user</param>
		public async Task<List<ApartmentDto>> GetUserApartmentsAsync(string login)
		{
			User user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);

			if (user != null)
			{
				List<Apartment> apartmentsList = new List<Apartment>();

				try
				{
					apartmentsList = _vinnytsiaCardContext.Apartments.Where(x => x.OwnerId == user.Id).ToList();
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't select apartments for user 'login': {ex.Message}");
				}

				List<ApartmentDto> acabApartmentDto =
					(from streets in _vinnytsiaCardContext.Streets
					 from buildings in _vinnytsiaCardContext.Buildings
					 from acabs in _vinnytsiaCardContext.Acabs
					 from apartments in apartmentsList
					 where apartments.AcabId == acabs.AcabId && acabs.BuildingId == buildings.Id && buildings.StreetId == streets.Id
					 select new ApartmentDto()
					 {
						 Id = apartments.Id,
						 AcabId = acabs.AcabId,
						 Street = streets.StreetName,
						 Building = buildings.BuildingNumber,
						 AcabName = acabs.AcabName,
						 UserApartmentNumber = apartments.ApartmentNumber
					 }).ToList();

				return acabApartmentDto;
			}

			return null;
		}

		// Retrieves apartments list by ACAB id
		/// <summary>
		/// Retrieves apartments list by ACAB id.
		/// </summary>
		/// <returns>
		/// Returns List of ApartmentDto
		/// </returns>
		/// <param name="acabId">Id of ACAB</param>
		public List<Apartment> GetApartmentsByAcabId(int acabId)
		{
			try
			{
				return _vinnytsiaCardContext.Apartments.Where(x => x.AcabId == acabId).ToList();
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't select apartments by ACAB id '{acabId}': {ex.Message}");
			}
		}

		// Retrieves users apartments list by ACAB id and users login
		/// <summary>
		/// Retrieves users apartments list by ACAB id and users login.
		/// </summary>
		/// <returns>
		/// Returns List of Apartment
		/// </returns>
		/// <param name="acabId">Id of ACAB</param>
		/// <param name="login">Login of user</param>
		public async Task<List<Apartment>> GetUserApartmentsByAcabIdAsync(int acabId, string login)
		{
			int userId = 0;
			try
			{
				userId = _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login).Id;
			}
			catch (Exception ex)
			{
				throw new Exception($"Invalid user '{login}': {ex.Message}");
			}

			if (userId > 0)
			{
				try
				{
					return await _vinnytsiaCardContext.Apartments.Where(x => x.AcabId == acabId && x.OwnerId == userId).ToListAsync();
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't select apartments by ACAB id '{acabId}': {ex.Message}");
				}
			}

			return null;
		}

		// Retrieves unavailable apartments numbers by ACAB id
		/// <summary>
		/// Retrieves unavailable apartments numbers by ACAB id.
		/// </summary>
		/// <returns>
		/// Returns List of integers
		/// </returns>
		/// <param name="acabId">Id of ACAB</param>
		public List<int> GetUnavailableApartmentsNumbers(int acabId)
		{
			try
			{
				return _vinnytsiaCardContext.Apartments.Where(x => x.AcabId == acabId).Select(x => x.ApartmentNumber).ToList();
			}
			catch (Exception ex)
			{
				throw new Exception($"Can't select all unavailable apartmens numbers: {ex.Message}");
			}
		}

		// Connect apartment to user
		/// <summary>
		/// Connect apartment to user.
		/// </summary>
		/// <returns>
		/// Task Apartment
		/// </returns>
		/// <param name="newApartment">Object of new Apartment</param>
		/// /// <param name="login">Login of user</param>
		public async Task<Apartment> AddApartmentAsync(Apartment newApartment, string login)
		{
			User user = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login);

			if (user != null)
			{
				newApartment.OwnerId = user.Id;
			}
			else
			{
				return null;
			}


			Acab acab = await _vinnytsiaCardContext.Acabs.FirstOrDefaultAsync(x => x.AcabId == newApartment.AcabId);

			if (acab == null)
			{
				return null;
			}

			Building building = await _vinnytsiaCardContext.Buildings.FirstOrDefaultAsync(x => x.Id == acab.BuildingId);

			if (building == null)
			{
				return null;
			}

			// Check if apartment number lower then count of apartments in building or apartmen exists
			Apartment sameApartment = await _vinnytsiaCardContext.Apartments.FirstOrDefaultAsync(x => x.ApartmentNumber == newApartment.ApartmentNumber
				&& x.AcabId == newApartment.AcabId && x.ApartmentNumber < building.CountOfApartments);

			if (sameApartment == null)
			{
				try
				{
					await _vinnytsiaCardContext.Apartments.AddAsync(newApartment);
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't add new apartment: {ex.Message}");
				}

				int savedItemsCount = 0;

				try
				{
					savedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
				}
				catch (Exception ex)
				{
					throw new Exception($"Can't save new apartment: {ex.Message}");
				}

				if (savedItemsCount == 0)
				{
					return null;
				}

				return newApartment;
			}

			return null;
		}
    }
}