using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface IAuthRepository
    {
        Task<User> RegisterAsync(UserRegisterDto user);
        Task<User> LoginAsync(string username, string password);    
        Task<UpdatePasswordDto> UpdateUserPasswordAsync(UpdatePasswordDto updatePassword, string login);
        Task<bool> UserExistAsync(string username);
        Task<bool> EmailExistAsync(string email);  
        void Logout(string login);
		Task<RefreshToken> AddRefreshTokenAsync(RefreshToken newRefreshToken);
        Task<RefreshToken> UpdateRefreshTokenAsync(RefreshToken newRefreshToken);        
        Task<(SecurityToken, SecurityToken)> RefreshTokensAsync(string username, SecurityKey key, SecurityKey refreshKey, RefreshToken refreshToken);
		Task<bool> RefreshTokenExistAsync(string login, string refreshToken);
        Task<List<LoginsIntoSystem>> GetUsersLoginsAsync(string userName);
        Task<bool> CreateLinkForUpdatingPasswordAsync(string email);  
    }
}