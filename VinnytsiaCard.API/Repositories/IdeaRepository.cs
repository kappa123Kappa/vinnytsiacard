using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VinnytsiaCard.API.DAL;
using VinnytsiaCard.API.DTO;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing ideas of ACABS.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing ideas of ACABS.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class IdeaRepository : IIdeaRepository
    {
        private VinnytsiaCardContext _vinnytsiaCardContext;
        IAuthRepository _authRepository;
        private IAcabRepository _acabRepository;

        public IdeaRepository(VinnytsiaCardContext vinnytsiaCardContext, IAuthRepository authRepository, IAcabRepository acabRepository)
        {
            _vinnytsiaCardContext = vinnytsiaCardContext;
            _authRepository = authRepository;
            _acabRepository = acabRepository;
        }

		// Adds new idea
		/// <summary>
		/// Adds new idea.
		/// </summary>
		/// <returns>
		/// Task Idea
		/// </returns>
		/// <param name="idea">New Idea</param>
		/// <param name="userName">Login of user</param>
		public async Task<Idea> AddIdeaAsync(Idea idea, string userName)
        {
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == userName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Invalid user name {userName}: {ex.Message}");
            }

            List<Apartment> userApartments = await _acabRepository.GetUserApartmentsByAcabIdAsync(idea.AcabId, userName);

            if (user == null || userApartments == null)
            {
                return null;
            }

            idea.AuthorId = user.Id;
            idea.Date = DateTime.Now;

            try
            {
                await _vinnytsiaCardContext.Ideas.AddAsync(idea);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't addAsync new idea '{idea.IdeaName}': {ex.Message}");
            }

            int addedItemsCount = 0;

            try
            {
                addedItemsCount = await _vinnytsiaCardContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't addAsync new idea '{idea.IdeaName}': {ex.Message}");
            }

            if (addedItemsCount == 0)
            {
                return null;
            }

            return idea;
        }

		// Retrieve idea by id
		/// <summary>
		/// Retrieve idea by id.
		/// </summary>
		/// <returns>
		/// Task IdeaDto
		/// </returns>
		/// <param name="ideaId">Id of idea</param>
		public async Task<IdeaDto> GetIdeaByIdAsync(int ideaId)
        {
            Idea idea;

            try
            {
                idea = await _vinnytsiaCardContext.Ideas.FirstOrDefaultAsync(x => x.Id == ideaId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find idea #{ideaId}: {ex.Message}");
            }

            if (idea == null)
            {
                return null;
            }

            User author;

            try
            {
                author = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Id == idea.AuthorId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find idea #{ideaId}: {ex.Message}");
            }

            IdeaDto ideaDto = new IdeaDto()
            {
                Id = idea.Id,
                AuthorId = idea.Id,
                AuthorFirstName = author.FirstName,
                AuthorLastName = author.LastName,
                IdeaName = idea.IdeaName,
                Description = idea.Description,
                ImageLink = idea.ImageLink,
                Date = idea.Date
            };

            return ideaDto;
        }

		// Retrieve ideas by users conected ACABs
		/// <summary>
		/// Retrieve ideas by users conected ACABs.
		/// </summary>
		/// <returns>
		/// Task GetIdeaDto
		/// </returns>
		/// <param name="pageSize">Page size value</param>
		/// <param name="pageNumber">Page number value</param>
		/// <param name="login">Login of user</param>
		public async Task<GetIdeaDto> GetAcabsIdeasAsync(int pageSize, int pageNumber, string login)
        {
            GetIdeaDto ideasList = new GetIdeaDto();
            User user;

            try
            {
                user = await _vinnytsiaCardContext.Users.FirstOrDefaultAsync(x => x.Login == login);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find user: {ex.Message}");
            }

            List<Apartment> userApartmentsList;

            try
            {
                userApartmentsList = _vinnytsiaCardContext.Apartments.Where(x => x.OwnerId == user.Id).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find users apartments: {ex.Message}");
            }

            ideasList.IdeasCount = _vinnytsiaCardContext.Ideas.Where(x => userApartmentsList.Exists(y => y.AcabId == x.AcabId)).Count();

            try
            {
                ideasList.IdeaDtoList = (from ideas in _vinnytsiaCardContext.Ideas
                                         from acabs in _vinnytsiaCardContext.Acabs
                                         from apartments in _vinnytsiaCardContext.Apartments
                                         from users in _vinnytsiaCardContext.Users
                                        where apartments.OwnerId == user.Id 
                                               && ideas.AcabId == apartments.AcabId 
                                               && acabs.AcabId == ideas.AcabId
                                               && ideas.AuthorId == users.Id
                                         orderby ideas.Date

                                         select new IdeaDto()
                                         {
                                             Id = ideas.Id,
                                             AcabId = apartments.AcabId,
                                             AcabName = acabs.AcabName,
                                             AuthorId = ideas.AuthorId,
                                             AuthorFirstName = users.FirstName,
                                             AuthorLastName = users.LastName,
                                             IdeaName = ideas.IdeaName,
                                             Description = ideas.Description,
                                             ImageLink = ideas.ImageLink,
                                             Date = ideas.Date
                                         }).Skip(pageSize * pageNumber).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't select ideas: {ex.Message}");
            }

            return ideasList;
        }

		// Retrieve ideas votes by id of idea
		/// <summary>
		/// Retrieve ideas votes by id of idea.
		/// </summary>
		/// <returns>
		/// Task IdeaVotesDto
		/// </returns>
		/// <param name="ideaId">Id of idea</param>
		/// <param name="login">Login of user</param>
		public async Task<IdeaVotesDto> GetIdeaVotesAsync(int ideaId, string login)
        {
            int userId;

            try
            {
                userId = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login).Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find petition #{ideaId}: {ex.Message}");
            }

            Idea idea;

            try
            {
                idea = await _vinnytsiaCardContext.Ideas.FirstOrDefaultAsync(x => x.Id == ideaId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find petition #{ideaId}: {ex.Message}");
            }

            if (idea == null)
            {
                return null;
            }

            IdeaVotesDto votes = new IdeaVotesDto();

            try
            {
                votes.VotesList = _vinnytsiaCardContext.IdeasVotes.Where(x => x.IdeaId == ideaId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find votes of petition #{ideaId}: {ex.Message}");
            }

            if (votes.VotesList?.FirstOrDefault(x => x.AuthorId == userId) != null)
            {
                votes.IsUserVoted = true;
            }

            return votes;
        }

		// Posts vote
		/// <summary>
		/// Posts vote.
		/// </summary>
		/// <returns>
		/// Task IdeaVote
		/// </returns>
		/// <param name="ideaVote">Object of vote</param>
		/// <param name="login">Login of user</param>
		public async Task<IdeaVote> PostIdeaVotesAsync(IdeaVote ideaVote, string login)
        {
            int userId = 0;

            try
            {
                userId = _vinnytsiaCardContext.Users.FirstOrDefault(x => x.Login == login).Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Invalid user: {ex.Message}");
            }

            ideaVote.AuthorId = userId;
            ideaVote.Date = DateTime.Now;

            int acabId = 0;

            try
            {
                acabId = _vinnytsiaCardContext.Ideas.FirstOrDefault(x => x.Id == ideaVote.IdeaId).AcabId;
            }
            catch (Exception ex)
            {
                
                throw new Exception($"Can't find ACAB: {ex.Message}");
            }

            if (userId != 0 && acabId != 0 && !await IsIdeaVoteExistAsync(acabId, ideaVote))
            {
                try
                {
                    await _vinnytsiaCardContext.IdeasVotes.AddAsync(ideaVote);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't addAsync PetitionsVotes: {ex.Message}");
                }

                int addedPetitions = 0;

                try
                {
                    addedPetitions = await _vinnytsiaCardContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Can't save PetitionsVote: {ex.Message}");
                }

                if (addedPetitions == 0)
                {
                    return null;
                }

                return ideaVote;
            }

            return null;
        }

		// Checks if vote exists for specific idea of ACAB
		/// <summary>
		/// Checks if vote exists.
		/// </summary>
		/// <returns>
		/// Return true if vote exists for specific idea of ACAB
		/// </returns>
		/// <param name="acabId">Id of ACAB</param>
		/// <param name="ideaVote">Object of vote</param>
		public async Task<bool> IsIdeaVoteExistAsync(int acabId, IdeaVote ideaVote)
        {
            try
            {
                IdeaVote vote = await _vinnytsiaCardContext.IdeasVotes.FirstOrDefaultAsync(x => x.AuthorId == ideaVote.AuthorId
                    && x.IdeaId == ideaVote.IdeaId);
                
                return vote != null && await _vinnytsiaCardContext.Ideas.FirstOrDefaultAsync(x => x.AcabId == acabId && x.Id == vote.IdeaId) != null;
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't find idea vote: {ex.Message}");
            }
        }
    }
}