using System.Threading.Tasks;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface ITelegramRepository
    {
        Task<SendBotMessageResponse> PostMessageBotAsync(string chatId, string text);
    }
}