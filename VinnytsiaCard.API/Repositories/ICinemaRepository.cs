using System.Threading.Tasks;
using VinnytsiaCard.API.Models;

namespace VinnytsiaCard.API.Repositories
{
    public interface ICinemaRepository
    {
         Task<SmartCinemaGetMoviesResponse> GetMoviesAsync(SmartCinemaGetMoviesRequestPayload requestPayload);
    }
}