using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NewsAPI;
using NewsAPI.Constants;
using NewsAPI.Models;

namespace VinnytsiaCard.API.Repositories
{
	/*
		Repository for managing news.
		Contains all methods for performing basic functionality.
	*/
	/// <summary>
	/// Repository for managing news.
	/// Contains all methods for performing basic functionality.
	/// </summary>
	public class NewsRepository : INewsRepository
    {
		// Retrieves news
		/// <summary>
		/// Retrieves news.
		/// </summary>
		/// <returns>
		/// Returns ArticlesResult
		/// </returns>
		/// <param name="request">NewsAPI models that contains request payload </param>
		/// <param name="config">configuration object</param>
		public async Task<ArticlesResult> GetNewsAsync(EverythingRequest request, IConfiguration config)
        {
            string apiKey = config.GetSection("NewsApiKey").Value;

            if(apiKey == null)
            {
                return new ArticlesResult();
            }

            NewsApiClient newsApiClient = new NewsApiClient(apiKey);
            ArticlesResult articlesResponse = await newsApiClient.GetEverythingAsync(request);

            return articlesResponse;
        }
    }
}