using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using VinnytsiaCard.API.Models;
using VinnytsiaCard.API.Repositories;

namespace VinnytsiaCard.API.Hubs
{
    [Authorize]
    public class ForumHub : Hub
    {
        IForumRepository _repository;
        public ForumHub(){}
        
        public ForumHub(IForumRepository repository)
        {
            _repository = repository;
        }
    }
}