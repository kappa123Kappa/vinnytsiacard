import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../models/user.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { error } from '@angular/compiler/src/util';
import { UserUpdate } from '../models/userUpdate.model';
import { FormGroup, FormBuilder, FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { state } from '@angular/animations';
import { UserLoginsIntoSystem } from '../models/userLoginsIntoSystem.model';
import { UpdatePasswordDto } from '../models/updatePasswordDto.model';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HttpClient, HttpRequest, HttpEventType } from '@angular/common/http';
import { Image } from '../models/image.model';

export interface UpdateUserImageDialogData {
  service: DataService;
  notification: MatSnackBar;
  http: HttpClient
}


@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css'],
  providers: [DataService]
})
export class UserpageComponent implements OnInit {

  private user: User;
  private userUpdate: UserUpdate;
  private diasbleInput: boolean = true;
  private diasablePass: boolean = true;
  private userLogins: UserLoginsIntoSystem[];
  private showPasswordErroMessage: boolean = false;
  private passwordErroMessage: string = '';
  private userMainImage: Image;

  constructor(public dialog: MatDialog, private dataService: DataService, private activateRoute: ActivatedRoute,
    private notification: MatSnackBar, private fb: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    this.userInfo();
    this.getUsersLogins();
  }

  userInfo() {
    this.dataService.userInformation().subscribe((data: User) => {
      this.user = data;
      this.userUpdate = new UserUpdate(
        data.firstName,
        data.lastName,
        data.middleName,
        data.email,
        data.phone
      );
    });

    this.dataService.getUserMainImage().subscribe((data: Image)=> {
      this.userMainImage = data;
    }, err => {
      this.userMainImage = undefined;
    });
  }

  updateUserData(userUpdate: UserUpdate) {
    this.dataService.updateUserData(userUpdate).subscribe((data: UserUpdate) => {
      this.user = data;
      this.diasbleInput = !this.diasbleInput;
      this.notification.open("Дані оновлено!", "Повідомлення", {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ["blue", "popupmessage"]
      });
    }, err => {
      this.notification.open("Дані не оновлено" + err + "!", "Помилка", {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ["red", "popupmessage"]
      });
    });
  }

  updatePassword(oldPassword: string, newPassword: string, newPassword2: string) {
    this.passwordErroMessage = '';
    this.showPasswordErroMessage = false;

    if (oldPassword === undefined || newPassword === undefined || newPassword2 === undefined) {
      this.passwordErroMessage = this.passwordErroMessage + "Заповніть усі поля.\n";
      this.showPasswordErroMessage = true;
    }

    if (oldPassword !== undefined || newPassword !== undefined || newPassword2 !== undefined) {

      if (newPassword.length < 8 || newPassword2.length < 8) {

        this.passwordErroMessage = this.passwordErroMessage + "Новий пароль менше 8 символів.\n";
        this.showPasswordErroMessage = true;
      } else {
        let updatePasswordDto = new UpdatePasswordDto();
        updatePasswordDto.oldPassword = oldPassword;
        updatePasswordDto.newPassword = newPassword;
        updatePasswordDto.newPassword2 = newPassword2;

        this.dataService.updatePassword(updatePasswordDto).subscribe((data: UpdatePasswordDto) => {
          if (data) {
            this.diasbleInput = !this.diasbleInput;
            this.notification.open("Пароль оновлено!", "Повідомлення", {
              duration: 5 * 1000,
              verticalPosition: 'top',
              panelClass: ["blue", "popupmessage"]
            });

            this.cancelPasswordUpdate();
          }
        }, err => {
          this.passwordErroMessage = this.passwordErroMessage + "Ви ввели не вірний старий пароль!\n";
          this.showPasswordErroMessage = true;
        });
      }
    }

    if (newPassword !== newPassword2) {

      this.passwordErroMessage = this.passwordErroMessage + "Нові паролі не однакові. ";
      this.showPasswordErroMessage = true;
    }

  }

  enablePasswordUpdate() {
    this.diasablePass = false;
  }

  cancelPasswordUpdate() {
    this.diasablePass = true;
  }

  enableInputToggle() {
    this.diasbleInput = false;
  }

  cancelUpdate() {
    this.diasbleInput = true;
    this.userInfo();
  }

  getUsersLogins() {
    this.dataService.userLogins().subscribe(data => {
      this.userLogins = data;
    });
  }

  openUploadUserImageDialog() {
    const dialogRef = this.dialog.open(UploadImageFormDialog, {
      width: '426px',
      data: {
        service: this.dataService, notification: this.notification, http: this.http
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     this.dataService.getUserMainImage().subscribe((data: Image)=> {
       this.userMainImage = data;
      }, err => {
        this.userMainImage = undefined;
        console.log(this.userMainImage);
      })
    });
  };
}



@Component({
  selector: 'upload-user-image-dialog',
  templateUrl: 'upload-user-image-dialog.html',
})

export class UploadImageFormDialog implements OnInit {

  public progress: number;
  public message: string;
  public userImage: Image;
  @ViewChild('file', {static: false}) fileInput:any;

  constructor(public dialogRef: MatDialogRef<UploadImageFormDialog>, @Inject(MAT_DIALOG_DATA) public data: UpdateUserImageDialogData) {
  }

  ngOnInit(): void {

  }

  upload() {
    let nativeElement: HTMLInputElement = this.fileInput.nativeElement;

    this.data.service.updeteUserImage(nativeElement.files).subscribe((data: Image) => {
      this.userImage = data;

      this.data.notification.open('Ви оновили фото проіфлю.', 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });
      this.dialogRef.close();
    }, err => {
      console.log(err);
      this.data.notification.open('Не вдалося оновити фото профілю!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
