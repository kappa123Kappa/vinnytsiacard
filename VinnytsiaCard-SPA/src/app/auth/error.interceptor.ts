import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { catchError, retry } from '../../..//node_modules/rxjs/operators';
import { Response } from '../data.service';
import { Configuration } from '../app.constants';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  configuration: Configuration = new Configuration();
  private url = this.configuration.Server; //'http://localhost:5000/api';
  dataService: DataService = new DataService(this.http, this.router);
  constructor(private http: HttpClient, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //https://vinnytsiacardapi.azurewebsites.net/api http://localhost:5000/api
    if (request.url.startsWith(this.url)) {
      console.log('Error interceptor');
      return next.handle(request).pipe(
        catchError(err => {
          if (err.status === 401) {
            // auto logout if 401 response returned from api
            this.dataService.refreshTokens().subscribe(
              (data: Response) => {

                localStorage.removeItem('access_token');
                localStorage.removeItem('refresh_token');
                localStorage.removeItem('login');
                localStorage.setItem('access_token', data.token);
                localStorage.setItem('refresh_token', data.refreshToken);
                localStorage.setItem('login', data.username);
                console.log('Error interceptor: ' + data);

                if (data !== null) {
                  this.router.navigate(['/home']);
                }

                return next.handle(request);
              }, () => {
                this.dataService.logout();
                this.router.navigate(['/landing/login']);
                return next.handle(request);
              });
          }

          console.log(err);
          const error = err.message || err.statusText;
          return Observable.throw(error);
      }));
    }

    return next.handle(request);
  }
}
