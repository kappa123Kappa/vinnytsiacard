import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Configuration } from '../app.constants';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  configuration: Configuration = new Configuration();
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = localStorage.getItem('login');
    //https://vinnytsiacardapi.azurewebsites.net/api
    //http://localhost:5000/api
    if (request.url.startsWith(this.configuration.ServerWithApiUrl) && currentUser !== '') {
      const accessToken = 'Bearer ' + localStorage.getItem('access_token');
      request = request.clone({
        setHeaders: {
          Authorization: accessToken
        }
      });
    }
    return next.handle(request);
  }
}

