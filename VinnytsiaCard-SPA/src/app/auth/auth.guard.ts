import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AuthGuard implements CanActivate {
  authService: AuthService = new AuthService(this.http, this.router);
  constructor(private http: HttpClient, private router: Router) { }

  async canActivate(): Promise<boolean> {
    return await this.authService.isAuthenticated();
  }
}
