import { Injectable, Component } from '@angular/core';
import { HttpRequest, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Configuration } from '../app.constants';

export class Response {
  login: string;
  token: string;
}

@Component({
  providers: [Response]
})

@Injectable()
export class AuthService {
  configuration: Configuration = new Configuration();
  private url = this.configuration.ServerWithApiUrl;//'https://vinnytsiacardapi.azurewebsites.net/api' http://localhost:5000/api;
  private response: Response = new Response();
  isAuthenticatedVal = false;

  constructor(private http: HttpClient, private router: Router) {

  }

  public getToken(): string {
    return localStorage.getItem('access_token');
  }

  public getLogin(): string {
    return localStorage.getItem('login');
  }

  public async isAuthenticated(): Promise<boolean> {
    this.response.token = localStorage.getItem('access_token');
    this.response.login = localStorage.getItem('login');
    let isAuth;
    await this.http.post(this.url + '/home/ping', {}).toPromise().then((data: boolean) => {
      return isAuth = data;
    });
    return isAuth;
  }
}


