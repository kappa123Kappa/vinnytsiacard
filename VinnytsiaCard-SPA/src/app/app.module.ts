import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormGroup, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { ErrorInterceptor } from './auth/error.interceptor';

import { routing, appRoutingProviders } from './app.routing';
import { LandingComponent } from './landing/landing.component';
import { UserpageComponent, UploadImageFormDialog } from './userpage/userpage.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EntertainmentComponent } from './entertainment/entertainment.component';
import { NewsComponent } from './news/news.component';
import { ForumComponent, CreateForumTopicDialog } from './forum/forum.component';
import { TopicDiscussionComponent } from './topicDiscussion/topicDiscussion.component';
import { MyosbbComponent, CreateAddAcabFormDialog, CreateAddPaymentFormDialog, PayFormDialog, DeletePayFormDialog } from './myosbb/myosbb.component';
import { VotingComponent, CreatePetitionDialog, CreateIdeaDialog } from './voting/voting.component';
import { PetitionComponent } from './petition/petition.component';
import { IdeaComponent } from './idea/idea.component';
import { MatDialogModule } from '@angular/material';
import { StatisticComponent } from './statistic/statistic.component';
import { AboutComponent } from './about/about.component';
import { ForgotPasswordComponent } from './forgotPassword/forgotPassword.component';
import { DisqusModule } from "ngx-disqus";


@NgModule({
   declarations: [
      AppComponent, IndexComponent, LandingComponent, ForumComponent, TopicDiscussionComponent, CreateForumTopicDialog,
      CreateAddAcabFormDialog, LoginComponent, RegistrationComponent, AboutComponent, HomeComponent, DashboardComponent, UserpageComponent,
      NewsComponent, EntertainmentComponent, MyosbbComponent, VotingComponent, CreatePetitionDialog, PetitionComponent, CreateIdeaDialog,
      IdeaComponent, StatisticComponent, CreateAddPaymentFormDialog, PayFormDialog, ForgotPasswordComponent, DeletePayFormDialog, UploadImageFormDialog
   ],
   imports: [
      HttpClientModule, BrowserModule, FormsModule, routing, RouterModule.forRoot(appRoutingProviders),
      BrowserAnimationsModule, MaterialModule, ReactiveFormsModule, ChartsModule, MatDialogModule, DisqusModule.forRoot('vinnytsiacard'),
   ],
   entryComponents: [CreateForumTopicDialog, CreateAddAcabFormDialog, CreatePetitionDialog, CreateIdeaDialog,
      CreateAddPaymentFormDialog, PayFormDialog, DeletePayFormDialog, UploadImageFormDialog],
   providers: [AuthGuard,
      {
         provide: HTTP_INTERCEPTORS,
         useClass: AuthInterceptor,
         multi: true
      },
      {
         provide: HTTP_INTERCEPTORS,
         useClass: ErrorInterceptor,
         multi: true,
      }
   ],
   bootstrap: [AppComponent]
})
export class AppModule { }
