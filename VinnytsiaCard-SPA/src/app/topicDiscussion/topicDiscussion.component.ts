import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { Forum } from '../models/forum.model';
import { ForumMessageDto } from '../models/forumMessageDto.model';
import { ForumMessage } from '../models/forumMessage.model';
import * as signalR from '@aspnet/signalr';
import { PageEvent, MatPaginator } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ForumMessageWithAuthorName } from '../models/forumMessageWithAuthorName.model';

@Component({
  selector: 'app-topicDiscussion',
  templateUrl: './topicDiscussion.component.html',
  styleUrls: ['./topicDiscussion.component.css']
})

export class TopicDiscussionComponent implements OnInit {

  private hubConnection: signalR.HubConnection;
  pageEvent: PageEvent;
  topicTitle = '';
  topicId = 0;
  quoteId = 0;
  forum: Forum = new Forum();
  forumMessages: ForumMessageDto;
  pageSizeOptions: number[] = [10, 20, 30];
  pageSize = 10;
  pageNumber = 0;
  forumLength = 0;
  newMessage: ForumMessageWithAuthorName = new ForumMessageWithAuthorName();
  comment = '';
  message = '';
  messageId = 0;
  author = '';

  @ViewChild('paginatorTop', {static: false}) paginator: MatPaginator;
  @ViewChild('paginatorBottom', {static: false}) paginator2: MatPaginator;

  @ViewChild('sendMessageForm', {
    static: false
  }) sendMessageForm: NgForm;

  constructor(private route: ActivatedRoute, private dataService: DataService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.topicId = params.id;
      this.newMessage.ForumId = this.topicId;
      console.log(this.topicId);
    });

    this.getForumDescription();
    this.getMessages(this.pageSize, this.forumLength);

    // const connection = new signalR.HubConnectionBuilder()
    //   .withUrl('http://localhost:5000/forumHub')
    //   .build();

    // connection.start().then(function () {
    //   console.log('Connected!');
    // }).catch(function (err) {
    //   return console.error(err.toString());
    // });

    // connection.on("BroadcastMessage", (type: string, payload: string) => {
    //   this.getMessages();
    // });
  }

  getForumDescription() {
    this.dataService.getForumData(this.topicId).subscribe((data: Forum) => {
      this.forum = data;
      console.log(this.forum);
    });
  }

  getMessages(pageSize: number, pageNumber: number) {
    this.dataService.getForumMessages(this.topicId, pageSize, pageNumber).subscribe((data: ForumMessageDto) => {
      this.forumMessages = data;

      this.forumLength = this.forumMessages.pagination.itemsCount - 1;

      console.log(this.forumMessages);
    });
  }

  sendMessage(messageFromUI: string) {
    this.newMessage.Message = messageFromUI;
    this.newMessage.QuoteId = this.quoteId;
    this.newMessage.MediaId = 0;
    this.dataService.sendForumMessage(this.newMessage).subscribe((data: ForumMessage) => {
      console.log(data);

      if (this.paginator.hasNextPage()) {
        this.paginator.lastPage();
        this.paginator2.lastPage();
      } else {
        this.getMessages(this.pageSize, this.pageNumber);
      }
      this.message = '';
      this.comment = '';
      this.forumLength = this.pageSize;
    }, err => {
      console.log(err);
    });
  }

  paginateForumMessagesTop(event: PageEvent) {
    console.log(event.pageIndex);
    this.pageNumber = this.paginator.pageIndex;
    this.pageSize = event.pageSize;
    this.paginator2.pageIndex = this.paginator.pageIndex;
    this.getMessages(event.pageSize, event.pageIndex);
  }

  paginateForumMessagesBottom(event: PageEvent) {
    console.log(event.pageIndex);
    this.pageNumber = this.paginator.pageIndex;
    this.pageSize = event.pageSize;
    this.paginator.pageIndex = this.paginator2.pageIndex;
    this.getMessages(event.pageSize, event.pageIndex);
  }

  selectComment(messageId: number, message: string, authorLogin: string) {
    this.quoteId = messageId;
    this.comment = message;
    this.author =  authorLogin;
  }

  deleteComment() {
    this.quoteId = 0;
    this.comment = "";
    this.author =  "";
  }
}
