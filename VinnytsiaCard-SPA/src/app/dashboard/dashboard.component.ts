import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Languages, SortBys, EverythingRequest } from '../models/everythingRequest.model';
import { DataService } from '../data.service';
import { ArticlesResult } from '../models/articlesResult.model';
import { SmartCinemaGetMoviesResponse } from '../models/smartCinemaGetMoviesResponse.model';
import { SmartCinemaRequestPayload } from '../models/smartCinemaRequestPayload.model';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DataService]
})
export class DashboardComponent implements OnInit {
  innerWidth: number = 0;
  pageNumber: number = 1;
  pageSize: number = 10;
  moviesLength: number = 0;
  moviesPageSize: number = 0;
  articlesResult: ArticlesResult;
  smartCinemaRequestPayload: SmartCinemaRequestPayload = new SmartCinemaRequestPayload(1);
  smartCinemaGetMoviesResponse: SmartCinemaGetMoviesResponse;
  pageEvent: PageEvent;
  pageSizeOptions: number[] = [1, 2, 3, 4, 5];

  constructor(private dataService: DataService) {
    this.innerWidth = window.innerWidth;

    if (innerWidth > 1614) {
      this.moviesPageSize = 4;
    }
    if (innerWidth < 1614 && innerWidth > 1290) {
      this.moviesPageSize = 3;
    }
    if (innerWidth < 1290) {
      this.moviesPageSize = 2;
    }

    this.getNews();
    this.getMovies(0, this.moviesPageSize);
  }

  ngOnInit() {
    
  }

  ngAfterViewInit() {

    
  }

  getNews() {
    var everythingRequest = new EverythingRequest([] as string[], [] as string[], "Ukraine",
      new Date(2018, 1, 25), new Date(2020, 1, 25), Languages.UK, SortBys.PublishedAt, this.pageNumber, this.pageSize);

    this.dataService.getNews(everythingRequest).subscribe(data => {
      this.articlesResult = data;
    }, err => {
      console.log(err);
    });
  }

  getMovies(pageNumber: number, pageSize: number) {
    this.smartCinemaRequestPayload = new SmartCinemaRequestPayload(1);
    this.dataService.getMovies(this.smartCinemaRequestPayload).subscribe((data: SmartCinemaGetMoviesResponse) => {
      this.smartCinemaGetMoviesResponse = data;
      this.smartCinemaGetMoviesResponse.rent.map(x => {
        x.seo_inline.url = "https://smartcinema.ua" + x.seo_inline.url;
        x.poster = "https://smartcinema.ua" + x.poster;
      });
      this.moviesLength = this.smartCinemaGetMoviesResponse.rent.length;
      this.smartCinemaGetMoviesResponse.rent = this.smartCinemaGetMoviesResponse.rent.slice(pageSize * pageNumber, (pageSize * pageNumber) + pageSize)
      console.log(this.smartCinemaGetMoviesResponse);
    }, err => {
      console.log(err);
    });
  }

  paginateMovies(event: PageEvent) {
    console.log(event.pageIndex);
    this.getMovies(event.pageIndex,  event.pageSize);
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
}
