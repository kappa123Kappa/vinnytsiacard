import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { GetAllAcabResponse } from '../models/getAllAcabResponse.model';
import { Apartment } from '../models/apartment.model';
import { Forum } from '../models/forum.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateForum } from '../models/createForum.model';
import { MatSnackBar, MatPaginator, PageEvent, MatSelectChange } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Acab } from '../models/acab.model';
import { AddAcabWithApartmentDto } from '../models/addAcabWithApartmentDto.model';
import { Input } from '@angular/compiler/src/core';
import { ApartmentDto } from '../models/apartmentDto.model';
import { GetSearchAllAcabs } from '../models/getSearchAllAcabs';
import { UserBillsDto } from '../models/userBillsDto.model';
import { UpdateMainBillsDto } from '../models/updateMainBillsDto.model';
import { UserBill } from '../models/userBill.model';
import { BillType } from '../models/billType.model';
import { MainBillsDto } from '../models/mainBillsDto.model';
import { PayDto } from '../models/payDto.model';
import { PaymentDto } from '../models/paymentDto.model';
import { EnterpriseType } from '../models/enterpriseType.model';
import { Enterprise } from '../models/enterprise.model';
import { Service } from '../models/service.model';
import { AddAdditionalBillDto } from '../models/addAdditionalBillDto.model';
import { AdditionalBillDto, AdditionalBillItem } from '../models/additionalBillDto.model';


export interface DialogData {
  service: DataService;
  notification: MatSnackBar;
}

export interface PaymentDialogData {
  service: DataService;
  notification: MatSnackBar;
  mainBillDto: MainBillsDto;
  apartmentDto: ApartmentDto;
  additionalBillItem: AdditionalBillItem;
}

export class ApartmenstDropdown {
  value: number;
  viewValue: number;
}

export class EnterprisesDropdown {
  value: number;
  viewValue: string;
}

const enum BillsTypesId {
  GasBillTypeId = 1,
  WaterBillTypeId,
  ElecBillTypeId,
  Other
}

@Component({
  selector: 'app-myosbb',
  templateUrl: './myosbb.component.html',
  styleUrls: ['./myosbb.component.css']
})
export class MyosbbComponent implements OnInit {

  paymentsPageNumber = 0;
  paymentsPageSize = 0;
  searchAcabString = '';
  paymentsLength = 0;
  pagePaymentsSizeOptions: number[] = [5, 10, 15];

  readonly mainBillsTypes: string[] = ['Газ', 'Електроенергія', 'Вода'];
  noApartments = false;
  diasbleInput = true;
  userApartments: ApartmentDto[] = [];
  billTypesArr: BillType[] = [];
  usersMainBills: MainBillsDto[];
  additionalBillDto: AdditionalBillDto = new AdditionalBillDto();

  userPayments: PaymentDto = new PaymentDto();
  mainBills: UpdateMainBillsDto = new UpdateMainBillsDto();

  constructor(public dialog: MatDialog, private dataService: DataService, private notification: MatSnackBar) { }

  ngOnInit() {

    this.dataService.getUserApartments().subscribe((data: ApartmentDto[]) => {
      this.userApartments = [];
      if (data.length === 0) {
        this.noApartments = true;
      } else {
        this.noApartments = false;
        this.userApartments = data;
      }
    });

    this.dataService.getBillsTypes().subscribe((data: BillType[]) => {
      this.usersMainBills = data;
    });

    this.dataService.getUserMainBills().subscribe((data: MainBillsDto[]) => {
      this.usersMainBills = data;
      this.getMainBills(data);
      console.log(this.usersMainBills);
    });

    // this.dataService.getUserOtherBills().subscribe((data: UserBillsDto[]) => {
    //   this.usersOtherBills = data;
    //   console.log(this.usersOtherBills);
    // })

    this.dataService.getUsersPayments(this.pagePaymentsSizeOptions[0], 0).subscribe((data: PaymentDto) => {
      console.log(data);
      this.userPayments = data;
      this.paymentsLength = data.paymentsCount;
    });

    this.dataService.getUserAdditionalBill().subscribe((data: AdditionalBillDto) => {
      if (data) {
        this.additionalBillDto = data;

      } else {
        this.additionalBillDto.additionalBillList = [];
      }
    }, err => {
      this.additionalBillDto.additionalBillList = [];
    });
  }

  paginatePayments(event: PageEvent) {
    console.log(event.pageIndex);

    this.dataService.getUsersPayments(event.pageSize, event.pageIndex).subscribe((data: PaymentDto) => {
      console.log(data);
      this.userPayments = data;
      this.paymentsLength = data.paymentsCount;
    });
  }

  getMainBills(userBills: UserBillsDto[]) {
    for (var i: number = 0; i < userBills.length; i++) {
      if (userBills[i].billTypeId === BillsTypesId.GasBillTypeId) {
        this.mainBills.GasBill.id = userBills[i].id;
        this.mainBills.GasBill.billTypeId = userBills[i].billTypeId;
        this.mainBills.GasBill.userId = userBills[i].userId;
        this.mainBills.GasBill.billNumber = userBills[i].billNumber;
      }
      if (userBills[i].billTypeId === BillsTypesId.WaterBillTypeId) {
        this.mainBills.WaterBill.id = userBills[i].id;
        this.mainBills.WaterBill.billTypeId = userBills[i].billTypeId;
        this.mainBills.WaterBill.userId = userBills[i].userId;
        this.mainBills.WaterBill.billNumber = userBills[i].billNumber;
      }
      if (userBills[i].billTypeId === BillsTypesId.ElecBillTypeId) {
        this.mainBills.ElectricityBill.id = userBills[i].id;
        this.mainBills.ElectricityBill.billTypeId = userBills[i].billTypeId;
        this.mainBills.ElectricityBill.userId = userBills[i].userId;
        this.mainBills.ElectricityBill.billNumber = userBills[i].billNumber;
      }
    }
  }

  openAddPaymentForm(): void {
    const dialogRef = this.dialog.open(CreateAddPaymentFormDialog, {
      width: '1026px',
      data: { service: this.dataService, notification: this.notification }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.userApartments = [];
      this.dataService.getUserApartments().subscribe((data: ApartmentDto[]) => {
        this.userApartments = [];

        if (data.length === 0) {
          this.noApartments = true;
        } else {
          this.noApartments = false;
          this.userApartments = data;
        }
      });
    });
  }

  openAddAcabForm(): void {
    const dialogRef = this.dialog.open(CreateAddAcabFormDialog, {
      width: '1026px', height: '87%',
      data: { service: this.dataService, notification: this.notification }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.userApartments = [];
      this.dataService.getUserApartments().subscribe((data: ApartmentDto[]) => {
        if (data.length === 0) {
          this.noApartments = true;
        } else {
          this.noApartments = false;
          this.userApartments = data;
        }
      });
    });
  }

  openPayForm(mainBillDto: MainBillsDto, apartmentDto: ApartmentDto): void {
    const dialogRef = this.dialog.open(PayFormDialog, {
      width: '1026px', height: '90%',
      data: {
        service: this.dataService, notification: this.notification, mainBillDto: mainBillDto,
        apartmentDto: apartmentDto
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dataService.getUsersPayments(this.pagePaymentsSizeOptions[0], 0).subscribe((data: PaymentDto) => {
        this.userPayments = data;
        this.paymentsLength = data.paymentsCount;
      });
    });
  }

  openPayFormAdditional(additionalBillItem: AdditionalBillItem) {
    const dialogRef = this.dialog.open(PayFormDialog, {
      width: '1026px', height: '90%',
      data: {
        service: this.dataService, notification: this.notification, additionalBillItem: additionalBillItem
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dataService.getUsersPayments(this.pagePaymentsSizeOptions[0], 0).subscribe((data: PaymentDto) => {
        this.userPayments = data;
        this.paymentsLength = data.paymentsCount;

        this.dataService.getUserAdditionalBill().subscribe((data: AdditionalBillDto) => {
          this.additionalBillDto.additionalBillList = [];

          if (data) {
            this.additionalBillDto = data;
          } else {
            this.additionalBillDto.additionalBillList = [];
          }
        }, err => {
          this.additionalBillDto.additionalBillList = [];
        });
      });
    });
  }

  openDeleteFormDialog(additionalBillItem: AdditionalBillItem) {
    const dialogRef = this.dialog.open(DeletePayFormDialog, {
      width: '426px', height: '40%',
      data: {
        service: this.dataService, notification: this.notification, additionalBillItem: additionalBillItem
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dataService.getUsersPayments(this.pagePaymentsSizeOptions[0], 0).subscribe((data: PaymentDto) => {
        this.userPayments = data;
        this.paymentsLength = data.paymentsCount;

        this.dataService.getUserAdditionalBill().subscribe((data: AdditionalBillDto) => {
          this.additionalBillDto.additionalBillList = [];

          if (data) {
            this.additionalBillDto = data;
          } else {
            this.additionalBillDto.additionalBillList = [];
          }
        }, err => {
          this.additionalBillDto.additionalBillList = [];
        });
      });
    });
  }

  enableChangePaymaents() {
    this.diasbleInput = false;
  }

  changePaymaents(gasBillNumber: number, waterBillNumber: number, electricityBillNumber: number) {
    this.mainBills.GasBill.billNumber = gasBillNumber;
    this.mainBills.WaterBill.billNumber = waterBillNumber;
    this.mainBills.ElectricityBill.billNumber = electricityBillNumber;

    this.mainBills.GasBill.billTypeId = BillsTypesId.GasBillTypeId;
    this.mainBills.WaterBill.billTypeId = BillsTypesId.WaterBillTypeId;
    this.mainBills.ElectricityBill.billTypeId = BillsTypesId.ElecBillTypeId;


    this.dataService.updateUserMainBills(this.mainBills).subscribe((data: UpdateMainBillsDto) => {
      console.log(data);
      this.cancelChange();
      this.dataService.getUserMainBills().subscribe((data: UserBillsDto[]) => {
        this.usersMainBills = data;
        this.getMainBills(data);
        console.log(this.usersMainBills);
      })
      this.notification.open('Головні платежі оновлено!', 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });
    }, err => {
      console.log(err);
      this.notification.open('Головні платежі не було оновлено!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  cancelChange() {
    this.diasbleInput = true;
  }
}

@Component({
  selector: 'create-add-payment-dialog',
  templateUrl: 'create-add-payment-dialog.html',
  styleUrls: ['./myosbb.component.css']
})

export class CreateAddPaymentFormDialog implements OnInit {

  enterpriseTypeId = 0;
  enterpriseId = 0;
  enterpriseServiceId = 0;
  disableEnterpriseDropdown = true;
  disableEnterpriseServiceDropdown = true;
  disableSybmitButton = true;
  enterprisesTypeArr: EnterpriseType[] = [];
  enterprisesArr: Enterprise[] = [];
  enterprisesServicesArr: Service[] = [];
  enterprisesDropdownServicesDataArr;
  enterprisesTypesDropdownDataArr: EnterprisesDropdown[] = [];
  enterprisesDropdownDataArr: EnterprisesDropdown[] = [];
  enterpriseServiceDropdownArr: EnterprisesDropdown[] = [];


  constructor(public dialogRef: MatDialogRef<CreateAddPaymentFormDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder) {
    this.data.service.getAllAcabs(5, 0).subscribe((acabData: GetAllAcabResponse) => {

    });
  }

  ngOnInit(): void {
    this.data.service.getEnterprisesTypes().subscribe((data: EnterpriseType[]) => {
      this.enterprisesTypeArr = data;
      for (var i: number = 0; i < this.enterprisesTypeArr.length; i++) {
        let temp = new EnterprisesDropdown();
        temp.value = this.enterprisesTypeArr[i].id;
        temp.viewValue = this.enterprisesTypeArr[i].enterpriseTypeName;
        this.enterprisesTypesDropdownDataArr.push(temp)
      }
    });
  }

  enterpriseTypeDropdownChange() {
    this.disableEnterpriseDropdown = this.disableEnterpriseServiceDropdown = this.disableSybmitButton = true;
    this.enterprisesArr = [];
    this.enterprisesDropdownDataArr = [];
    this.enterpriseServiceDropdownArr = [];
    if (this.enterpriseTypeId > 0) {
      this.disableEnterpriseDropdown = false;

      this.data.service.getEnterprisesNames(this.enterpriseTypeId).subscribe((data: Enterprise[]) => {
        this.enterprisesArr = data;
        this.enterprisesDropdownDataArr = this.enterprisesServicesArr = [];
        this.enterpriseId = 0;

        for (var i: number = 0; i < this.enterprisesArr.length; i++) {
          let temp = new EnterprisesDropdown();
          temp.value = this.enterprisesArr[i].id;
          temp.viewValue = this.enterprisesArr[i].enterpriseName;
          this.enterprisesDropdownDataArr.push(temp)
        }
      });

    } else {

      this.disableEnterpriseDropdown = this.disableEnterpriseServiceDropdown = this.disableSybmitButton = true;
      this.enterprisesDropdownDataArr = [];

    }
  }

  enterpriseDropdownChange() {
    this.enterprisesServicesArr = [];
    this.enterpriseServiceDropdownArr = [];
    this.disableEnterpriseServiceDropdown = this.disableSybmitButton = true;
    if (this.enterpriseId > 0) {

      this.disableEnterpriseServiceDropdown = false;
      this.data.service.getEnterpriseServicesNames(this.enterpriseId).subscribe((data: Service[]) => {
        this.enterprisesServicesArr = data;
        console.log(data);

        this.enterpriseServiceId = 0;
        for (var i: number = 0; i < this.enterprisesServicesArr.length; i++) {
          let temp = new EnterprisesDropdown();
          temp.value = this.enterprisesServicesArr[i].id;
          temp.viewValue = this.enterprisesServicesArr[i].serviceName;
          this.enterpriseServiceDropdownArr.push(temp)
        }
      });

    } else {
      this.enterpriseServiceDropdownArr = [];
      this.disableEnterpriseServiceDropdown = this.disableSybmitButton = true;
    }
  }

  enterpriseServiceDropdownChange() {
    if (this.enterpriseServiceId > 0) {
      this.disableSybmitButton = false;
    } else {
      this.disableSybmitButton = true;
    }
  }

  addAdditionalBill() {
    const addAdditionalBillDto: AddAdditionalBillDto = new AddAdditionalBillDto();
    addAdditionalBillDto.enterpriseId = this.enterpriseId;
    addAdditionalBillDto.serviceId = this.enterpriseServiceId;
    this.data.service.addAdditionalBill(addAdditionalBillDto).subscribe((data: AddAdditionalBillDto) => {
      if (data) {
        console.log('Added additional bill');
        this.dialogRef.close();

        this.data.notification.open('Додатковий платіж додано ', 'Повідомлення', {
          duration: 5 * 1000,
          verticalPosition: 'top',
          panelClass: ['blue', 'popupmessage']
        });
      }
    }, err => {
      console.log(err);
      this.data.notification.open('Додатковий пліж не було додано!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}




@Component({
  selector: 'pay-dialog',
  templateUrl: 'pay-dialog.html',
})

export class PayFormDialog implements OnInit {
  mainBillDto: MainBillsDto;
  apartmentDto: ApartmentDto;
  additionalBillItem: AdditionalBillItem;
  payDto: PayDto = new PayDto();

  constructor(public dialogRef: MatDialogRef<PayFormDialog>, @Inject(MAT_DIALOG_DATA) public data: PaymentDialogData,
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.mainBillDto = this.data.mainBillDto;
    this.apartmentDto = this.data.apartmentDto;
    this.additionalBillItem = this.data.additionalBillItem;
    console.log(this.mainBillDto);
    this.loadStripe();
  }

  loadStripe() {
    if (!window.document.getElementById('stripe-custom-form-script')) {
      const s = window.document.createElement('script');
      s.id = 'stripe-custom-form-script';
      s.type = 'text/javascript';
      s.src = 'https://js.stripe.com/v2/';
      s.onload = () => {
        window['Stripe'].setPublishableKey('pk_test_fh943lowdLJNFfYgivMRv85r006JLfsB8G');
      };

      window.document.body.appendChild(s);
    }
  }

  pay(cardNumber: string, expYear: number, expMonth: number, cvc: string, sumGrn: string, sumCop: string, description: string) {

    if (!window['Stripe']) {
      alert('Oops! Stripe did not initialize properly.');
      return;
    }

    (<any>window).Stripe.card.createToken({
      number: cardNumber,
      exp_month: expMonth,
      exp_year: expYear,
      cvc: cvc
    }, (status: number, response: any) => {

      if (status === 200) {
        console.log(`Success! Card token ${response}.`);
        if (this.mainBillDto) {
          this.payDto.enterpriseId = this.mainBillDto.enterpriseId;
          this.payDto.customerId = this.mainBillDto.customerBillId;
          this.payDto.recipientId = this.mainBillDto.enterpriseId.toString();
        } else if (this.additionalBillItem) {
          this.payDto.enterpriseId = this.additionalBillItem.enterpriseId;
          this.payDto.customerId = this.additionalBillItem.customerBillId;
          this.payDto.recipientId = this.additionalBillItem.enterpriseId.toString();
        }

        this.payDto.cardId = response.id;
        this.payDto.amount = sumGrn + sumCop;

        if (this.mainBillDto) {
          this.payDto.description = description + `. [${this.apartmentDto.street}, будинок ${this.apartmentDto.building}, `
            + `квартира ${this.apartmentDto.userApartmentNumber}]`;
        } else if (this.additionalBillItem) {
          this.payDto.description = description + `. [${this.additionalBillItem.serviceName}]`;
        }

        this.data.service.pay(this.payDto).subscribe((data: boolean) => {
          if (data) {
            console.log('Transaction was successful');
            this.dialogRef.close();

            if (this.mainBillDto) {
              this.data.notification.open('Ви оплатили платіж ' + this.payDto.description
                + '. Оримувач: ' + this.mainBillDto.enterprise + '.', 'Повідомлення', {
                duration: 5 * 1000,
                verticalPosition: 'top',
                panelClass: ['blue', 'popupmessage']
              });
            } else if (this.additionalBillItem) {
              this.data.notification.open('Ви оплатили платіж ' + this.payDto.description
                + '. Оримувач: ' + this.additionalBillItem.enterprise + '.', 'Повідомлення', {
                duration: 5 * 1000,
                verticalPosition: 'top',
                panelClass: ['blue', 'popupmessage']
              });
            }
          }
        }, err => {
          console.log(err);
          this.data.notification.open('Не вдалося виконати опалату!', 'Помилка', {
            duration: 5 * 1000,
            verticalPosition: 'top',
            panelClass: ['red', 'popupmessage']
          });
        })
      } else {
        console.log(response.error.message);
      }
    });


  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}


@Component({
  selector: 'delete-additional-payment-dialog',
  templateUrl: 'delete-additional-payment-dialog.html',
})

export class DeletePayFormDialog implements OnInit {
  mainBillDto: MainBillsDto;
  apartmentDto: ApartmentDto;
  additionalBillItem: AdditionalBillItem;
  payDto: PayDto = new PayDto();

  constructor(public dialogRef: MatDialogRef<PayFormDialog>, @Inject(MAT_DIALOG_DATA) public data: PaymentDialogData,
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.additionalBillItem = this.data.additionalBillItem;
    this.mainBillDto = this.data.mainBillDto;
  }

  deletePayment() {
    this.data.service.deletePayment(this.additionalBillItem.id).subscribe((data: boolean) => {
      if (data) {
        this.data.notification.open('Ви видалили платіж ' + this.additionalBillItem.enterprise + '.', 'Повідомлення', {
          duration: 5 * 1000,
          verticalPosition: 'top',
          panelClass: ['blue', 'popupmessage']
        });
        this.dialogRef.close();
      } else {
        this.data.notification.open('Не вдалося видалити платіж!', 'Помилка', {
          duration: 5 * 1000,
          verticalPosition: 'top',
          panelClass: ['red', 'popupmessage']
        });
      }
    }, err => {
      console.log(err);
      this.data.notification.open('Не вдалося видалити платіж!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}





@Component({
  selector: 'create-add-acab-forum-dialog',
  templateUrl: 'create-add-acab-forum-dialog.html',
})

export class CreateAddAcabFormDialog implements OnInit {

  @ViewChild('paginator', { static: false }) paginator: MatPaginator;
  pageNumber = 0;
  pageSize = 0;
  searchAcabString = '';
  acabLength = 0;
  pageSizeOptions: number[] = [5, 10, 15];
  acabs: GetAllAcabResponse;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  disableAcabNext = true;
  selectedAcab = new Acab();
  isAcabExists = false;
  isOptional = false;
  addAcabDto = new AddAcabWithApartmentDto();
  apartments: ApartmenstDropdown[] = [];
  apartmentUser = '';
  

  constructor(public dialogRef: MatDialogRef<CreateAddAcabFormDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder) {
    this.data.service.getAllAcabs(5, 0).subscribe((acabData: GetAllAcabResponse) => {
      this.acabs = acabData;
      this.acabLength = acabData.result.total;
      this.pageSize = this.pageSizeOptions[0];
      console.log(this.acabs);
      console.log(this.addAcabDto);
    });
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      acabName: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      acabAddress: ['', Validators.required],
      acabBulding: ['', Validators.required],
      acabApartmentsCount: ['', Validators.required],
      acabRegistrationDate: ['', Validators.required]
    });
    this.thirdFormGroup = this.formBuilder.group({
      userApartment: ['', Validators.required]
    });
  }

  saveAcabApartment() {
    this.addAcabDto.userApartmentNumber = Number.parseInt(this.apartmentUser);
    this.data.service.addNewAcabAndUserApartment(this.addAcabDto).subscribe((data: AddAcabWithApartmentDto) => {
      console.log('Apartment was successfully added.');
      console.log(data);
      this.dialogRef.close();
      this.data.notification.open('Ви підключились до ОСББ ' + data.acabName + '!', 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });
    }, err => {
      console.log(err);
      this.data.notification.open('Ви не підключились до ОСББ' + this.addAcabDto.acabName + '!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  paginateAcabs(event: PageEvent) {
    console.log(event.pageIndex);
    this.pageNumber = this.paginator.pageIndex;
    this.pageSize = event.pageSize;

    let searchAcabs = new GetSearchAllAcabs();
    searchAcabs.text = this.searchAcabString;
    searchAcabs.pageSize = this.pageSize;
    searchAcabs.pageNumber = this.pageNumber;

    this.data.service.getSearchAllAcabs(searchAcabs).subscribe((data: GetAllAcabResponse) => {
      this.acabs = data;
      this.acabLength = data.result.records.length;

      if (searchAcabs.text === '') {
        this.acabLength = data.result.total;
      }
    });
    // this.data.service.getAllAcabs(this.pageSize, this.pageNumber).subscribe((data: GetAllAcabResponse) => {
    //   this.acabs = data;
    // });
  }

  onSearchChange(searchValue: string): void {
    this.searchAcabString = searchValue;
    let searchAcabs = new GetSearchAllAcabs();
    searchAcabs.text = this.searchAcabString;
    searchAcabs.pageSize = this.pageSize;
    searchAcabs.pageNumber = 0;

    this.data.service.getSearchAllAcabs(searchAcabs).subscribe((data: GetAllAcabResponse) => {
      this.acabs = data;
      this.acabLength = data.result.records.length;

      if (searchAcabs.text === '') {
        this.acabLength = data.result.total;
      }

    });
  }

  searchAcabs(text: string) {
    let searchAcabs = new GetSearchAllAcabs();
    searchAcabs.text = text;
    searchAcabs.pageSize = this.pageSize;
    searchAcabs.pageNumber = 0;

    this.data.service.getSearchAllAcabs(searchAcabs).subscribe((data: GetAllAcabResponse) => {
      this.acabs = data;
      this.acabLength = data.result.records.length;
    });
  }

  selectAcab(acabId: number, acabName: string, addres: string, date: string) {
    this.disableAcabNext = false;
    this.selectedAcab.id = acabId;
    this.addAcabDto.street = addres;
    this.addAcabDto.registrationDate = date;
    this.selectedAcab.acabName = acabName.replace('"', '').replace('"', '');
    this.checkSelectedAcab();
  }

  checkSelectedAcab() {
    this.data.service.getAcabByIdName(this.selectedAcab.id).subscribe((data: AddAcabWithApartmentDto) => {
      if (data != null) {
        this.isAcabExists = true;
        this.addAcabDto.id = data.id;
        this.addAcabDto.street = data.street;
        this.addAcabDto.building = data.building;
        this.addAcabDto.acabName = data.acabName;
        this.addAcabDto.countOfApartments = data.countOfApartments;
        this.addAcabDto.registrationDate = data.registrationDate;

        for (var i: number = 0; i < data.avaliableApartments.length; i++) {
          let temp = new ApartmenstDropdown();
          temp.value = data.avaliableApartments[i];
          temp.viewValue = data.avaliableApartments[i];
          this.apartments.push(temp)
        }
      } else {
        this.isAcabExists = false;
        let secondCommaPosition = this.addAcabDto.street.indexOf(',', this.addAcabDto.street.indexOf(',') + 1);
        let tempAdd = this.addAcabDto.street.slice(0, secondCommaPosition);
        let buildAdd = this.addAcabDto.street.slice(secondCommaPosition + 2, this.addAcabDto.street.length);
        this.addAcabDto.street = tempAdd;
        this.addAcabDto.building = buildAdd;
        this.addAcabDto.acabName = this.selectedAcab.acabName;
        this.addAcabDto.id = this.selectedAcab.id;
        console.log(this.addAcabDto);
      }
    });
  }

  addAdditionalInformation(apartmentsCount: number) {
    this.addAcabDto.countOfApartments = apartmentsCount;
    for (var i: number = 1; i < apartmentsCount; i++) {
      let temp = new ApartmenstDropdown();
      temp.value = i;
      temp.viewValue = i;
      this.apartments.push(temp);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
