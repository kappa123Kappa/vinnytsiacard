export class EverythingRequest {
    constructor(
        public Sources: string[],
        public Domains: string[],
        public Q: string,
        public From: Date,
        public To: Date,
        public Languages: Languages,
        public SortBy: SortBys,
        public Page: number,
        public PageSize: number) { }
}

export enum Languages {
    //
    // Summary:
    //     Afrikaans (South Africa)
    AF = 0,
    AN = 1,
    AR = 2,
    AZ = 3,
    BG = 4,
    BN = 5,
    BR = 6,
    BS = 7,
    CA = 8,
    CS = 9,
    CY = 10,
    DA = 11,
    //
    // Summary:
    //     German
    DE = 12,
    EL = 13,
    //
    // Summary:
    //     English
    EN = 14,
    EO = 15,
    //
    // Summary:
    //     Spanish
    ES = 16,
    ET = 17,
    EU = 18,
    FA = 19,
    FI = 20,
    FR = 21,
    GL = 22,
    HE = 23,
    HI = 24,
    HR = 25,
    HT = 26,
    HU = 27,
    HY = 28,
    ID = 29,
    IS = 30,
    //
    // Summary:
    //     Italian
    IT = 31,
    //
    // Summary:
    //     Japanese
    JP = 32,
    JV = 33,
    KK = 34,
    KO = 35,
    LA = 36,
    LB = 37,
    LT = 38,
    LV = 39,
    MG = 40,
    MK = 41,
    ML = 42,
    MR = 43,
    MS = 44,
    //
    // Summary:
    //     Dutch
    NL = 45,
    NN = 46,
    NO = 47,
    OC = 48,
    PL = 49,
    //
    // Summary:
    //     Portuguese
    PT = 50,
    RO = 51,
    RU = 52,
    SH = 53,
    SK = 54,
    SL = 55,
    SQ = 56,
    SR = 57,
    SV = 58,
    SW = 59,
    TA = 60,
    TE = 61,
    TH = 62,
    TL = 63,
    TR = 64,
    UK = 65,
    UR = 66,
    VI = 67,
    VO = 68,
    //
    // Summary:
    //     Chinese
    ZH = 69
}

export enum SortBys {
    //
    // Summary:
    //     Sort by publisher popularity
    Popularity = 0,
    //
    // Summary:
    //     Sort by article publish date (newest first)
    PublishedAt = 1,
    //
    // Summary:
    //     Sort by relevancy to the Q param
    Relevancy = 2
}