export class Acab {
    public id?: number;
    public streetId?: number;
    public buildingId?: number;
    public acabName?: string;
    public RegistrationDate?: Date;
    public countOfApartments?: number;
}