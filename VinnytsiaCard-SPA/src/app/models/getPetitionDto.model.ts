export class GetPetitionDto {
    public petitionDtoList?: PetitionDto[];
    public petitionsCount = 0;
}

export class PetitionDto {
    public id = 0;
    public authorId = '';
    public authorFirstName = '';
    public authorLastName = '';
    public petitionName = '';
    public description = '';
    public imageLink = '';
    public date?: Date;
}
