export class FoodStatisticDto {
    public id?: number = 0;
    public foodTypeId?: number = 0;
    public foodTypeName?: string = '';
    public foodValue?: number = 0;
    public year?: number = 0;
}
