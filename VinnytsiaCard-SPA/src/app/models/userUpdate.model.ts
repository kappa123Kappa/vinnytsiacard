export class UserUpdate {
    constructor(
      public firstName?: string,
      public lastName?: string,
      public middleName?: string,
      public email?: string,
      public phone?: string) { }
  }