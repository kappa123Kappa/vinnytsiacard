import { IdeaVote } from './ideaVote.model';

export class IdeaVotesDto {
    public votesList?: IdeaVote[] = [];
    public isUserVoted = false;
}
