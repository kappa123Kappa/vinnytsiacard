export class UserBillsDto {
    public id?: number = 0;
    public billTypeId?: number = 0;
    public billTypeName?: string = "";
    public userId?: number = 0; 
    public billNumber?: number = 0;
}
