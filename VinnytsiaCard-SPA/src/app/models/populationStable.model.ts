export class PopulationStable {
    public id?: number = 0;
    public cityId?: number = 0;
    public count?: number = 0;
    public date?: Date;
}
