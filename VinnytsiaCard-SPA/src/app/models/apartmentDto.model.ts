export class ApartmentDto {
    public id?: number;
    public acabId?: number;
    public street?: string;
    public building?: string;
    public acabName?: string;
    public userApartmentNumber?: number = 0;
}
