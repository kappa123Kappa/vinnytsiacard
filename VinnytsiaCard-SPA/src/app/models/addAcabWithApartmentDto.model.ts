export class AddAcabWithApartmentDto {
    public id?: number = 0;
    public street?: string = "";
    public building?: string = "";
    public acabName?: string = "";
    public registrationDate?: string = "";
    public countOfApartments?: number = 0;
    public userApartmentNumber?: number = 0;
    public avaliableApartments?: number[] = [];
}