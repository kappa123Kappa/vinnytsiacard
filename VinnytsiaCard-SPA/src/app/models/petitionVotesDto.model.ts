import { PetitionVote } from './petitionVote.model';


export class PetitionVotesDto {
    public votesList?: PetitionVote[] = [];
    public isUserVoted = false;
}
