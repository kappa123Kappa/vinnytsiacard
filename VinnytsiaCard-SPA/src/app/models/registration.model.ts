export class Registration {
    constructor(
      public login?: string,
      public Password?: string,
      public Password2?: string,
      public FirstName?: string,
      public LastName?: string,
      public MiddleName?: string,
      public Email?: string,
      public Phone?: string) { }
  }