export class Pagination {
    constructor(public pageSize: number,
        public pageNumber: number,
        public itemsCount: number){}
}
