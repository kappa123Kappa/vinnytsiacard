export class ForumMessage {
    constructor();
    constructor(
        public Id?: number,
        public ForumId?: number, 
        public AuthorId?: number,
        public Message?: string,
        public QuoteId?: number,
        public MediaId?: number,
        public Date?: Date) { };
}