export class GetIdeaDto {
    public ideaDtoList?: IdeaDto[];
    public ideaCount = 0;
}

export class IdeaDto {
    public id = 0;
    public acabId = 0;
    public acabName = '';
    public authorId = 0;
    public authorFirstName = '';
    public authorLastName = '';
    public petitionName = '';
    public description = '';
    public imageLink = '';
    public date?: Date;
}