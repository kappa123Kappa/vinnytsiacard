export class Image {
    public Id?: number = 0;
    public ImageTypeId?: number = 0;
    public ImageKey?: string = "";
    public ImageSrc?: string = "";
}
