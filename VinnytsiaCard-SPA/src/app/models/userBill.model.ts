export class UserBill {
    public id?: number = 0;
    public billTypeId?: number = 0;
    public userId?: number = 0;
    public billName: string = "";
    public billNumber?: number = 0;
}
