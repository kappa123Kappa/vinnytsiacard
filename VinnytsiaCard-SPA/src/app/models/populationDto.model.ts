export class PopulationDto {
    public id?: number = 0;
    public cityId?: number = 0;
    public cityName?: string = "";
    public count?: number = 0;
    public date?: Date;
}
