export class GetSearchAllAcabs {
    public text?: string;
    public pageSize?: number; 
    public pageNumber?: number;
}
