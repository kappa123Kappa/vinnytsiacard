export class Idea {
    public id = 0;
    public acabId = 0;
    public authorId = 0;
    public ideaName = '';
    public description = '';
    public imageLink = '';
}
