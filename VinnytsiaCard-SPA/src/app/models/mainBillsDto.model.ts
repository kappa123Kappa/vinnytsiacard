export class MainBillsDto {
    public id?: number = 0;
    public billType?: string = "";
    public customerBillId?: string = "";
    public enterpriseId?: number = 0;
    public enterprise?: string = "";
    public billNumber?: number = 0;
}
