import { UserBill } from './userBill.model';

export class UpdateMainBillsDto {
    public GasBill?: UserBill = new UserBill();
    public WaterBill?: UserBill = new UserBill();
    public ElectricityBill?: UserBill = new UserBill();
}
