export class Apartment {
    constructor(public id?: number,
        public acabId?: number,
        public apartmentNumber?: number,
        public ownerId?: number,
        public roomsCount?: number,
        public gasAccount?: number,
        public waterAccount?: number,
        public electricityAccount?: number) {
        }
}