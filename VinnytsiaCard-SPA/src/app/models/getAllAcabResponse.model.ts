export class GetAllAcabResponse {
    constructor(public help?: string,
        public success?: boolean,
        public result?: Result,){}
}

export interface Field {
    type: string;
    id: string;
}

export class Record {
    _id: number;
    id: number;
    nameACMB: string = "";
    legalAddress: string = "";
    Addresses: string = "";
    RegistrationDate: Date;
    ReRegistrationDate?: any;
}

export interface Links {
    start: string;
    next: string;
}

export interface Result {
    include_total: boolean;
    resource_id: string;
    fields: Field[];
    records_format: string;
    records: Record[];
    limit: number;
    _links: Links;
    total: number;
}