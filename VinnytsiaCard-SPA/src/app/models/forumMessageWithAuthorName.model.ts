export class ForumMessageWithAuthorName {
    constructor(public Id?: number,
                public ForumId?: number,
                public AuthorMessageId?: number,
                public AuthorMessageLogin?: string,
                public AuthorImageSrc?: string,
                public Message?: string,
                public AuthorQuoteId?: number,
                public AuthorQuoteLogin?: string,
                public QuoteId?: number,     
                public Quote?: string,
                public MediaId?: number,
                public DateTime?: Date) {}
}
