export class Forum {
    constructor(
        public Id?: number,
        public Title?: string, 
        public MainMessageId?: number,
        public CreatedAt?: Date,
        public AuthorId?: number) { }
}