export class IdeaVote {
    public id = 0;
    public ideaId = 0;
    public authorId = 0;
    public voteStatus = false;
    public date: Date;
}
