export class AdditionalBillDto {
    public additionalBillList?: AdditionalBillItem[] = [];
    public additionalBillsCount?: number = 0;
}

export class AdditionalBillItem {
    public id?: number = 0;
    public customerBillId?: string = "";
    public enterpriseId?: number = 0;
    public enterprise?: string = "";
    public serviceName?: string = "";
}
