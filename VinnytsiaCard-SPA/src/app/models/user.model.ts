export class User {
  constructor(
    public id?: number,
    public login?: string,
    public firstName?: string,
    public lastName?: string,
    public middleName?: string,
    public email?: string,
    public phone?: string) { }
}