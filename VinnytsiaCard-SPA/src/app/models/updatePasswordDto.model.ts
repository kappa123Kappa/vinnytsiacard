export class UpdatePasswordDto {
    oldPassword: string; 
    newPassword: string; 
    newPassword2: string;
}
