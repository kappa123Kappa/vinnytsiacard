export class PaymentDto {
    public paymentsList: SimplePayment[];
    public paymentsCount?: number = 0;

}

export class SimplePayment {
    public id?: number = 0;
    public userId?: number = 0;
    public RecipientId?: number = 0;
    public RecipientName?: string = "";
    public PaymentId?: string = "";
    public Amount?: string = "";
    public Type?: string = "";
    public Description?: string = "";
    public SenderCardMask?: string = "";
    public Currency?: string = "";
    public EndDate?: Date;
}
