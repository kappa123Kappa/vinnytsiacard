export class SalaryDto {
    public id?: number = 0;
    public activityId?: number = 0;
    public activityName?: string = '';
    public salaryValue?: number = 0;
    public month?: number = 0;
    public year?: number = 0;
}
