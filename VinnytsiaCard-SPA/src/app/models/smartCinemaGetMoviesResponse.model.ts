export class SmartCinemaGetMoviesResponse {
    soon: Soon[];
    rent: Rent[];
    branch: Branch[];
    shares: Share[];
}

export interface SeoInline {
    url: string;
}

export interface Soon {
    id: number;
    title: string;
    age: number;
    poster: string;
    have_showtime: boolean;
    format: string;
    seo_inline: SeoInline;
    film_release_date: string;
    pictures: string;
    trailer: string;
}

export interface Rent {
    id: number;
    title: string;
    age: number;
    poster: string;
    have_showtime: boolean;
    format: string;
    seo_inline: SeoInline;
    film_release_date: string;
    pictures: string;
    trailer: string;
}

export interface Branch {
    id: number;
    name: string;
    address: string;
    phone: string;
    lon: string;
    lat: string;
    city: string;
}

export interface Share {
    title: string;
    image: string;
    short_description: string;
    seo_inline: SeoInline;
    type_of_post: string;
}