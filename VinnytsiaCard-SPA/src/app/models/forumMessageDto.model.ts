import { Pagination } from './pagination.model';
import { ForumMessageWithAuthorName } from './forumMessageWithAuthorName.model';

export class ForumMessageDto {
    constructor(public forumMessageWithAuthorName?: ForumMessageWithAuthorName[],
                public pagination?: Pagination) { }
}
