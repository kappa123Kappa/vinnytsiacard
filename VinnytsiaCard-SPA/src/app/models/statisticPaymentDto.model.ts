export class StatisticPaymentDto {
    public recipientId?: number = 0;
    public amount?: number = 0;
    public monthNumber?: number = 0;
}
