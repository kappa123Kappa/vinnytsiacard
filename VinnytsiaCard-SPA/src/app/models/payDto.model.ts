export class PayDto {
    public id?: number = 0;
    public enterpriseId?: number = 0;
    public customerId?: string = "";
    public recipientId?: string = "";
    public cardId?: string = ""; 
    // public cardNumber?: string = "";
    // public expYear?: number = 0;
    // public expMonth?: number = 0;
    // public cvc?: string = "";
    public amount?: string = "";
    public description?: string = "";
}
