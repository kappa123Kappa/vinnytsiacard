export class Petition {
    public id?: number = 0;
    public authorId?: number = 0;
    public petitionName: string = "";
    public description: string = "";
    public imageLink: string = "";
}
