export class CreateForum {
    constructor(
        public Id?: number,
        public Title?: string, 
        public Message?: string,
        public CreatedAt?: Date,
        public AuthorId?: number) { }
}
