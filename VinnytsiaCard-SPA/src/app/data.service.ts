import { Injectable } from '@angular/core';
import {
  HttpClient, HttpInterceptor, HttpHandler, HttpRequest,
  HttpEvent, HttpResponse, HttpParams, HttpHeaders
} from '@angular/common/http';
import { Login } from './models/login.model';
import { Registration } from './models/registration.model';
import { RouterModule, Routes, Router } from '@angular/router';

import { Observable } from 'rxjs';
import { error } from 'protractor';
import { map } from 'rxjs/internal/operators/map';
import { User } from './models/user.model';
import { UserUpdate } from './models/userUpdate.model';
import { UserLoginsIntoSystem } from './models/userLoginsIntoSystem.model';
import { EverythingRequest, SortBys, Languages } from './models/everythingRequest.model';
import { ArticlesResult } from './models/articlesResult.model';
import { SmartCinemaGetMoviesResponse } from './models/smartCinemaGetMoviesResponse.model';
import { SmartCinemaRequestPayload } from './models/smartCinemaRequestPayload.model';
import { Forum } from './models/forum.model';
import { CreateForum } from './models/createForum.model';
import { ForumMessage } from './models/forumMessage.model';
import { ForumMessageDto } from './models/forumMessageDto.model';
import { ForumMessageWithAuthorName } from './models/forumMessageWithAuthorName.model';
import { GetAllAcabResponse } from './models/getAllAcabResponse.model';
import { Apartment } from './models/apartment.model';
import { Acab } from './models/acab.model';
import { AddAcabWithApartmentDto } from './models/addAcabWithApartmentDto.model';
import { ApartmentDto } from './models/apartmentDto.model';
import { GetSearchAllAcabs } from './models/getSearchAllAcabs';
import { UserBillsDto } from './models/userBillsDto.model';
import { UpdateMainBillsDto } from './models/updateMainBillsDto.model';
import { BillType } from './models/billType.model';
import { Petition } from './models/petition.model';
import { GetPetitionDto, PetitionDto } from './models/getPetitionDto.model';
import { PetitionVotesDto } from './models/petitionVotesDto.model';
import { PetitionVote } from './models/petitionVote.model';
import { Idea } from './models/idea.model';
import { GetIdeaDto, IdeaDto } from './models/getIdeaDto.model';
import { IdeaVotesDto } from './models/ideaVotesDto.model';
import { IdeaVote } from './models/ideaVote.model';
import { MainBillsDto } from './models/mainBillsDto.model';
import { PayDto } from './models/payDto.model';
import { PaymentDto } from './models/paymentDto.model';
import { EnterpriseType } from './models/enterpriseType.model';
import { Enterprise } from './models/enterprise.model';
import { Service } from './models/service.model';
import { AddAdditionalBillDto } from './models/addAdditionalBillDto.model';
import { AdditionalBillDto } from './models/additionalBillDto.model';
import { StatisticPaymentDto } from './models/statisticPaymentDto.model';
import { PopulationDto } from './models/populationDto.model';
import { PopulationStable } from './models/populationStable.model';
import { FoodStatisticDto } from './models/foodStatisticDto.model';
import { SalaryDto } from './models/salaryDto.model';
import { UpdatePassword } from './models/updatePassword.model';
import { Configuration } from './app.constants';
import { UpdatePasswordDto } from './models/updatePasswordDto.model';
import { Image } from './models/image.model';


@Injectable()
export class DataService {
  items: string;
  userData: User = new User();
  configuration: Configuration = new Configuration();
  private url = this.configuration.ServerWithApiUrl;//'http://localhost:5000/api';//'https://vinnytsiacardapi.azurewebsites.net/api'; http://localhost:5000/api
  respnce: Response;
  isUpdatedTokens = false;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(login: Login) {
    return this.http.post(this.url + '/auth/login', login);
  }

  register(registration: Registration) {
    return this.http.post(this.url + '/auth/register', registration)
      .subscribe(
        (data: Response) => {
          localStorage.setItem('access_token', data.token);
          localStorage.setItem('refresh_token', data.refreshToken);
          localStorage.setItem('login', data.username);
          console.log(data);
          if (data !== null) {
            this.router.navigateByUrl('/home/dashboard');
          }
        }, err => {
          console.log('Error: ' + err);
          this.router.navigateByUrl('/landing/registration');
        }
      );
  }

  refreshTokens() {
    const headers = new HttpHeaders()
      .set('refreshToken', localStorage.getItem('refresh_token')).set('username', localStorage.getItem('login'));

    return this.http.post(this.url + '/auth/refreshTokens', {}, { headers });
  }

  updatePassword(updatePasswordDto: UpdatePasswordDto) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<UpdatePasswordDto>(this.url + '/auth/updateUserPassword', updatePasswordDto);
    }
  }

  userInformation() {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.get<User>(this.url + '/profile/userdata');
    }
    return new Observable<User>();
  }

  updateUserData(userUpdate: UserUpdate) {
    return this.http.post<UserUpdate>(this.url + '/profile/updateuser', userUpdate);
  }

  updeteUserImage(file) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {

      let input = new FormData();
      input.append("file", file[0], file[0].name);
      
      return this.http.post<Image>(this.url + '/profile/UpdateUserImage', input);

      // const formData = new FormData();
      // formData.append(files[0].name, files[0]);
      // let headers = new HttpHeaders();
      // headers.append('Content-Type', 'multipart/form-data');
      // headers.append('Accept', 'application/json');

      // const uploadReq = new HttpRequest('POST', this.url + '/profile/UpdateUserImage', formData, {headers});

      // return this.http.request<Image>(uploadReq);//this.http.post<Image>(, files[0])
    }
  }

  getUserMainImage() {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.get<Image>(this.url + '/profile/getUserMainImage');
    }
    return new Observable<Image>();
  }

  userLogins() {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.get<UserLoginsIntoSystem[]>(this.url + '/auth/getuserslogins');
    }
    return new Observable<UserLoginsIntoSystem[]>();
  }

  getAllAcabs(pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.get<GetAllAcabResponse>(this.url + '/acab/getAllAcabs/' + pageSize + '/' + pageNumber);
    }
    return new Observable<GetAllAcabResponse>();
  }

  getSearchAllAcabs(searchAcabs: GetSearchAllAcabs) {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.post<GetAllAcabResponse>(this.url + '/acab/getSearchAllAcabs/', searchAcabs);
    }
    return new Observable<GetAllAcabResponse>();
  }

  getAcabByIdName(selectedAcabId: number) {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.get<Acab>(this.url + '/acab/getAcabById/' + selectedAcabId);
    }
    return new Observable<Acab>();
  }

  getUserApartments() {
    const accessToken = localStorage.getItem('access_token');
    if (accessToken) {
      return this.http.get<ApartmentDto[]>(this.url + '/acab/getUserApartments');
    }
    return new Observable<ApartmentDto[]>();
  }

  addNewAcabAndUserApartment(acabWithApartmen: AddAcabWithApartmentDto) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<AddAcabWithApartmentDto>(this.url + '/acab/addAcabAndWithApartment', acabWithApartmen);
    }

    return new Observable<AddAcabWithApartmentDto>();
  }

  addNewForumTopic(forum: CreateForum) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<CreateForum>(this.url + '/forum/createNewTopic/', forum);
    }

    return new Observable<CreateForum>();
  }

  getForumTopics(pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get(this.url + '/forum/getforumtopics/'
        + pageSize + '/' + pageNumber);
    }

    return Forum[1];
  }

  getNews(everythingRequest: EverythingRequest) {
    const accessToken = localStorage.getItem('access_token');

    const params = new HttpParams()
      .set('key', everythingRequest.Q)
      .set('page', everythingRequest.Page.toString())
      .set('pageSize', everythingRequest.PageSize.toString());

    if (accessToken) {
      return this.http.get<ArticlesResult>(this.url + '/news/getnews/'
        + everythingRequest.Q + '/'
        + everythingRequest.Page.toString() + '/'
        + everythingRequest.PageSize.toString());
    }

    return new Observable<ArticlesResult>();
  }

  getMovies(smartCinemaRequestPayload: SmartCinemaRequestPayload) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<SmartCinemaGetMoviesResponse>(this.url + '/cinema/getMovies/', smartCinemaRequestPayload);
    }

    return new Observable<SmartCinemaGetMoviesResponse>();
  }

  getForumData(forumId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<Forum>(this.url + '/forum/gettopicdata/' + forumId);
    }

    return new Observable<Forum>();
  }

  getForumMessages(forumId: number, pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<ForumMessageDto>(this.url + '/forum/getforummessages/' + forumId + '/' + pageSize + '/' + pageNumber);
    }

    return new Observable<ForumMessageDto>();
  }

  sendForumMessage(forumMessage: ForumMessageWithAuthorName) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<ForumMessage>(this.url + '/forum/sendmessage/', forumMessage);
    }

    return new Observable<ForumMessage>();
  }

  getBillsTypes() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<BillType[]>(this.url + '/payment/getBillsTypes');
    }
  }

  getUserMainBills() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<MainBillsDto[]>(this.url + '/payment/getUserMainBills');
    }
  }

  updateUserMainBills(data: UpdateMainBillsDto) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<UpdateMainBillsDto>(this.url + '/payment/updatMainBills', data);
    }
  }

  getUserOtherBills() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<UserBillsDto[]>(this.url + '/payment/getUserOtherBills');
    }
  }

  getEnterprisesTypes() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<EnterpriseType[]>(this.url + '/payment/getEnterprisesTypes');
    }
  }

  getEnterprisesNames(enterpriseTypeId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<Enterprise[]>(this.url + '/payment/getEnterprisesNames/' + enterpriseTypeId);
    }
  }

  getEnterpriseServicesNames(enterpriseId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<Service[]>(this.url + '/payment/getEnterpriseServicesNames/' + enterpriseId);
    }
  }

  addAdditionalBill(addAdditionalBillDto: AddAdditionalBillDto) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<AddAdditionalBillDto>(this.url + '/payment/addUserAdditionalBill', addAdditionalBillDto);
    }
  }

  getUserAdditionalBill() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<AdditionalBillDto>(this.url + '/payment/getUserAdditionalBills');
    }
  }

  getUsersPayments(pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<PaymentDto>(this.url + '/payment/getUserPayments/' + pageSize + '/' + pageNumber);
    }
  }

  pay(payDto: PayDto) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<boolean>(this.url + '/payment/pay', payDto);
    }
  }

  deletePayment(paymentId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<boolean>(this.url + '/payment/deleteUserAdditionalPaymentById/' + paymentId, '');
    }
  }

  addNewPetition(petition: Petition) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<Petition>(this.url + '/petition/addpetition', petition);
    }
  }

  getPetitions(pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<GetPetitionDto>(this.url + '/petition/getPetitions/' + pageSize + '/' + pageNumber);
    }
  }

  getPetitionById(petitionId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<PetitionDto>(this.url + '/petition/getPetition/' + petitionId);
    }
  }

  getPetitionVotes(petitionId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<PetitionVotesDto>(this.url + '/petition/getPetitionVotes/' + petitionId);
    }
  }

  postPetitionVote(petitionVote: PetitionVote) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<PetitionVote>(this.url + '/petition/vote', petitionVote);
    }
  }




  getIdeas(pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<GetIdeaDto>(this.url + '/idea/getIdeas/' + pageSize + '/' + pageNumber);
    }
  }

  addNewIdea(idea: Idea) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<Idea>(this.url + '/idea/addIdea', idea);
    }
  }

  getIdea(pageSize: number, pageNumber: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<GetIdeaDto>(this.url + '/idea/getIdeas/' + pageSize + '/' + pageNumber);
    }
  }

  getIdeaById(ideaId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<IdeaDto>(this.url + '/idea/getIdea/' + ideaId);
    }
  }

  getIdeaVotes(ideaId: number) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<IdeaVotesDto>(this.url + '/idea/getIdeaVotes/' + ideaId);
    }
  }

  postIdeaVote(ideaVote: IdeaVote) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.post<IdeaVote>(this.url + '/idea/vote', ideaVote);
    }
  }

  getStatisticPaymentData(recipientId, year) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<StatisticPaymentDto[]>(this.url + '/statistic/getPaymentStatistic/' + recipientId + '/' + year);
    }
  }

  getStatisticPopulationData(year) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<PopulationDto[]>(this.url + '/statistic/GetPopulationStatistic/' + year);
    }
  }

  getPopulationStableStatistic(year) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<PopulationStable[]>(this.url + '/statistic/GetPopulationStableStatistic/' + year);
    }
  }

  getSalariesStatistic(year) {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<SalaryDto[]>(this.url + '/statistic/getSalariesStatistic/' + year);
    }
  }

  getFoodStatistic() {
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      return this.http.get<FoodStatisticDto[]>(this.url + '/statistic/GetFoodStatistic');
    }
  }

  sendMessageForUpdatingPassword(updatePassword: UpdatePassword) {
    return this.http.post<Boolean[]>(this.url + '/auth/sendMessageForUpdatingPassword', updatePassword);
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('login');
  }
}

export class Response {
  username: string;
  token: string;
  refreshToken: string;
}


