import { Component, OnInit, Inject } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA, MatDialog, PageEvent } from '@angular/material';
import { DataService } from '../data.service';
import { CreateForum } from '../models/createForum.model';
import { Petition } from '../models/petition.model';
import { GetPetitionDto } from '../models/getPetitionDto.model';
import { ApartmentDto } from '../models/apartmentDto.model';
import { Idea } from '../models/idea.model';
import { GetIdeaDto } from '../models/getIdeaDto.model';



export interface DialogData {
  service: DataService;
  notification: MatSnackBar;
}

export class AcabDropdown {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-voting',
  templateUrl: './voting.component.html',
  styleUrls: ['./voting.component.css']
})
export class VotingComponent implements OnInit {

  noApartments = true;
  petitionDto: GetPetitionDto = new GetPetitionDto();
  ideaDto: GetIdeaDto = new GetIdeaDto();
  vinnytsiaPageNumber = 0;
  vinnytsiaPageSize = 5;
  vinnytsiaLength = 0;
  vinnytsiaSizeOptions: number[] = [5, 10, 20];
  ideasPageNumber = 0;
  ideasPageSize = 5;
  ideasLength = 0;
  ideasSizeOptions: number[] = [5, 10, 20];
  moviesPageSize = 0;
  pageEvent: PageEvent;

  constructor(public dialog: MatDialog, private dataService: DataService, private notification: MatSnackBar) { }

  ngOnInit() {
    this.getPetitions(this.vinnytsiaSizeOptions[0], 0);
    this.getIdeas(this.ideasSizeOptions[0], 0);
    this.dataService.getUserApartments().subscribe((data: ApartmentDto[]) => {
      if (data.length === 0) {
        this.noApartments = true;
      } else {
        this.noApartments = false;
      }
    });
  }

  getPetitions(pageSize: number, pageNumber: number) {
    this.dataService.getPetitions(pageSize, pageNumber).subscribe((data: GetPetitionDto) => {
      console.log(data);
      this.petitionDto = data;
      this.vinnytsiaLength = data.petitionsCount;
    });
  }

  getIdeas(pageSize: number, pageNumber: number) {
    this.dataService.getIdeas(pageSize, pageNumber).subscribe((data: GetIdeaDto) => {
      console.log(data);
      this.ideaDto = data;
      this.ideasLength = data.ideaCount;
    });
  }

  openIdeaDialog(): void {
    const dialogRef = this.dialog.open(CreateIdeaDialog, { width: '760px',
    data: { service: this.dataService, notification: this.notification } });
    dialogRef.afterClosed().subscribe(result => {
      this.getIdeas(0, this.vinnytsiaSizeOptions[0]);
    });
  }

  openPetitionDialog(): void {
    const dialogRef = this.dialog.open(CreatePetitionDialog, { width: '760px',
    data: { service: this.dataService, notification: this.notification } });
    dialogRef.afterClosed().subscribe(result => {
      this.getPetitions(0, this.vinnytsiaSizeOptions[0]);
    });
  }

  paginateVinnytsia(event: PageEvent) {
    console.log(event.pageIndex);
    this.getPetitions(event.pageSize, event.pageIndex);
  }

  paginateIdea(event: PageEvent) {
    console.log(event.pageIndex);
    this.getIdeas(event.pageSize, event.pageIndex);
  }
}



@Component({
  selector: 'create-idea-dialog',
  templateUrl: 'create-idea-dialog.html',
})

export class CreateIdeaDialog implements OnInit {

  noApartments = true;
  userApartments: ApartmentDto[] = [];
  acabs: AcabDropdown[] = [];
  ideaAcab = '';
  idea: Idea = new Idea();

  constructor(public dialogRef: MatDialogRef<CreateIdeaDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
    this.data.service.getUserApartments().subscribe((data: ApartmentDto[]) => {
      if (data.length === 0) {
        this.noApartments = true;
      } else {
        this.noApartments = false;
        this.userApartments = data;

        for (var i = 0; i < data.length; i++) {
          let temp = new AcabDropdown();
          temp.value = data[i].acabId;
          temp.viewValue = data[i].acabName;
          this.acabs.push(temp);
        }
      }
    });
  }

  createNewIdea(title: string, description: string) {
    this.idea.acabId = Number.parseInt(this.ideaAcab);
    this.idea.ideaName = title;
    this.idea.description = description;

    this.data.service.addNewIdea(this.idea).subscribe(x => {
      console.log('Idea was successfully added.');
      this.dialogRef.close();

      this.data.notification.open('Нову ідею для ОСББ створено!', 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });
    }, err => {
      console.log(err);
      this.data.notification.open('Ідею для ОСББ не створено!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'create-petition-dialog',
  templateUrl: 'create-petition-dialog.html',
})

export class CreatePetitionDialog {

  constructor(public dialogRef: MatDialogRef<CreatePetitionDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  createNewPetition(title: string, description: string) {
    const petition = new Petition();
    petition.petitionName = title;
    petition.description = description;
    this.data.service.addNewPetition(petition).subscribe(x => {
      console.log('Petition was successfully added.');
      this.dialogRef.close();

      this.data.notification.open('Нову петицію створено!', 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });
    }, err => {
      console.log(err);
      this.data.notification.open('Петицію не створено!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
