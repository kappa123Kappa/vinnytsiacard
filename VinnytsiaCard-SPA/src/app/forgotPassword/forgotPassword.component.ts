import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { UpdatePassword } from '../models/updatePassword.model';

@Component({
  selector: 'app-forgotPassword',
  templateUrl: './forgotPassword.component.html',
  styleUrls: ['./forgotPassword.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  invalidEmail = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  sendMessageForUpdatingPassword(email: string) {
    let updatePassword: UpdatePassword = new UpdatePassword();
    updatePassword.email = email;

    this.dataService.sendMessageForUpdatingPassword(updatePassword).subscribe((data: any) => {
      
    }, err => {
      this.invalidEmail = true;
    })
  }
}
