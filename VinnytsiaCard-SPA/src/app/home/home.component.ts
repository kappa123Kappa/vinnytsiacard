import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, AfterViewInit, OnInit, Inject } from '@angular/core';
import { DataService } from '../data.service';
import { User } from '../models/user.model';
import { DOCUMENT } from '@angular/common';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css', '../../assets/css/mdl/material.css'],
  providers: [DataService]
})

export class HomeComponent implements AfterViewInit, OnInit {
  sideMode = 'side';
  userData: User[] = [];
  private url = '/api/products';
  title = 'MyOSBB';
  showFiller = true;
  mobileQuery: MediaQueryList;
  fillerNav = Array.from({ length: 50 }, (_, i) => `Nav Item ${i + 1}`);
  private mobileQueryListener: () => void;
  userName = '';

  private osbbId: number;
  private userId: number;
  private subscription: Subscription;
  ngAfterViewInit(): void {

  }

  ngOnInit() {
    this.userInfo();
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }

  constructor(private dataService: DataService, private activateRoute: ActivatedRoute, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, 
    private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
    // this.subscription = activateRoute.params.subscribe(params => this.osbbId = params['osbbId']);
    // this.subscription = activateRoute.params.subscribe(params => this.userId = params['userId']);
  }

  userInfo() {
    this.dataService.userInformation().subscribe((data: User) => {
      this.userName = data.login;
    });
  }

  public logout() {
    this.dataService.logout();
    this.router.navigate(['/landing/login']);
  }
}
