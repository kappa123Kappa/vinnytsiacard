import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DataService } from '../data.service';
import { ArticlesResult, Article } from '../models/articlesResult.model';
import { EverythingRequest, SortBys, Languages } from '../models/everythingRequest.model';
import { MatTabChangeEvent } from '@angular/material';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [DataService]
})
export class NewsComponent implements OnInit {
  matTabChangeEvent: MatTabChangeEvent;
  pageNumber: number = 1;
  pageSize: number = 20;
  articlesResult: ArticlesResult;
  innerWidth: any;
  columnCount: number;
  keywordsArray: string[] = ["Ukraine", "Vinnytsia", "Sport", "Entertainment"];
  articlesResultArray: Article[][];
  defaultImage = "../../assets/Images/news/defaultNew.png";
  everythingRequest: EverythingRequest;

  constructor(private dataService: DataService) { }

  onResize(event) {
    this.innerWidth = window.innerWidth;

    if (innerWidth > 1614) {
      this.columnCount = 4;
    }
    if (innerWidth < 1614 && innerWidth > 1290) {
      this.columnCount = 3;
    }
    if (innerWidth < 1290) {
      this.columnCount = 2;
    }
    this.articlesResultArray = this.articlesResultArray.slice(0);

    this.getNews(this.matTabChangeEvent);
  }

  ngOnInit() {
    this.articlesResultArray = [];
    this.innerWidth = window.innerWidth;

    if (innerWidth > 1614) {
      this.columnCount = 4;
    }
    if (innerWidth < 1614 && innerWidth > 1290) {
      this.columnCount = 3;
    }
    if (innerWidth < 1290) {
      this.columnCount = 2;
    }

    var event = new MatTabChangeEvent();
    event.index = 0;
    this.getNews(event);
  }

  getNews(a: MatTabChangeEvent) {
    this.matTabChangeEvent = a;

    this.everythingRequest = new EverythingRequest([] as string[], [] as string[], this.keywordsArray[a.index],
      new Date(2018, 1, 25), new Date(2020, 1, 25), Languages.UK, SortBys.PublishedAt, this.pageNumber, this.pageSize);

    this.dataService.getNews(this.everythingRequest).subscribe(data => {
      this.articlesResult = data;
      console.log(data);
      var col = this.columnCount;
      this.articlesResultArray = this.articlesResultArray.slice(0);

      for (var i = 0; i < col; i++) {

        this.articlesResultArray[i] = [];

        for (var j = 0; j < this.articlesResult.articles.length; j++) {

          if (i + (j * col) < this.articlesResult.articles.length) {
            if (this.articlesResult.articles[i + (j * col)].urlToImage.indexOf("istpravda") !== -1) {
              this.articlesResult.articles[i + (j * col)].urlToImage = this.defaultImage;
            }

            this.articlesResultArray[i][j] = this.articlesResult.articles[i + (j * col)];
          }
          else break;
        }
      }

      console.log(this.articlesResultArray);

    }, err => {

      console.log(err);

    });
  }

}
