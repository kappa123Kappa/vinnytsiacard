import { Component, OnInit } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType, controllers } from 'chart.js';
import { Color } from 'chartjs-plugin-datalabels/types/options';
import { DataService } from '../data.service';
import { StatisticPaymentDto } from '../models/statisticPaymentDto.model';
import { PopulationDto } from '../models/populationDto.model';
import * as Chart from 'chart.js';
import { MatTabChangeEvent } from '@angular/material';
import { SalaryDto } from '../models/salaryDto.model';
import { FoodStatisticDto } from '../models/foodStatisticDto.model';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  lineChartDataSet: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: 'Газ',
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: 'Електроенергія',
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(214, 214, 47,1)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: 'Вода',
      backgroundColor: 'rgba(78, 177,226,0.3)',
      borderColor: 'rgba(78, 177,226, 1)',
      pointBackgroundColor: 'rgba(78, 177,226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(78, 177,226,0.8)'
    }
  ];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        gridLines: {
          display: false
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartLabels: Label[] = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень',
    'Жовтень', 'Листопад', 'Грудень'];
  public lineChartColors: Array<any> = [
    { // Red
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)'
    },
    { // yello
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(214, 214, 47,1)'
    },
    { // blue
      backgroundColor: 'rgba(78, 177,226,0.3)',
      borderColor: 'rgba(78, 177,226, 1)',
      pointBackgroundColor: 'rgba(78, 177,226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(78, 177,226,0.8)'
    }
  ];




  lineChartSalaryDataSet: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(214, 214, 47,1)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(78, 177,226,0.3)',
      borderColor: 'rgba(78, 177,226, 1)',
      pointBackgroundColor: 'rgba(78, 177,226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(78, 177,226,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(146, 204,128,0.3)',
      borderColor: 'rgba(146, 204,128, 1)',
      pointBackgroundColor: 'rgba(146, 204,128,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(146, 204,128,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(244, 117, 54,0.3)',
      borderColor: 'rgba(244, 117, 54, 1)',
      pointBackgroundColor: 'rgba(244, 117, 54,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(244, 117, 54,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(154, 51, 245,0.3)',
      borderColor: 'rgba(154, 51, 245, 1)',
      pointBackgroundColor: 'rgba(154, 51, 245,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(154, 51, 245,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(154, 154, 154,0.3)',
      borderColor: 'rgba(154, 154, 154, 1)',
      pointBackgroundColor: 'rgba(154, 154, 154,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(154, 154, 154,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(252, 0, 154,0.3)',
      borderColor: 'rgba(252, 0, 154, 1)',
      pointBackgroundColor: 'rgba(252, 0, 154,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(252, 0, 154,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(36, 213, 226,0.3)',
      borderColor: 'rgba(36, 213, 226, 1)',
      pointBackgroundColor: 'rgba(36, 213, 226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(36, 213, 226,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(175, 85, 0,0.3)',
      borderColor: 'rgba(175, 85, 0, 1)',
      pointBackgroundColor: 'rgba(175, 85, 0,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(175, 85, 0,0.8)'
    }
  ];
  public lineChartSalaryOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        gridLines: {
          display: false
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartSalaryLabels: Label[] = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень',
    'Жовтень', 'Листопад', 'Грудень'];
  public lineChartSalaryColors: Array<any> = [
    { // Red
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)'
    },
    { // yello
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(214, 214, 47,1)'
    },
    { // blue
      backgroundColor: 'rgba(78, 177,226,0.3)',
      borderColor: 'rgba(78, 177,226, 1)',
      pointBackgroundColor: 'rgba(78, 177,226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(78, 177,226,0.8)'
    }
  ];

  lineChartFoodDataSet: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(214, 214, 47,1)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(78, 177,226,0.3)',
      borderColor: 'rgba(78, 177,226, 1)',
      pointBackgroundColor: 'rgba(78, 177,226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(78, 177,226,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(146, 204,128,0.3)',
      borderColor: 'rgba(146, 204,128, 1)',
      pointBackgroundColor: 'rgba(146, 204,128,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(146, 204,128,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(244, 117, 54,0.3)',
      borderColor: 'rgba(244, 117, 54, 1)',
      pointBackgroundColor: 'rgba(244, 117, 54,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(244, 117, 54,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(154, 51, 245,0.3)',
      borderColor: 'rgba(154, 51, 245, 1)',
      pointBackgroundColor: 'rgba(154, 51, 245,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(154, 51, 245,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(154, 154, 154,0.3)',
      borderColor: 'rgba(154, 154, 154, 1)',
      pointBackgroundColor: 'rgba(154, 154, 154,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(154, 154, 154,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(252, 0, 154,0.3)',
      borderColor: 'rgba(252, 0, 154, 1)',
      pointBackgroundColor: 'rgba(252, 0, 154,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(252, 0, 154,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(36, 213, 226,0.3)',
      borderColor: 'rgba(36, 213, 226, 1)',
      pointBackgroundColor: 'rgba(36, 213, 226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(36, 213, 226,0.8)'
    },
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: '',
      backgroundColor: 'rgba(175, 85, 0,0.3)',
      borderColor: 'rgba(175, 85, 0, 1)',
      pointBackgroundColor: 'rgba(175, 85, 0,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(175, 85, 0,0.8)'
    }
  ];
  public lineChartFoodOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          display: false
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartFoodLabels: Label[] = ['1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007',
    '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016'];
  public lineChartFoodColors: Array<any> = [
    { // Red
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)'
    },
    { // yello
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(214, 214, 47,1)'
    },
    { // blue
      backgroundColor: 'rgba(78, 177,226,0.3)',
      borderColor: 'rgba(78, 177,226, 1)',
      pointBackgroundColor: 'rgba(78, 177,226,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(78, 177,226,0.8)'
    }
  ];

  public lineChartLegend = true;
  public lineChartType = 'line';


  public barChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: { xAxes: [{}], yAxes: [{}] },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData: ChartDataSets[] = [
    { data: [],
      backgroundColor: 'rgba(251, 95, 95,0.2)',
      borderColor: 'rgba(251, 95, 95,1)',
      pointBackgroundColor: 'rgba(251, 95, 95,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(251, 95, 95,0.8)',
      borderWidth: 2,
    },
    { data: [],
      backgroundColor: 'rgba(214, 214, 47,0.2)',
      borderColor: 'rgba(214, 214, 47,1)',
      pointBackgroundColor: 'rgba(214, 214, 47,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
    }
  ];



  chart: Chart;
  barChart: Chart;
  salaryChart: Chart;
  foodChart: Chart;

  constructor(private dataService: DataService) {
    this.lineChartDataSet[0].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.lineChartDataSet[1].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.lineChartDataSet[2].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.getStatisticPaymentData(1, 2019);
    this.getStatisticPaymentData(2, 2019);
    this.getStatisticPaymentData(3, 2019);
    this.getStatisticPopulationData(0, 2018);
    this.getStatisticPopulationData(1, 2019);
    this.getSalariesStatistic(2017);
    this.getFoodStatistic();
   }

  ngOnInit() {
  }

  getStatisticPaymentData(recipientId: number, year: number) {
    this.dataService.getStatisticPaymentData(recipientId, year).subscribe((dataResp: StatisticPaymentDto[]) => { 
      for (let i = 0; i < dataResp.length; i++) {
        if (recipientId === 1) {
          this.lineChartDataSet[1].data[dataResp[i].monthNumber - 1] = dataResp[i].amount;
        }
        if (recipientId === 2) {
          this.lineChartDataSet[0].data[dataResp[i].monthNumber - 1] = dataResp[i].amount;
        }
        if (recipientId === 3) {
          this.lineChartDataSet[2].data[dataResp[i].monthNumber - 1] = dataResp[i].amount;
        }
      }

      this.chart = new Chart('canvas', {
        type: this.lineChartType,
        data: {
          labels: this.lineChartLabels,
          datasets: this.lineChartDataSet
        },
        options: this.lineChartOptions,
      });
    });
  }

  getStatistic(a: MatTabChangeEvent) {
    if (a.index === 1) {
      this.barChart = new Chart('barCanvas', {
        type: this.barChartType,
        data: {
          labels: this.barChartLabels,
          datasets: this.barChartData
        },
        options: this.barChartOptions,
      });
    }
    if(a.index === 2)
    {
      this.foodChart = new Chart('foodCanvas', {
        type: this.lineChartType,
        data: {
          labels: this.lineChartFoodLabels,
          datasets: this.lineChartFoodDataSet
        },
        options: this.lineChartFoodOptions,
      });
    }
  }

  getStatisticPopulationData(index: number, year: number) {
    this.barChartLabels = [];
    this.dataService.getStatisticPopulationData(year).subscribe((data: PopulationDto[]) => {
      for (let i = 0; i < data.length; i ++) {
        this.barChartData[index].data.push(data.filter(x => x.cityId === i + 1)[0].count);
      }

      this.barChartLabels = data.sort((x, y) => x.cityId > y.cityId ? 1 : -1 ).map(x => x.cityName.toString());
      const yearVal = data[0].date.toString().slice(0, 4);
      this.barChartData[index].label = yearVal.toString();
    });
  }

  getSalariesStatistic(year: number) {
    this.dataService.getSalariesStatistic(year).subscribe((data: SalaryDto[]) => {
      for (let i = 0; i < 10; i++) {
        this.lineChartSalaryDataSet[i].data = data.filter(x => x.activityId === i + 1).map(x => x.salaryValue);
        this.lineChartSalaryDataSet[i].label = data.filter(x => x.activityId === i + 1)[i].activityName;
      }

      this.salaryChart = new Chart('salaryCanvas', {
        type: this.lineChartType,
        data: {
          labels: this.lineChartSalaryLabels,
          datasets: this.lineChartSalaryDataSet
        },
        options: this.lineChartSalaryOptions,
      });
    });
  }

  getFoodStatistic() {
    this.dataService.getFoodStatistic().subscribe((data: FoodStatisticDto[]) => {
      for (let i = 0; i < 10; i++) {
        this.lineChartFoodDataSet[i].data = data.filter(x => x.foodTypeId === i + 1).map(x => x.foodValue);
        this.lineChartFoodDataSet[i].label = data.filter(x => x.foodTypeId === i + 1)[i].foodTypeName;
      }
      console.log(data);

      
    });
  }
}
