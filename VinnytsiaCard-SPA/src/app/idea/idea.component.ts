import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { MatSnackBar } from '@angular/material';
import { IdeaDto } from '../models/getIdeaDto.model';
import { IdeaVotesDto } from '../models/ideaVotesDto.model';
import { IdeaVote } from '../models/ideaVote.model';
import { ChartOptions, ChartDataSets, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-idea',
  templateUrl: './idea.component.html',
  styleUrls: ['./idea.component.css']
})
export class IdeaComponent implements OnInit {

  ideaId = 0;
  ideaDto: IdeaDto = new IdeaDto();
  ideaVotesDto: IdeaVotesDto = new IdeaVotesDto();
  ideaVote: IdeaVote = new IdeaVote();
  disqusIdentifier = '';

  public pieChartOptions: ChartOptions = {
    responsive: true,   
    legend: {
      position: 'top',
    }
  };
  public pieChartLabels: Label[] = ['За', 'Проти'];
  public pieChartData: number[] = [0, 0];
  public pieChartDataSet: ChartDataSets[] = [
    { label: 'За', borderWidth: 3 },
    { label: 'Проти', borderWidth: 3 },
  ];

  public pieChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['rgba(146, 204, 128, 0.3)', 'rgba(251, 95, 95, 0.3)'],
      borderColor: ['rgba(146, 204, 128, 1)', 'rgba(251, 95, 95, 1)']
    }
  ];
  
  constructor(private route: ActivatedRoute, private dataService: DataService, private notification: MatSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.ideaId = params.id;
      this.getIdeasById();
      this.getIdeaVotes();
      this.disqusIdentifier = '/home/voting/idea/' + this.ideaId
    });
  }

  getIdeasById() {
    this.dataService
      .getIdeaById(this.ideaId)
      .subscribe((data: IdeaDto) => {
        this.ideaDto = data;
      });
  }

  getIdeaVotes() {
    this.dataService
      .getIdeaVotes(this.ideaId)
      .subscribe((data: IdeaVotesDto) => {
        this.ideaVotesDto = data;

        if (this.ideaVotesDto.votesList !== undefined && this.ideaVotesDto.votesList.length > 0) {
          this.pieChartData[0] = this.pieChartData[1] = 0;
          for (var i: number = 0; i < this.ideaVotesDto.votesList.length; i++) {
            if (this.ideaVotesDto.votesList[i].voteStatus) {
              this.pieChartData[0] = this.pieChartData[0] + 1;
            } else {
              this.pieChartData[1] = this.pieChartData[1] + 1;
            }
          }
        }
      });
  }

  vote(voteStatus: boolean) {
    this.ideaVote.ideaId = this.ideaDto.id;
    this.ideaVote.voteStatus = voteStatus;
    this.dataService.postIdeaVote(this.ideaVote).subscribe((data: IdeaVote) => {
      console.log(data);

      let voteString ="";

      if(voteStatus) {
        voteString = 'Ви проголосували ЗА!'
      } else {
        voteString = 'Ви проголосували ПРОТИ!'
      }

      this.notification.open(voteString, 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });

      this.getIdeaVotes();
    }, err => {
      this.notification.open('Не вдалося відправити голос!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }
}
