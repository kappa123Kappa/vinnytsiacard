import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, AfterViewInit, OnInit } from '@angular/core';
import { Registration } from '../models/registration.model';
import { DataService } from '../data.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: 'registration.component.html',
  styleUrls: ['registration.component.css', '../../assets/css/mdl/material.css']
})

export class RegistrationComponent implements AfterViewInit, OnInit {

  title = 'MyOSBB';
  showFiller = false;
  mobileQuery: MediaQueryList;
  registrationFormGroup: FormGroup;
  fillerNav = Array.from({ length: 50 }, (_, i) => `Nav Item ${i + 1}`);
  private mobileQueryListener: () => void;
  registrationData: Registration;
  errorMessage = '';
  submitted = false;
  userLogin = new FormControl('', [Validators.required, Validators.minLength(4)]);
  password = new FormControl('', [Validators.required, Validators.minLength(4)]);
  password2 = new FormControl('', [Validators.required, Validators.minLength(4)]);
  firstName = new FormControl('', [Validators.required, Validators.minLength(4)]);
  lastName = new FormControl('', [Validators.required, Validators.minLength(4)]);
  middleName = new FormControl('', [Validators.required, Validators.minLength(4)]);
  email = new FormControl('', [Validators.required, Validators.email]);
  phone = new FormControl('', [Validators.required, Validators.minLength(14)]);

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);

  }

  constructor(private dataService: DataService, changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher, private formBuilder: FormBuilder) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  getLoginErrorMessage() {
    return this.userLogin.hasError('required') ? 'Ввeдіть логін' :
      this.userLogin.hasError('minlength') ? 'Логін повинен бути не менше 4 символів' :
        '';
  }

  getFirstNameErrorMessage() {
    return this.firstName.hasError('required') ? "Ввeдіть ім'я" :
      this.firstName.hasError('minlength') ? "Ім'я повинне бути не менше 4 символів" :
        '';
  }

  getLastNameErrorMessage() {
    return this.lastName.hasError('required') ? 'Ввeдіть прізвище' :
      this.lastName.hasError('minlength') ? 'Прізвище повинне бути не менше 4 символів' :
        '';
  }

  getMiddleNameErrorMessage() {
    return this.middleName.hasError('required') ? 'Ввeдіть по-батькові' :
      this.middleName.hasError('minlength') ? 'поле "По-батькові" повинно містити не менше 4 символів' :
        '';
  }

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'Ввeдіть по-батькові' :
      this.phone.hasError('email') ? 'Введіть коректну поштову скриньку' :
        '';
  }

  getPhoneErrorMessage() {
    return this.phone.hasError('required') ? 'Ввeдіть по-батькові' :
      this.phone.hasError('minlength') ? 'Номер повинен бути +390ХХХХХХХХХ' :
        '';
  }

  get f() { return this.registrationFormGroup.controls; }

  register(userLogin: string, password: string, password2: string, firstName: string,
           lastName: string, middleName: string, email: string, phone: string) {
    this.registrationFormGroup.reset();
    this.submitted = true;


    if (this.registrationFormGroup.invalid) {
      return;
    }

    if (password !== password2) {
      this.errorMessage = 'Паролі не однакові!';
    }

    this.registrationData = new Registration(userLogin, password, password2, firstName, lastName, middleName, email, phone);
    this.dataService.register(this.registrationData);
  }
}
