import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, AfterViewInit } from '@angular/core';
import { DataService } from '../data.service';
import { Login } from '../models/login.model';
import { Router } from '@angular/router';


export class Response {
  username: string;
  token: string;
  refreshToken: string;
}

@Component({
  // selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css', '../../assets/css/mdl/material.css'],
  providers: [DataService]
})
export class LoginComponent implements AfterViewInit {

  loginData: Login = new Login('', '', false);
  private url = '/api/products';
  title = 'MyOSBB';
  showFiller = false;
  mobileQuery: MediaQueryList;
  invalidLogin = false;
  invalidLoginErrorMessage = "";
  fillerNav = Array.from({ length: 50 }, (_, i) => `Nav Item ${i + 1}`);
  private mobileQueryListener: () => void;

  ngAfterViewInit(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);

  }

  constructor(private dataService: DataService, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  login(userLogin: string, password: string, isRemembered: boolean) {
    this.invalidLoginErrorMessage = "";

    if (userLogin === undefined || password === undefined) {
      this.invalidLoginErrorMessage = this.invalidLoginErrorMessage + "Заповніть усі поля.";
      this.invalidLogin = true;
    }

    if (userLogin.length < 4) {
      this.invalidLoginErrorMessage = this.invalidLoginErrorMessage + "Логін повинен бути більше 4 символів. ";
      this.invalidLogin = true;
    }
    if (password.length < 8) {
      this.invalidLoginErrorMessage = this.invalidLoginErrorMessage + "Пароль повинен бути більше 8 символів. ";
      this.invalidLogin = true;
    }

    if (userLogin.length >= 4 && password.length >= 8) {
      this.loginData = new Login(userLogin, password, isRemembered);
      this.dataService.login(this.loginData).subscribe(
        (data: Response) => {
          localStorage.removeItem('access_token');
          localStorage.removeItem('refresh_token');
          localStorage.removeItem('login');
          localStorage.setItem('access_token', data.token);
          localStorage.setItem('refresh_token', data.refreshToken);
          localStorage.setItem('login', data.username);
          console.log(data);
          if (data !== null) {
            this.router.navigateByUrl('/home/dashboard');
          }
        }, err => {
          console.log('Error: ' + err);
          this.router.navigateByUrl('/landing/login');
          this.invalidLoginErrorMessage = "Не вірний логіг чи пароль.";
          this.invalidLogin = true;
        }
      );
    }
  }
}
