import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { PetitionDto } from '../models/getPetitionDto.model';
import { PetitionVotesDto } from '../models/petitionVotesDto.model';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { PetitionVote } from '../models/petitionVote.model';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-petition',
  templateUrl: './petition.component.html',
  styleUrls: ['./petition.component.css']
})

export class PetitionComponent implements OnInit {
  petitionId = 0;
  petitionDto: PetitionDto = new PetitionDto();
  petitionVotesDto: PetitionVotesDto = new PetitionVotesDto();
  petitionVote: PetitionVote = new PetitionVote();
  disqusIdentifier = '';

  public pieChartOptions: ChartOptions = {
    responsive: true,   
    legend: {
      position: 'top',
    }
  };
  public pieChartLabels: Label[] = ['За', 'Проти'];
  public pieChartData: number[] = [0, 0];
  public pieChartDataSet: ChartDataSets[] = [
    { label: 'За', borderWidth: 3 },
    { label: 'Проти', borderWidth: 3 },
  ];

  public pieChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['rgba(146, 204, 128, 0.3)', 'rgba(251, 95, 95, 0.3)'],
      borderColor: ['rgba(146, 204, 128, 1)', 'rgba(251, 95, 95, 1)']
    }
  ];

  constructor(private route: ActivatedRoute, private dataService: DataService, private notification: MatSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.petitionId = params.id;
      this.getPetitionsById();
      this.getPetitionVotes();
      
    });
  }

  getPetitionsById() {
    this.dataService
      .getPetitionById(this.petitionId)
      .subscribe((data: PetitionDto) => {
        this.petitionDto = data;
        this.disqusIdentifier = '/home/voting/petition/'+ this.petitionDto.id
      });
  }

  getPetitionVotes() {
    this.dataService
      .getPetitionVotes(this.petitionId)
      .subscribe((data: PetitionVotesDto) => {
        this.petitionVotesDto = data;

        if (this.petitionVotesDto.votesList !== undefined && this.petitionVotesDto.votesList.length > 0) {
          this.pieChartData[0] = this.pieChartData[1] = 0;
          for (var i: number = 0; i < this.petitionVotesDto.votesList.length; i++) {
            if (this.petitionVotesDto.votesList[i].voteStatus) {
              this.pieChartData[0] = this.pieChartData[0] + 1;
            } else {
              this.pieChartData[1] = this.pieChartData[1] + 1;
            }
          }
        }
      });
  }

  vote(voteStatus: boolean) {
    this.petitionVote.petitionId = this.petitionDto.id;
    this.petitionVote.voteStatus = voteStatus;
    this.dataService.postPetitionVote(this.petitionVote).subscribe((data: PetitionVote) => {
      console.log(data);

      let voteString ="";

      if(voteStatus) {
        voteString = 'Ви проголосували ЗА!'
      } else {
        voteString = 'Ви проголосували ПРОТИ!'
      }

      this.notification.open(voteString, 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });

      this.getPetitionVotes();
    }, err => {
      this.notification.open('Не вдалося відправити голос!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }
}
