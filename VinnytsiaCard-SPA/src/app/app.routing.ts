import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { RegistrationComponent } from './registration/registration.component';
import { AboutComponent } from './about/about.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserpageComponent } from './userpage/userpage.component';
import { NewsComponent } from './news/news.component';
import { EntertainmentComponent } from './entertainment/entertainment.component';
import { ForumComponent } from './forum/forum.component';
import { TopicDiscussionComponent } from './topicDiscussion/topicDiscussion.component';
import { MyosbbComponent } from './myosbb/myosbb.component';
import { VotingComponent } from './voting/voting.component';
import { PetitionComponent } from './petition/petition.component';
import { IdeaComponent } from './idea/idea.component';
import { StatisticComponent } from './statistic/statistic.component';
import { ForgotPasswordComponent } from './forgotPassword/forgotPassword.component';


const routes: Routes = [
    { path: 'landing', component: LandingComponent,
      children: [
        {
          path: 'index', component: IndexComponent
        },
        {
          path: 'login', component: LoginComponent
        },
        {
          path: 'registration', component: RegistrationComponent
        },
        {
          path: 'about', component: AboutComponent
        },
        {
          path: 'forgotPassword', component: ForgotPasswordComponent
        },
        {
          path: 'updatePassowrd/:id', component: TopicDiscussionComponent
        },
      ]
    },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard],
      children: [
        {
          path: 'dashboard', component: DashboardComponent
        },
        {
          path: 'userpage', component: UserpageComponent
        },
        {
          path: 'myosbb', component: MyosbbComponent
        },
        {
          path: 'entertainment', component: EntertainmentComponent
        },
        {
          path: 'forum', component: ForumComponent
        },
        {
          path: 'forum/:id', component: TopicDiscussionComponent
        },
        {
          path: 'news', component: NewsComponent
        },
        {
          path: 'voting', component: VotingComponent
        },
        {
          path: 'voting/petition/:id', component: PetitionComponent
        },
        {
          path: 'voting/idea/:id', component: IdeaComponent
        },
        {
          path: 'statistic', component: StatisticComponent
        }
    ]
    },
    { path: '**', redirectTo: '/landing/index', pathMatch: 'full' }
    // { path: '/about', component: ChildFourComponent }
];

export const appRoutingProviders: any[] = [
];

export const routing = RouterModule.forRoot(routes, { useHash: true });
