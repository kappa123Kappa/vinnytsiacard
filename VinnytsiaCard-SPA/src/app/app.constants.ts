import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
  public Server = 'https://vinnytsiacard.azurewebsites.net/';//''https://vinnytsiacard.azurewebsites.net/'; //'http://localhost:5000/'
  public ApiUrl = 'api';
  public ServerWithApiUrl = this.Server + this.ApiUrl;
}
