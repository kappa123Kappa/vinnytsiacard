import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from '../data.service';
import { Forum } from '../models/forum.model';
import { MatSnackBar } from '@angular/material';
import { CreateForum } from '../models/createForum.model';

export interface DialogData {
  service: DataService;
  notification: MatSnackBar;
}

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {

  forumTopics: Forum[] = [];

  constructor(public dialog: MatDialog, private dataService: DataService, private notification: MatSnackBar) { }

  ngOnInit() {
    this.dataService.getForumTopics(10, 0).subscribe((data: Forum[]) => {
      this.forumTopics = data;
      console.log(this.forumTopics);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateForumTopicDialog, { width: '760px',
    data: { service: this.dataService, notification: this.notification } });
    dialogRef.afterClosed().subscribe(result => {
        this.dataService.getForumTopics(10, 0).subscribe((data: Forum[]) => {
          this.forumTopics = data;
          console.log(this.forumTopics);
        });
    });
  }
}

@Component({
  selector: 'create-forum-topic-dialog',
  templateUrl: 'create-forum-topic-dialog.html',
})

export class CreateForumTopicDialog {

  constructor(public dialogRef: MatDialogRef<CreateForumTopicDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  createNewTopic(title: string, message: string) {
    const forum = new CreateForum();
    forum.Title = title;
    forum.Message = message;
    this.data.service.addNewForumTopic(forum).subscribe(x => {
      console.log('Topic was successfully added.');
      this.dialogRef.close();

      this.data.notification.open('Нову тему створено!', 'Повідомлення', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['blue', 'popupmessage']
      });
    }, err => {
      console.log(err);
      this.data.notification.open('Тему не створено!', 'Помилка', {
        duration: 5 * 1000,
        verticalPosition: 'top',
        panelClass: ['red', 'popupmessage']
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
