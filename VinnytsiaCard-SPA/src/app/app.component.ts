import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { User } from './models/user.model';
import { Router } from '../../node_modules/@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataService]
})

export class AppComponent implements OnInit {
  title = 'Картка Вінничанина';
  showFiller = false;
  mobileQuery: MediaQueryList;
  userName: string;
  private mobileQueryListener: () => void;

  constructor(public http: HttpClient, private dataService: DataService,  changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private titleService: Title) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
    this.titleService.setTitle(this.title);
    //this.userInfo();
  }

  ngOnInit() {
    //this.userInfo();
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }

  // userInfo() {
  //   this.dataService.userInformation().subscribe((data: User) => {
  //     this.userName = data.login;
  //     if (this.userName !== undefined) {
  //       this.showFiller = true;
  //     }
  //   }, err => {
  //       if (err.status === 401) {
  //       this.showFiller = false;
  //     }
  //   });
  // }

  // public requestlogin() {
  //   const id = this.http.get('https://4200/login');
  // }

  // public ping() {
  //   this.http.get('https://4200/api/things')
  //     .subscribe(
  //       data => console.log(data),
  //       err => console.log(err)
  //     );
  // }

  // logout() {
  //   this.dataService.logout();
  // }
}
