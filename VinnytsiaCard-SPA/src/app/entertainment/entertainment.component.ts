import { Component, OnInit, ViewChild } from '@angular/core';
import { SmartCinemaRequestPayload } from '../models/smartCinemaRequestPayload.model';
import { SmartCinemaGetMoviesResponse, Soon, Rent } from '../models/smartCinemaGetMoviesResponse.model';
import { PageEvent, MatPaginator } from '@angular/material';
import { DataService } from '../data.service';
import { EverythingRequest, Languages, SortBys } from '../models/everythingRequest.model';

@Component({
  selector: 'app-entertainment',
  templateUrl: './entertainment.component.html',
  styleUrls: ['./entertainment.component.css']
})

export class EntertainmentComponent implements OnInit {
  innerWidth: number = 0;
  moviesRentLength: number = 0;
  moviesRentPageSize: number = 0;
  moviesSoonLength: number = 0;
  moviesSoonPageSize: number = 0;
  smartCinemaRequestPayload: SmartCinemaRequestPayload;
  smartCinemaGetMoviesRentResponse: Rent[];
  smartCinemaGetMoviesSoonResponse: Soon[];
  
  pageEvent: PageEvent;
  pageSizeOptions: number[] = [1, 2, 3, 4, 5];
  @ViewChild('paginatorRent', {static: false}) paginator: MatPaginator;
  @ViewChild('paginatorSoon', {static: false}) paginator2: MatPaginator;
  
  constructor(private dataService: DataService) {
    this.innerWidth = window.innerWidth;

    if (innerWidth > 1614) {
      this.moviesRentPageSize = 4;
      this.moviesSoonPageSize = 4;
    }
    if (innerWidth < 1614 && innerWidth > 1290) {
      this.moviesRentPageSize = 3;
      this.moviesSoonPageSize = 3;
    }
    if (innerWidth < 1290) {
      this.moviesRentPageSize = 2;
      this.moviesSoonPageSize = 2;
    } 

    this.getMoviesRent(0, this.moviesRentPageSize);
    this.getMoviesSoon(0, this.moviesSoonPageSize);
  }

  ngOnInit() {

  }
  
  ngAfterViewInit() {
    this.paginator.pageIndex = 1;
    this.paginator2.pageIndex = 1;  
  }

  getMoviesRent(pageNumber: number, pageSize: number) {
    this.smartCinemaRequestPayload = new SmartCinemaRequestPayload(1);
    this.dataService.getMovies(this.smartCinemaRequestPayload).subscribe((data: SmartCinemaGetMoviesResponse) => {
      this.smartCinemaGetMoviesRentResponse = data.rent;
      this.paginator.length = this.smartCinemaGetMoviesRentResponse.length;

      this.smartCinemaGetMoviesRentResponse.map(x => {
        x.seo_inline.url = "https://smartcinema.ua" + x.seo_inline.url;
        x.poster = "https://smartcinema.ua" + x.poster;
      });      

      this.smartCinemaGetMoviesRentResponse = this.smartCinemaGetMoviesRentResponse.slice(pageSize * pageNumber, (pageSize * pageNumber) + pageSize)
      console.log(this.smartCinemaGetMoviesRentResponse);
    }, err => {
      console.log(err);
    });
  }

  getMoviesSoon(pageNumber: number, pageSize: number) {
    this.smartCinemaRequestPayload = new SmartCinemaRequestPayload(1);
    this.dataService.getMovies(this.smartCinemaRequestPayload).subscribe((data: SmartCinemaGetMoviesResponse) => {
      this.smartCinemaGetMoviesSoonResponse = data.soon;
      this.paginator2.length = this.smartCinemaGetMoviesSoonResponse.length;
      this.smartCinemaGetMoviesSoonResponse.map(x => {
        x.seo_inline.url = "https://smartcinema.ua" + x.seo_inline.url;
        x.poster = "https://smartcinema.ua" + x.poster;
      });      

      this.smartCinemaGetMoviesSoonResponse = this.smartCinemaGetMoviesSoonResponse.slice(pageSize * pageNumber, (pageSize * pageNumber) + pageSize)
      console.log(this.smartCinemaGetMoviesSoonResponse);
    }, err => {
      console.log(err);
    });
  }

  paginateMoviesRent(event: PageEvent) {
    console.log(event.pageIndex);
    this.getMoviesRent(event.pageIndex, event.pageSize);
  }

  paginateMoviesSoon(event: PageEvent) {
    console.log(event.pageIndex);
    this.getMoviesSoon(event.pageIndex, event.pageSize);
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
}
